using UnityEngine;
using System.Collections;

public class CountInventory : MonoBehaviour {

    public KeyCode keyCode;

	private void Update() {
        if (Input.GetKeyDown(keyCode)) {
            D.log("count items in inventory: {0}", Fieldbook.InventoryCount);
        }
    }
}
