using UnityEngine;
using System.Collections;

public class TeleportPlayer : MonoBehaviour {

    public KeyCode keyTeleport;

	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(keyTeleport)) {
            FPController.Instance.transform.position = transform.position;
        }
	}
}
