using UnityEngine;
using System.Collections;

public class EnableOnKeyPress : MonoBehaviour {

    public GameObject toBeEnabled;

	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.K)) {
            toBeEnabled.SetActive (true);
        }
	}
}
