using UnityEngine;
using System.Collections;
using System;

public class Collectress : MonoBehaviour {

    public DialoguerDialogues museumStart;
    public Transform headLookTarget;

    private bool _firstDialogueComplete = false;
    private int _nextDialogueID = (int)DialoguerDialogues.museum_collectress_missing;

    private TaskType _taskTypeAdvanceOnZonePuzzleComplete = TaskType.museum_match;

    //-----------------------------------------------
    // Unity functions
    //-----------------------------------------------

    protected void Awake() {

    }

	protected void Start() {
        //Dialoguer.events.onEnded += onDialogueEnded;
        Dialoguer.events.onMessageEvent += onMessageEvent;
        ZoneManager.ZoneChanged += onZoneChanged;
        ZoneManager.ZoneMatchCompleted += OnZoneMatchCompleted;
    }

    private void OnZoneMatchCompleted(object sender, EventArgs e) {
        TaskManager.Instance.AdvanceTask(_taskTypeAdvanceOnZonePuzzleComplete);
    }

    private void onMessageEvent(string message, string metadata) {
        if (!string.IsNullOrEmpty(message)) {
            if (message == "firstDialogueComplete") {
                _firstDialogueComplete = true;
            }
        }
    }

    protected void OnDestroy() {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
        ZoneManager.ZoneChanged -= onZoneChanged;
        ZoneManager.ZoneMatchCompleted -= OnZoneMatchCompleted;
    }

    private void onZoneChanged(int newDialogueID) {
        _nextDialogueID = newDialogueID;
    }

    //private void onDialogueEnded() {
    //    _firstDialogueComplete = true;
    //}

    public void OnMouseDown() {
        int dialogueID;
        if (!_firstDialogueComplete) { dialogueID = (int)museumStart; }
        else { dialogueID = _nextDialogueID; }
        GUIDialogue.Instance.HandleDialogueStartRequest(dialogueID, headLookTarget.position);
    }
	
	
}
