using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class SliderEventArgs : System.EventArgs {
    public int[] Order { get; set; }
}

public class SliderManager : MonoBehaviour {

    public event EventHandler<SliderEventArgs> SliderStart;
    public event EventHandler<SliderEventArgs> SliderEnd;
    public SliderSlot[] slots;

    // for each SliderSlot in slots, gets the id of the SliderDragHandler
    // stores in array to represent the state of the Slider puzzle
    public int[] Order {
        get {
            int[] arr = new int[slots.Length];
            for (int i = 0; i < slots.Length; i++) {
                SliderDragHandler sdh = slots[i].GetComponentInChildren<SliderDragHandler>();
                D.assert(sdh != null);
                arr[i] = sdh.ID;
            }
            return arr;
        }
    }
    [SerializeField]
    private int[] _sliderPuzzleSolution = { 0, 1, 2, 3, 4, 5, 6 }; // when Order returns array that matches this, puzzle is correct
    private float _updateInterval = 2f;

    private IEnumerator updateStatus() {
        while (true) {
            if (_sliderPuzzleSolution.SequenceEqual(Order)) {
                onSliderEnd();
                break;
            }
            yield return new WaitForSeconds(_updateInterval);
        }
    }

    public void onSliderStart() {
        SliderEventArgs e = new SliderEventArgs();
        e.Order = Order;
        if (SliderStart != null) {
            SliderStart(this, e);
        }
        // unlock the sliders, puzzle is active
        setAllDragHandlerLockedState(false);
        // continuously check for completion with _updateInterval frequency
        StartCoroutine(updateStatus());         
    }


    public void onSliderEnd() {
        D.log("onSliderEnd was called");
        SliderEventArgs e = new SliderEventArgs();
        e.Order = Order;
        if (SliderEnd != null) {
            SliderEnd(this, e);
        }
        // lock the sliders, puzzle is complete
        setAllDragHandlerLockedState(true);
        // stop any coroutines running on this object
        StopAllCoroutines();
    }

    public void setAllDragHandlerLockedState(bool state) {
        foreach (SliderSlot ss in slots) {
            ss.setDragHandlerLockedState(state);
        }
    }

}
