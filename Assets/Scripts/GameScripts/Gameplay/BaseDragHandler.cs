using UnityEngine;
using System.Collections;
using System;

public class BaseDragHandler : MonoBehaviour {

    public virtual void OnMouseDown () {
        throw new NotImplementedException();
    }

    public virtual void OnMouseDrag () {
        throw new NotImplementedException();
    }

    public virtual void OnMouseUpAsButton () {
        throw new NotImplementedException();
    }
}
