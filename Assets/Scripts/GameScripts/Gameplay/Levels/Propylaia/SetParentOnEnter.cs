using UnityEngine;
using System.Collections;

public class SetParentOnEnter : MonoBehaviour {

    public Transform parentOnEnter;
    public Transform parentOnExit;
    public float disableDelayOnDrop = 2f;

    public static bool triggerEnabled = true;

    private void OnTriggerEnter (Collider other) {

        if (!triggerEnabled) { return; }

        // make work for all objects except the player
        if (other.tag != "Player" && other.tag != "MainCamera") {
            other.SendMessage("SetParentOnRelease", parentOnEnter);
        }
    }

    private void OnTriggerExit (Collider other) {

        if (!triggerEnabled) { return; }

        if (other.tag != "Player" && other.tag != "MainCamera") {
            other.SendMessage("SetParentOnRelease", parentOnExit);
        }
    }

}
