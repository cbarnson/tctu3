using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

public class ColumnComparer : IComparer<ColumnComponent> {
    public int Compare(ColumnComponent x, ColumnComponent y) {
        if (x == null) {
            if (y == null) {
                // If x is null and y is null, they're
                // equal. 
                return 0;
            } else {
                // If x is null and y is not null, y
                // is greater. 
                return -1;
            }
        } else {
            // If x is not null...
            if (y == null) { // ...and y is null, x is greater.
                return 1;
            } else {
                // neither is null, compare the y position of the two objects
                int retval = x.transform.position.y.CompareTo(y.transform.position.y);

                // retval < 0, x precedes y in sort order
                // retval == 0, x occurs in same position as y in sort order
                // retval > 0, x follows y in sort order
                return retval;
            }
        }
    }
}


public class ColumnStack : MonoBehaviour {

    // raised when this column is completed (i.e. correctly "built")
    public static event EventHandler ColumnCompleted;
    public ColumnType columnType;

    protected void Start() {
        ColumnComponent.ColumnComponentDropped += OnColumnComponentDropped;
    }

    protected void OnDestroy() {
        ColumnComponent.ColumnComponentDropped -= OnColumnComponentDropped;
    }

    /// <summary>
    /// Checks child objects to this gameobject, i.e. those with ColumnComponent script attached, and returns true
    /// if there is a child ColumnComponent who's componentType field matches that provided by the parameter.
    /// </summary>
    /// <param name="cct"></param>
    /// <returns>True if a child ColumnComponent with componentType field matches the parameter; false otherwise.</returns>
    public bool HasColumnComponentType(ColumnComponentType cct) {
        bool result = false;
        int count = transform.childCount;
        for (int i = 0; i < count; i++) {
            ColumnComponent cc = transform.GetChild(i).GetComponent<ColumnComponent>();
            if (cc != null) {
                if (cc.componentType == cct) {
                    result = true;
                }
            }
        }
        return result;
    }

    private void OnColumnComponentDropped(object sender, EventArgs e) {
        ColumnComponent cc = (ColumnComponent)sender; // unboxing
        if (cc != null) {
            if (cc.transform.parent == transform) {
                // based on what is already on this stack, reorder things such that the y values: base < shaft < capital
                ArrangeStackChildren();
                // then call showStackOrder
                showStackOrder();
            }
        }
    }

    private void SetHeightColumnComponent(ColumnComponentType cct, ref float height, float spacing) {
        ColumnComponent[] ccArray = this.transform.GetComponentsInChildren<ColumnComponent>();
        foreach (ColumnComponent c in ccArray) {
            if (c.componentType == cct) {
                c.transform.localPosition = new Vector3(0f, height, 0f);
                Collider col = c.GetComponent<Collider>();
                height += col.bounds.size.y + spacing;
            }
        }
    }

    private void ArrangeStackChildren() {
        ColumnComponent[] ccArray = this.transform.GetComponentsInChildren<ColumnComponent>();
        float spacing = 5f;
        float height = spacing;
        SetHeightColumnComponent(ColumnComponentType.column_base, ref height, spacing);
        SetHeightColumnComponent(ColumnComponentType.column_shaft, ref height, spacing);
        SetHeightColumnComponent(ColumnComponentType.column_capital, ref height, spacing);
    }

    private void showStackOrder() {
        List<ColumnComponent> columnList = new List<ColumnComponent>();

        for (int i = 0; i < transform.childCount; i++) {
            if (transform.GetChild(i).GetComponent<ColumnComponent>() != null)
                columnList.Add(transform.GetChild(i).GetComponent<ColumnComponent>());
        }
        // if empty, exit now
        if (columnList.Count < 1) { return; }

        // use our custom sort to organize the list
        ColumnComparer cc = new ColumnComparer();
        columnList.Sort(cc);

        // print the order to the debug log
        int count = 0;
        foreach (ColumnComponent ccc in columnList) {
            D.log("{0} order number {1}, is {2}", gameObject.name, count++, ccc.name);
        }

        // check if the column list in this stack is correct
        if (isColumnComplete(ref columnList)) {
            D.log("column has been build successfully");
            OnColumnComplete();
        }
    }

    private void OnColumnComplete() {
        if (ColumnCompleted != null) {
            ColumnCompleted(this, EventArgs.Empty);
        }
    }

    /// <summary>
    /// Called by showStackOrder, returns true if the stack for this ColumnStack object is correctly completed, 
    /// false otherwise.
    /// </summary>
    /// <param name="cList"></param>
    /// <returns>True if correctly completed; false otherwise.</returns>
    private bool isColumnComplete(ref List<ColumnComponent> cList) {
        // all must match this stack's columnType
        for (int i = 0; i < cList.Count-1; i++) {
            ColumnComponent cc = cList[i].GetComponent<ColumnComponent>();
            if (cc != null) {
                if (cc.type != columnType) return false;
            }
        }

        if (columnType == ColumnType.doric) {
            if (cList.Count == 2 &&
                cList[0].componentType == ColumnComponentType.column_shaft &&
                cList[1].componentType == ColumnComponentType.column_capital)
                return true;
        } else if (columnType == ColumnType.corinthian) {
            if (cList.Count == 3 &&
                cList[0].componentType == ColumnComponentType.column_base &&
                cList[1].componentType == ColumnComponentType.column_shaft &&
                cList[2].componentType == ColumnComponentType.column_capital)
                return true;
        } else if (columnType == ColumnType.ionic) {
            if (cList.Count == 3 &&
                cList[0].componentType == ColumnComponentType.column_base &&
                cList[1].componentType == ColumnComponentType.column_shaft &&
                cList[2].componentType == ColumnComponentType.column_capital)
                return true;
        }
        return false;

        //    // at this point, all elements in cList are guaranteed to match type

        //    // if columnType == doric, cList must have Count == 2
        //    if (columnType == ColumnType.doric) {
        //    if (cList.Count != 2) { return false; }
        //    // order must be column_shaft, column_capital
        //    if (cList[0].GetComponent<ColumnComponent>().componentType == ColumnComponentType.column_shaft &&
        //        cList[1].GetComponent<ColumnComponent>().componentType == ColumnComponentType.column_capital) {
        //        D.log("doric column is correct");
        //        return true;
        //    }
        //} else {
        //    // else, cList must have Count == 3
        //    if (cList.Count != 3) { return false; }
        //    // order must be column_base, column_shaft, column_capital
        //    if (cList[0].GetComponent<ColumnComponent>().componentType == ColumnComponentType.column_base &&
        //        cList[1].GetComponent<ColumnComponent>().componentType == ColumnComponentType.column_shaft &&
        //        cList[2].GetComponent<ColumnComponent>().componentType == ColumnComponentType.column_capital) {
        //        D.log("either corinthian or ionic column is correct");
        //        return true;
        //    }
        //}
        //// if we haven't returned true or false by this point, is incorrect, return false
        //return false;
    }

    


}
