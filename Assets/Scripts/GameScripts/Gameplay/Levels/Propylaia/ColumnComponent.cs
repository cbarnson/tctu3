using UnityEngine;
using System.Collections;
using System;

public enum ColumnComponentType {
    column_capital,
    column_shaft,
    column_base
}

public enum ColumnType {
    corinthian,
    doric,
    ionic
}

public class ColumnComponent : MonoBehaviour {

    // raised each time this piece is dropped, regardless of whether or not dropped into a valid stack
    public static event EventHandler ColumnComponentDropped;
    private const string INTERACTIVE_ON_MESSAGE_EVENT = "startColumn";

    public ColumnComponentType componentType;
    public ColumnType type;
    public bool IsHolding {
        get {
            return _holding;
        }
    }

    private Rigidbody _rigidbody;
    private float pullSpeed = 7f;
    private bool _interactable;
    private bool _holding;
    private float _distToObject;
    private float _distToGround;        // set in Start
    private float _minLiftDistance;     // set in Start
    private float _holdDistance = 3f;
    private Transform _parentOnRelease;

    public void SetPotentialParent(Transform t) {
        _parentOnRelease = t;
    }

    protected void Awake() {
        _interactable = false;
        _holding = false;
        _rigidbody = gameObject.GetComponent<Rigidbody>();
        _parentOnRelease = transform.parent;
    }

    protected void Start() {
        Dialoguer.events.onMessageEvent += OnMessageEvent;
        ColumnStack.ColumnCompleted += OnColumnCompleted;
        _minLiftDistance = transform.position.y + 1f;
        _distToGround = transform.position.y;
    }

    protected void OnDestroy() {
        Dialoguer.events.onMessageEvent -= OnMessageEvent;
        ColumnStack.ColumnCompleted -= OnColumnCompleted;
    }

    /// <summary>
    /// Upon receiving ColumnStack event that a stack was completed, if this object matches the type, 
    /// it must be part of that stack.  Thus, disable interactivity for this piece.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void OnColumnCompleted(object sender, EventArgs e) {
        D.log("on column completed in columncomponent");
        ColumnStack cs = (ColumnStack)sender;
        D.assert(cs != null, "Assert failed, reference to ColumnStack object was null", true);
        if (type == cs.columnType) _interactable = false;
    }

    /// <summary>
    /// Set field _interactable to true upon receiving Dialoguer message event with message string that matches
    /// the public string field interactiveOnMessageEvent.
    /// </summary>
    /// <param name="message"></param>
    /// <param name="metadata"></param>
    private void OnMessageEvent(string message, string metadata) {
        if (!string.IsNullOrEmpty(INTERACTIVE_ON_MESSAGE_EVENT) && !string.IsNullOrEmpty(message)) {
            if (message.Equals(INTERACTIVE_ON_MESSAGE_EVENT, StringComparison.Ordinal)) {
                _interactable = true;
            }
        }
    }

    protected void OnMouseDown() {
        if (!_interactable) { return; }
        _holding = true;
        _rigidbody.isKinematic = true;          // to prevent gravity from interferring
        _distToObject = getDistanceToObject();  // initialize value
        SetParentToColumnManager();             // reset parent to ColumnManager
        Pull();
    }

    protected void OnMouseDrag() {
        if (!_interactable) { return; }
        Vector3 newPosition = new Vector3();
        newPosition = FPController.Instance.cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _distToObject));
        if (newPosition.y < _minLiftDistance) {
            transform.position = new Vector3(newPosition.x, _distToGround, newPosition.z);
            return;
        }
        transform.position = newPosition;
    }

    // called when released and cursor is NOT over the object
    protected void OnMouseUp() {
        Release();
    }

    // called when released and cursor is over the object
    protected void OnMouseUpAsButton() {
        Release();
    }

    protected void Release() {
        if (!_interactable) { return; }
        _holding = false;
        _rigidbody.isKinematic = false;
        transform.parent = _parentOnRelease;        // set parent
        OnColumnComponentDropped();
    }

    protected void Pull() {
        StopAllCoroutines();
        StartCoroutine(PullTowards());
        StartCoroutine(PullUp());
    }

    protected virtual void OnColumnComponentDropped() {
        if (ColumnComponentDropped != null) {
            ColumnComponentDropped(this, EventArgs.Empty);
        }
    }

    protected float getDistanceToObject() {
        return FPController.Instance.cam.WorldToScreenPoint(gameObject.transform.position).z;
    }

    // moves _distToObject to _holdDistance
    IEnumerator PullTowards() {
        while (_distToObject != _holdDistance) {
            _distToObject = Mathf.MoveTowards(_distToObject, _holdDistance, pullSpeed * Time.deltaTime);
            yield return null;
        }
        yield return null;
    }

    // moves _distToGround to _minLiftDistance
    // ensures the holding object cannot be moved through the ground    
    IEnumerator PullUp() {
        while (_distToGround != _minLiftDistance) {
            _distToGround = Mathf.MoveTowards(_distToGround, _minLiftDistance, pullSpeed * Time.deltaTime);
            yield return null;
        }
        yield return null;
    }

    private void SetParentToColumnManager() {
        ColumnManager cm = this.transform.GetComponentInParent<ColumnManager>();
        if (cm != null) {
            this.transform.SetParent(cm.transform);
            _parentOnRelease = cm.transform;
        } else {
            D.warn("warning, ColumnComponent could not locate ColumnManager");
        }
    }


}
