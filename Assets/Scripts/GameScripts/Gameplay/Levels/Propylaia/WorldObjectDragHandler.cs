using UnityEngine;
using System.Collections;

public class WorldObjectDragHandler : MonoBehaviour {

    public delegate void DropEventHandler(GameObject obj);
    public static event DropEventHandler onObjectDrop;

    public float pullSpeed = 7f;
    public bool interactable = true;

    protected Rigidbody _rigidbody;
    protected bool _holding = false;
    protected float _distToObject;
    protected float _distToGround;    // set in Start
    protected float _minLiftDistance; // set in Start
    protected float _holdDistance = 3f;

    private Transform _parentOnRelease;

    public void SetParentOnRelease(Transform t) {
        D.log("parent on release is: " + t.name);
        _parentOnRelease = t;
    }

    protected virtual void Awake () {
        _rigidbody = GetComponent<Rigidbody> ();
        _parentOnRelease = transform.parent;
    }

    protected virtual void Start () {
        _minLiftDistance = transform.position.y + 1f;
        _distToGround = transform.position.y;
    }

    protected virtual void OnMouseDown () {
        if (!interactable) { return; }
        _holding = true;
        _rigidbody.isKinematic = true; // to prevent gravity from interferring
        _distToObject = getDistanceToObject();
        //D.log ("dist to object: {0}", _distToObject);
        Pull();
    }

    protected virtual void OnMouseDrag () {
        if (!interactable) { return; }
        Vector3 newPosition = new Vector3 ();
        newPosition = FPController.Instance.cam.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, _distToObject));
        if (newPosition.y < _minLiftDistance) {
            transform.position = new Vector3 (newPosition.x, _distToGround, newPosition.z);
            return;
        }
        transform.position = newPosition;
    }

    // called when released and cursor is NOT over the object
    protected virtual void OnMouseUp () {
        Release();
        //if (!interactable) { return; }
        //if (_holding) {
        //    _holding = false;
        //    Drop ();
        //}
    }

    // called when released and cursor is over the object
    protected virtual void OnMouseUpAsButton () {
        Release();
        //if (!interactable) { return; }
        //_holding = false;
        //Drop ();
    }

    protected virtual void Release() {
        if (!interactable) { return; }
        _holding = false;
        //D.log("setting new parent, from " + this.name);
        transform.parent = _parentOnRelease;        // set parent
        Drop();
    }


    // called when OnMouseUpAsButton is called, or OnMouseUp when _holding is true
    protected virtual void Drop () {
        _rigidbody.isKinematic = false;
        if (onObjectDrop != null) {
            onObjectDrop (gameObject);
        }
    }

    protected virtual void Pull() {
        StopAllCoroutines();
        StartCoroutine(PullTowards());
        StartCoroutine(PullUp());
    }

    // moves _distToObject to _holdDistance
    IEnumerator PullTowards () {
        while (_distToObject != _holdDistance) {
            _distToObject = Mathf.MoveTowards (_distToObject, _holdDistance, pullSpeed * Time.deltaTime);
            yield return null;
        }
        yield return null;
    }

    // moves _distToGround to _minLiftDistance
    // ensures the holding object cannot be moved through the ground    
    IEnumerator PullUp () {
        while (_distToGround != _minLiftDistance) {
            _distToGround = Mathf.MoveTowards (_distToGround, _minLiftDistance, pullSpeed * Time.deltaTime);
            yield return null;
        }
        yield return null;
    }

    protected virtual float getDistanceToObject() {
        return FPController.Instance.cam.WorldToScreenPoint(gameObject.transform.position).z;
    }
}
