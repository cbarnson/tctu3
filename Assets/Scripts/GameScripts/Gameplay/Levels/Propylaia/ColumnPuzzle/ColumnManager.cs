using UnityEngine;
using System;
using System.Collections;

public class ColumnManager : MonoBehaviour {

    public static event EventHandler ColumnPuzzleCompleted;

    private const int STACK_COUNT = 3;
    private int _countComplete;

    protected void Awake() {
        _countComplete = 0;
    }

    protected void Start() {
        ColumnStack.ColumnCompleted += OnColumnCompleted;
    }

    protected void OnDestroy() {
        ColumnStack.ColumnCompleted -= OnColumnCompleted;
    }

    private void OnColumnCompleted(object sender, EventArgs e) {
        // unbox
        ColumnStack cs = (ColumnStack)sender;
        D.assert(cs != null);
        D.log("ColumnStack completed: " + cs.columnType.ToString());
        // increment counter
        _countComplete++;
        if (_countComplete >= STACK_COUNT) OnColumnPuzzleCompleted();
    }

    protected virtual void OnColumnPuzzleCompleted() {
        if (ColumnPuzzleCompleted != null) {
            ColumnPuzzleCompleted(this, EventArgs.Empty);
        }
    }











    //private void OnColumnComplete(object sender, ColumnCompleteEventArgs e) {
    //    //_counter++;
    //    //DebugPrintCompletedCount();
    //    //if (_counter > 2) {
    //    //    onColumnPuzzleComplete(new ColumnPuzzleCompleteEventArgs());
    //    //}
    //}

    //private void onColumnPuzzleComplete(ColumnPuzzleCompleteEventArgs e) {
    //    if (ColumnPuzzleComplete != null) {
    //        ColumnPuzzleComplete(this, e);
    //    }
    //}

    //private void DebugPrintCompletedCount() {
    //    D.log("columns completed: " + _counter);
    //}
}
