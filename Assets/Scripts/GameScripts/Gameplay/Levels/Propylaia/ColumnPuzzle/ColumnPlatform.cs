using UnityEngine;
using System.Collections;

public class ColumnPlatform : MonoBehaviour {

    private ColumnStack _stack;

    protected void Awake() {
        _stack = GetComponentInChildren<ColumnStack>();
        D.assert(_stack != null, "Assert failed, ColumnPlatform: " + this.gameObject.name.ToString() + ", is missing child ColumnStack");
    }

    /// <summary>
    /// If the collider object entering the trigger has a ColumnComponent component and the 'potential' parent
    /// ColumnStack does not already have a child of that componentType, then set the collider object's parent
    /// to be the ColumnStack object.
    /// </summary>
    /// <param name="other"></param>
	protected void OnTriggerEnter(Collider other) {
        if (IsColumnComponent(other) && IsStackValid(other))
            other.GetComponent<ColumnComponent>().SetPotentialParent(_stack.transform);
    }

    /// <summary>
    /// If collider exiting this trigger has a ColumnComponent and that object is being held at the time of 
    /// exiting, then set the collider object's parent to be the ColumnManager object.
    /// </summary>
    /// <param name="other"></param>
    protected void OnTriggerExit(Collider other) {
        if ((IsColumnComponent(other)) && (IsHoldingColumnComponent(other))) 
            SetParentToColumnManager(other.transform);
    }

    /// <summary>
    /// Checks if parameter object has a ColumnComponent attached.
    /// </summary>
    /// <param name="other"></param>
    /// <returns>True if it does; false otherwise.</returns>
    private bool IsColumnComponent(Collider other) {
        if (other.GetComponent<ColumnComponent>() != null) 
            return true;
        return false;
    }

    /// <summary>
    /// Checks if the parameter object is being held.
    /// </summary>
    /// <param name="other"></param>
    /// <returns>If the collider object has a ColumnComponent and its IsHolding field is true, return true; 
    /// false otherwise.</returns>
    private bool IsHoldingColumnComponent(Collider other) {
        if (other.GetComponent<ColumnComponent>() != null) 
            return other.GetComponent<ColumnComponent>().IsHolding;
        return false;
    }

    /// <summary>
    /// If collider object has a ColumnComponent component and this ColumnPlatform's ColumnStack doesn't 
    /// already have a child of that componentType, return true.
    /// </summary>
    /// <param name="other"></param>
    /// <returns>True if valid; false otherwise.</returns>
    private bool IsStackValid(Collider other) {
        ColumnComponent cc = other.GetComponent<ColumnComponent>();
        if ((cc != null) && (!_stack.HasColumnComponentType(cc.componentType)))
            return true;
        return false;
    }

    /// <summary>
    /// Takes a Transform parameter and sets that objects parent to the transform of the first
    /// gameobject that has a ColumnManager component.
    /// </summary>
    /// <param name="child"></param>
    private void SetParentToColumnManager(Transform child) {
        if (transform.GetComponentInParent<ColumnManager>() != null) {
            child.SetParent(transform.GetComponentInParent<ColumnManager>().transform);
        }
    }
}
