﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnStackDisablerOnComplete : MonoBehaviour {

    public GameObject toBeDisabled;
    private ColumnStack _columnStack;

    protected void Awake() {
        _columnStack = GetComponent<ColumnStack>();
        D.assert(_columnStack != null);
        D.assert(toBeDisabled != null);
    }

	protected void Start() {
        ColumnStack.ColumnCompleted += OnColumnCompleted;
    }

    protected void OnDestroy() {
        ColumnStack.ColumnCompleted -= OnColumnCompleted;
    }

    private void OnColumnCompleted(object sender, EventArgs e) {
        ColumnStack cs = (ColumnStack) sender;
        if (cs != null) {
            if (_columnStack == cs) {
                D.log(string.Format("disabler for gameobject {0}", this.gameObject.name.ToString()));
                toBeDisabled.SetActive(false);
            } else {
                D.log("no match");
            }
        } else {
            D.log("cs was null");
        }
    }
}
