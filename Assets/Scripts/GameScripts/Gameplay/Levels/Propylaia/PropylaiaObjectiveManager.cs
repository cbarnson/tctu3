using UnityEngine;
using System.Collections;
using System;

public class PropylaiaObjectiveManager : MonoBehaviour {

    bool[] solved = new bool[] { false, false, false, false };

	void Start() {
        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    void OnDestroy() {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    private void onMessageEvent(string message, string metadata) {
        if (message == "puzzleComplete") {
            // guard dialogue
            solved[0] = true;
            ObjectiveStatusUpdate();
        } else if (message == "statueComplete") {
            // statue slider
            solved[1] = true;
            ObjectiveStatusUpdate();
        } else if (message == "columnComplete") {
            // column builder
            solved[2] = true;
            ObjectiveStatusUpdate();
        } else if (message == "laocoonOracleComplete") {
            // laocoon
            solved[3] = true;
            ObjectiveStatusUpdate();
        }
    }

    private void ObjectiveStatusUpdate() {
        if (Array.TrueForAll<bool>(solved, IsSolved)) {
            // level is complete
            util.tctu.loadNextScene();
        }
    }

    private bool IsSolved(bool b) {
        return b;
    }
}
