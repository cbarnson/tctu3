using UnityEngine;
using System.Collections;
using System;

public class GuardManager : MonoBehaviour {

    //private Guard [] guards;
    public Guard[] guards;

    //void Awake () {
    //    guards = GetComponentsInChildren<Guard> (true);
    //}

    void Start () {
        Dialoguer.events.onTextPhase += onTextPhase;
    }

    void OnDestroy () {
        Dialoguer.events.onTextPhase -= onTextPhase;
    }

    private void onTextPhase (DialoguerTextData data) {
        // if there is some metadata
        if (data.metadata != "") {
            switch (data.metadata) {
                case "0":
                    lookAtGuard (0);
                    break;
                case "1":
                    lookAtGuard (1);
                    break;
                case "2":
                    lookAtGuard (2);
                    break;
            }
        }
    }

    //void Update () {
    //    // temporary for testing
    //    if (Input.GetKeyDown(KeyCode.Q)) {
    //        D.log ("looking at guard 0");
    //        lookAtGuard (0);
    //    } else if (Input.GetKeyDown(KeyCode.W)) {
    //        D.log ("looking at guard 1");
    //        lookAtGuard (1);
    //    } else if (Input.GetKeyDown(KeyCode.E)) {
    //        D.log ("looking at guard 2");
    //        lookAtGuard (2);
    //    }
    //}

    public void lookAtGuard(int guardNum) {
        //D.assert (!(guardNum >= guards.Length), "guardNum exceeds number of active guards", true);
        CameraController.focus (guards [guardNum].transform.position);
    }

}
