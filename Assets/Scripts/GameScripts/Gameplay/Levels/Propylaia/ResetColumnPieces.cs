using UnityEngine;
using System.Collections;
using System;

public class ResetColumnPieces : MonoBehaviour {

    public delegate void ResetColumnHandler();
    public static event ResetColumnHandler ResetColumns;

    

    public GameObject[] columnPiece;
    public Transform[] deleteChildren;
    public Transform parentForNewPieces;

    private Vector3[] startPositions;
    private Quaternion[] startRotations;
    private bool _enabled = true;


    private void Start() {
        // save all the initial positions of the child objects of parentForNewPieces for use later
        startPositions = new Vector3[columnPiece.Length];
        startRotations = new Quaternion[columnPiece.Length];
        for (int i = 0; i < parentForNewPieces.childCount; i++) {
            startPositions[i] = parentForNewPieces.GetChild(i).position;
            startRotations[i] = parentForNewPieces.GetChild(i).rotation;
        }
        //ColumnStackManager.ColumnPuzzleComplete += onColumnPuzzleComplete;
    }

    private void OnDestroy() {
        //ColumnStackManager.ColumnPuzzleComplete -= onColumnPuzzleComplete;
    }

    // once puzzle is complete, do not allow resets
    //private void onColumnPuzzleComplete(object sender, ColumnPuzzleCompleteEventArgs e) {
    //    _enabled = false;
    //}

    private void OnMouseDown() {
        if (!_enabled) { return; }
        killChildren();
        createPieces();
        onResetColumns();
    }

    private void onResetColumns() {
        if (ResetColumns != null) {
            ResetColumns();
        }
    }

    private void createPieces() {
        for (int i = 0; i < columnPiece.Length; i++) {
            GameObject obj = (GameObject)Instantiate(columnPiece[i], startPositions[i], startRotations[i]);
            obj.transform.parent = parentForNewPieces;
        }
    }

    private void killChildren() {
        for (int i = 0; i < deleteChildren.Length; i++) {
            for (int j = 0; j < deleteChildren[i].childCount; j++) {
                Destroy(deleteChildren[i].GetChild(j).gameObject);
            }
        }
    }
}
