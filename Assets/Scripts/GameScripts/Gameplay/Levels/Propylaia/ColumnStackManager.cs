using UnityEngine;
using System.Collections;
using System;

// class to hold info when the ENTIRE puzzle is done, i.e. all three columns
public class dColumnPuzzleCompleteEventArgs : System.EventArgs {

}

public class ColumnStackManager : MonoBehaviour {

    //public static event EventHandler<ColumnPuzzleCompleteEventArgs> ColumnPuzzleComplete;

    //private int _counter = 0;

    // Use this for initialization
 //   private void Start () {
 //       ColumnStack.ColumnComplete += onColumnComplete;
 //       ResetColumnPieces.ResetColumns += onResetColumns;
	//}

 //   private void OnDestroy() {
 //       ColumnStack.ColumnComplete -= onColumnComplete;
 //       ResetColumnPieces.ResetColumns -= onResetColumns;
 //   }

 //   private void onResetColumns() {
 //       DebugPrintCompletedCount();
 //       _counter = 0;
 //   }

 //   private void onColumnComplete(object sender, ColumnCompleteEventArgs e) {
 //       _counter++;
 //       DebugPrintCompletedCount();
 //       if (_counter > 2) {
 //           onColumnPuzzleComplete(new ColumnPuzzleCompleteEventArgs());
 //       }
 //   }

 //   private void onColumnPuzzleComplete(ColumnPuzzleCompleteEventArgs e) {
 //       if (ColumnPuzzleComplete != null) {
 //           ColumnPuzzleComplete(this, e);
 //       }
 //   }

 //   private void DebugPrintCompletedCount() {
 //       D.log("columns completed: " + _counter);
 //   }
}
