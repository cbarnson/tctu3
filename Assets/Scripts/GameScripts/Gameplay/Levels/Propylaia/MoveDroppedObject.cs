using UnityEngine;
using System.Collections;

public class MoveDroppedObject : MonoBehaviour {

    public float moveSpeed = 20f;
    public float yDropHeight = 20f;

    private Rigidbody _rigidbody;
    private Vector3 _target;

    private void Awake () {
        _rigidbody = GetComponent<Rigidbody> ();
    }

	// Use this for initialization
	void Start () {
        WorldObjectDragHandler.onObjectDrop += onObjectDrop;
	}

    private void OnDestroy () {
        WorldObjectDragHandler.onObjectDrop -= onObjectDrop;
    }

    private void onObjectDrop (GameObject obj) {
        if (obj.transform.parent == transform) {
            // temporarily disable the functionality of SetParentOnEnter
            SetParentOnEnter.triggerEnabled = false;

            Rigidbody rb = obj.GetComponent<Rigidbody> ();
            if (rb != null) { rb.isKinematic = true; }
            _target = new Vector3 (0, 20f, 0);
            StopAllCoroutines ();
            StartCoroutine (Move (obj));
        }
    }
	
    IEnumerator Move (GameObject obj) {
        while (obj.transform.localPosition != _target) {
            obj.transform.localPosition = Vector3.MoveTowards (obj.transform.localPosition, _target, moveSpeed * Time.deltaTime);
            yield return null;
        }
        //D.log ("setting isKinematic to false");
        Rigidbody rb = obj.GetComponent<Rigidbody> ();
        if (rb != null) { rb.isKinematic = false; }
        // re-enable the functionality of SetParentOnEnter
        SetParentOnEnter.triggerEnabled = true;
        yield return null;
    }   

}
