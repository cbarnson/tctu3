using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

// class to manage dialogue puzzle in propylaia, should control turning off "continue" button
// and replace with 3 buttons for the response choices
public class PropylaiaPuzzle : MonoBehaviour {

    public bool modeBasic = true;
    public Fade responseBasic;
    public Fade responseChoice;
    public Text[] responseChoiceText;

    private CanvasGroup _responseBasicCanvasGroup;
    private CanvasGroup _responseChoiceCanvasGroup;

    void Awake () {
        D.assert (responseBasic != null, "PropylaiaPuzzle, responseBasic member is null", true);
        D.assert (responseChoice != null, "PropylaiaPuzzle, responseChoice member is null", true);
        _responseBasicCanvasGroup = responseBasic.gameObject.GetComponent<CanvasGroup> ();
        _responseChoiceCanvasGroup = responseChoice.gameObject.GetComponent<CanvasGroup> ();
        // make basic the default
        _responseBasicCanvasGroup.interactable = true;
        _responseBasicCanvasGroup.alpha = 1.0f;
        _responseBasicCanvasGroup.blocksRaycasts = true;
        // initial values for choice
        _responseChoiceCanvasGroup.interactable = false;
        _responseChoiceCanvasGroup.alpha = 0.0f;
        _responseChoiceCanvasGroup.blocksRaycasts = false;
    }

    void Start () {
        Dialoguer.events.onTextPhase += onTextPhase;
    }

    void OnDestroy () {
        Dialoguer.events.onTextPhase -= onTextPhase;
    }

    private void onTextPhase (DialoguerTextData data) {
        string [] _choices = data.choices;
        if (_choices == null) {
            //D.log ("no choices");
            if (!modeBasic) { switchToBasic (); }
        } else {
            //D.log ("there are choices");
            if (modeBasic) { switchToChoice (); }
            for (int i = 0; i < responseChoiceText.Length; i++) {
                responseChoiceText [i].text = _choices [i];
            }
        }
    }

    public void switchToChoice () {
        // fade in choice
        responseChoice.CancelRoutines ();
        responseChoice.FadeMeIn ();
        _responseChoiceCanvasGroup.interactable = true;
        _responseChoiceCanvasGroup.blocksRaycasts = true;
        // fade out basic
        responseBasic.CancelRoutines ();
        responseBasic.FadeMeOut ();
        _responseBasicCanvasGroup.interactable = false;
        _responseBasicCanvasGroup.blocksRaycasts = false;
        // change mode
        modeBasic = false;
    }

    public void switchToBasic () {
        // fade in basic
        responseBasic.CancelRoutines ();
        responseBasic.FadeMeIn ();
        _responseBasicCanvasGroup.interactable = true;
        _responseBasicCanvasGroup.blocksRaycasts = true;
        // fade out choice
        responseChoice.CancelRoutines ();
        responseChoice.FadeMeOut ();
        _responseChoiceCanvasGroup.interactable = false;
        _responseChoiceCanvasGroup.blocksRaycasts = false;
        // change mode
        modeBasic = true;
    }
}
