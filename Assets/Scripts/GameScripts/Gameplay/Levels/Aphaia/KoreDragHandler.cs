using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class KoreDragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    [HideInInspector]
    public bool droppedCorrectly = false;
    public string periodName;

    private Vector2 _startPos;
    private RectTransform _rectTransform;

    void Awake () {
        _rectTransform = GetComponent<RectTransform>();
        _startPos = _rectTransform.anchoredPosition;
    }

    public void OnBeginDrag (PointerEventData eventData) {
        if (GUIManager.IsPaused) { return; }
        Debug.Log("on begin drag");
    }

    public void OnDrag (PointerEventData eventData) {
        if (GUIManager.IsPaused) { return; }
        transform.position = eventData.position;
    }

    public void OnEndDrag (PointerEventData eventData) {
        Debug.Log("on end drag");
        if (!droppedCorrectly) {
            Debug.Log("reset position");
            StartCoroutine(resetPosition());
        }
    }

    IEnumerator resetPosition () {
        while (_rectTransform.anchoredPosition != _startPos) {
            _rectTransform.anchoredPosition = Vector2.MoveTowards(_rectTransform.anchoredPosition, _startPos, 600f * Time.deltaTime);
            yield return null;
        }
        yield return null;
    }
}
