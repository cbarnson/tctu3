using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class KoreDropHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler {

    public string periodName;

    private Image _image;
    private CanvasGroup _canvasGroup;
    private float _fadeRate = 5.0f;                     // fade rate of dropzone highlight on timeline periods

	// Use this for initialization
	void Start () {
        _image = GetComponent<Image>();
        _image.enabled = true;
        _canvasGroup = GetComponent<CanvasGroup>();
        _canvasGroup.alpha = 0.0f;
    }
	
    private void showImage() {
        StopAllCoroutines();
        StartCoroutine(fadeIn());
    }

    private void hideImage() {
        StopAllCoroutines();
        StartCoroutine(fadeOut());
    }

    public void OnDrop (PointerEventData eventData) {
        KoreDragHandler _dragging = eventData.pointerDrag.gameObject.GetComponent<KoreDragHandler>();
        if (!_dragging) { return; }
        if (_dragging.periodName == periodName) {
            D.log("correct, names match");
            _dragging.droppedCorrectly = true;
            Destroy(_dragging.gameObject);
        } else {
            // incorrect
            KorePuzzle.m_errorCount++;
            KorePuzzle.m_fade.FadeMeInOut();
        }
    }

    public void OnPointerEnter (PointerEventData eventData) {
        showImage();
    }

    public void OnPointerExit (PointerEventData eventData) {
        hideImage();
    }

    IEnumerator fadeIn() {
        while (_canvasGroup.alpha < 1.0f) {
            _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 1.0f, _fadeRate * Time.deltaTime);
            yield return null;
        }
        yield return null;
    }

    IEnumerator fadeOut() {
        while (_canvasGroup.alpha > 0.0f) {
            _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 0.0f, _fadeRate * Time.deltaTime);
            yield return null;
        }
        yield return null;
    }

    

}
