using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class KorePuzzle : MonoBehaviour {

    public static int m_errorCount = 0;         // number of errors during the puzzle
    public static Fade m_fade;                  // canvas with Fade component for error flash

    public GameObject aphrodite;                // UI sprite objects
    public GameObject peplos;
    public int completionDialogue = 22;         // dialogue after the Kore Puzzle is completed
    public Fade fade;                           // to be assigned in inspector, and hooked to m_fade

    private Canvas _canvas;
    private bool _isComplete = false;

    void Awake () {
        _canvas = GetComponent<Canvas>();
    }

    void Start () {
        Assert.IsNotNull (aphrodite);
        Assert.IsNotNull (peplos);
        aphrodite.SetActive (false);
        peplos.SetActive (false);
        _canvas.enabled = false;

        // set m_fade
        m_fade = fade;

        // events to listen to
        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    void OnDestroy () {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    private void onMessageEvent (string message, string metadata) {
        if (message == "startKorePuzzle") {
            startPuzzle ();


            // OVERRIDE
            GUICrosshair.CrosshairCursorLocked = true;
            GUITimeline.Instance.Show();
            GUIFieldbook.FieldbookTimelineLocked = true;

            // send out UIPuzzle event
            UIPuzzleEventArgs e = new UIPuzzleEventArgs();
            e.ErrorCount = 0;                       // initial error count of 0
            e.LevelName = "aphaia";
            e.PuzzleName = "korePuzzle";
            UIPuzzle.onPuzzleStarted(this, e);
        }
    }

    void Update () {
        if (!aphrodite && !peplos && !_isComplete) {
            D.log("puzzle complete");
            _canvas.enabled = false;
            _isComplete = true;

            // send out UIPuzzle ended event
            UIPuzzleEventArgs e = new UIPuzzleEventArgs();
            e.ErrorCount = m_errorCount;
            e.LevelName = "aphaia";
            UIPuzzle.onPuzzleEnded(this, e);


            // OVERRIDE
            GUICrosshair.CrosshairCursorLocked = false;
            GUITimeline.Instance.Hide();
            GUIFieldbook.FieldbookTimelineLocked = false;
            // deactivate Timeline

            //===============================
            //Timeline.m_canvas.gameObject.GetComponent<Timeline>().deactivate();

            // start the dialogue that follows from completion of the Kore Puzzle

            GUIDialogue.Instance.HandleDialogueStartRequest(completionDialogue);

            //Dialoguer.StartDialogue(completionDialogue);
        } 
    }

    public void startPuzzle() {
        D.log("kore start puzzle");
        _canvas.enabled = true;
        aphrodite.SetActive(true);
        peplos.SetActive(true);
    }

}
