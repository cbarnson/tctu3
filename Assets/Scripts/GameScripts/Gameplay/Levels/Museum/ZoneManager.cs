using UnityEngine;
using System;
using System.Collections;

public class ZoneManager : MonoBehaviour {

    public delegate void ZoneManagerEventHandler(int newDialogueID);
    public static event ZoneManagerEventHandler ZoneChanged;

    public static event EventHandler ZoneMatchCompleted;

    public zone[] zones;

    public int CurrentDialogue {
        get {
            if (isComplete()) {
                if (isCorrect()) {
                    OnZoneMatchCompleted();
                    return _dialogues[0];
                } else {
                    return _dialogues[1];
                }
            } else {
                return _dialogues[2];
            }
        }
    }

    protected virtual void OnZoneMatchCompleted() {
        if (ZoneMatchCompleted != null) {
            ZoneMatchCompleted(this, EventArgs.Empty);
        }
    }

    // zones send message upwards to call this
    private void onZoneChanged() {
        if (ZoneChanged != null) {
            ZoneChanged(CurrentDialogue);
        }
    }

    private int[] _dialogues = {
        (int)DialoguerDialogues.museum_collectress_good,
        (int)DialoguerDialogues.museum_collectress_bad,
        (int)DialoguerDialogues.museum_collectress_missing
    };

    // a warning to the user
    private void Awake() {
        D.assert(zones.Length == 4, "ZoneManager must contain 4 Zone elements in zones array", true);
    }

	// Update is called once per frame
	void Update () {
        updateColliders();
	}

    //private void OnTransformChildrenChanged() {
    //    D.log("on transform children changed");
    //    onZoneChanged();
    //}

    private bool isCorrect() {
        for (int i = 0; i < zones.Length; i++) {
            // has a child, and the child's baseItem component's ID does not match the zone's ID
            if (zones[i].transform.childCount > 0 &&
                (zones[i].gameObject.GetComponent<zone>()._zoneID != 
                zones[i].gameObject.GetComponentInChildren<baseItem>()._ID)) {
                return false;
            }
        }
        D.log("puzzle correct");
        return true;
    }

    private bool isComplete() {
        for (int i = 0; i < zones.Length; i++) {
            if (zones[i].transform.childCount == 0) {
                return false;
            }
        }
        return true;
    }

    private void updateColliders() {
        // disable or enable the colliders for the zones based on whether or not they own a child
        for (int i = 0; i < zones.Length; i++) {
            if (zones[i].transform.childCount > 0) {
                zones[i].disableHitbox();
            } else {
                zones[i].enableHitbox();
            }
        }
    }
}
