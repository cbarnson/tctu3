using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

public class zone : MonoBehaviour {

    public int _zoneID;

    private Collider _collider;

    void Start() {
        _collider = gameObject.GetComponent<Collider>();
    }

    public void OnTransformChildrenChanged() {
        D.log("zone, children changed");
        SendMessageUpwards("onZoneChanged", null, SendMessageOptions.RequireReceiver);
    }

    public void disableHitbox() {
        _collider.enabled = false;
    }

    public void enableHitbox() {
        _collider.enabled = true;
    }

}
