using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

using util;

public class museManager : MonoBehaviour {

    public static int NUMBER_OF_ARTIFACTS = 4;
    public static string[] ArtifactFile = { "dipylon_info", "inanna_info", "primaporta_info", "palette_info" };
    public static string[] LabelFile = { "artifactIcon0", "artifactIcon1", "artifactIcon2", "artifactIcon3" };

    public static GameObject[] Artifact = new GameObject[NUMBER_OF_ARTIFACTS];
    public static GameObject[] Label = new GameObject[NUMBER_OF_ARTIFACTS];
    public static Vector3 artifact_position_on_stand = new Vector3(0, 0.85f, 0);
    public static Quaternion artifact_rotation_on_stand = Quaternion.Euler(-40, 0, 0);

    void Awake() {
        loadArtifacts();
        loadLabels();
    }

    private void loadArtifacts() {
        for (int i = 0; i < ArtifactFile.Length; i++) {
            Artifact[i] = Resources.Load(ArtifactFile[i], typeof(GameObject)) as GameObject;
            Assert.IsNotNull(Artifact[i]);
        }
    }

    private void loadLabels() {
        for (int i = 0; i < LabelFile.Length; i++) {
            Label[i] = Resources.Load(LabelFile[i], typeof(GameObject)) as GameObject;
            Assert.IsNotNull(Label[i]);
        }
    }

    

}
