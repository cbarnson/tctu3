using UnityEngine;
using System.Collections;
using System;

public class ForceDialogueOnStart : MonoBehaviour {

    public Transform lookTarget;
    public DialoguerDialogues dialogueID;
    public float delayFromStart = 0.1f;

    //private const string _messageEventEndLevel = "loadNextLevel";

	// Use this for initialization
	protected void Start () {
        D.assert(lookTarget != null);
        //Dialoguer.events.onMessageEvent += OnMessageEvent;
        StartCoroutine(StartDialogueAfterDelay());
	}

    protected void OnDestroy() {
        //Dialoguer.events.onMessageEvent -= OnMessageEvent;
    }

    //private void OnMessageEvent(string message, string metadata) {
    //    if (message.Equals(_messageEventEndLevel, StringComparison.Ordinal)) {
    //        util.tctu.loadNextScene();
    //    }
    //}

    IEnumerator StartDialogueAfterDelay() {
        yield return new WaitForSeconds(delayFromStart);
        GUIDialogue.Instance.HandleDialogueStartRequest((int)dialogueID, lookTarget.position);
    }
	
	
}
