using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class PuzzleSelect : MonoBehaviour {

    // static members
    private static int _incorrectCounter = 0;

    private int[] _incorrectDialogue = {
        (int)DialoguerDialogues.acropolis_oracle_puzzleTwo_wrong1,
        (int)DialoguerDialogues.acropolis_oracle_puzzleTwo_wrong2,
        (int)DialoguerDialogues.acropolis_oracle_puzzleTwo_wrong3
    };

    // the 3 buttons for each temple
    public Button _optionA;
    public Button _optionB;
    public Button _optionC;

    private Canvas _canvas;
    private bool _inventoryOpen;

    void Awake () {
        _canvas = GetComponent<Canvas>();
        _canvas.enabled = false;
        _inventoryOpen = false;
    }

    void Start () {
        Dialoguer.events.onMessageEvent += onMessageEvent;
        InventoryBehaviour.InventoryEntered += OnInventoryEntered;
        InventoryBehaviour.InventoryExited += OnInventoryExited;
        PauseableStateMachineBehaviour.PauseEntered += OnPauseEntered;
        PauseableStateMachineBehaviour.PauseExited += OnPauseExited;
    }

    void OnDestroy () {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
        InventoryBehaviour.InventoryEntered -= OnInventoryEntered;
        InventoryBehaviour.InventoryExited -= OnInventoryExited;
        PauseableStateMachineBehaviour.PauseEntered -= OnPauseEntered;
        PauseableStateMachineBehaviour.PauseExited -= OnPauseExited;
    }

    private void OnPauseEntered(object sender, EventArgs e) {
        SetButtonInteractivity(false);
    }

    private void OnPauseExited(object sender, EventArgs e) {
        if (!_inventoryOpen)
            SetButtonInteractivity(true);
    }

    private void OnInventoryEntered(object sender, EventArgs e) {
        _inventoryOpen = true;
        SetButtonInteractivity(false);
    }

    private void OnInventoryExited(object sender, EventArgs e) {
        _inventoryOpen = false;
        SetButtonInteractivity(true);
    }

    private void SetButtonInteractivity(bool targetState) {
        _optionA.interactable = targetState;
        _optionB.interactable = targetState;
        _optionC.interactable = targetState;
    }

    //private void Update () {
    //    if (m_canvas.enabled && Crosshair.m_canvas.enabled) {
    //        // disable the crosshair while puzzle active
    //        Crosshair.deactivate();
    //    }
    //}

    private void onMessageEvent (string message, string metadata) {
        if (message == "startPuzzleTwo") {

            // OVERRIDE
            GUICrosshair.CrosshairCursorLocked = true;

            // open canvas to perform multiple choice puzzle
            _canvas.enabled = true;

            // puzzle start event for PuzzleSelect (i.e. choose one of three temples)
            UIPuzzleEventArgs e = new UIPuzzleEventArgs();
            e.ErrorCount = 0;
            e.LevelName = "acropolis";
            e.PuzzleName = "puzzleSelect";
            UIPuzzle.onPuzzleStarted(this, e);
        }
    }

    // each time an incorrect option is selected, increment the incorrect counter, and play appropriate dialogue
    public void playIncorrectDialogue() {
        _canvas.enabled = false;

        GUIDialogue.Instance.HandleDialogueStartRequest(_incorrectDialogue[_incorrectCounter]);

        _incorrectCounter++;
        if (_incorrectCounter > _incorrectDialogue.Length - 1) {
            _incorrectCounter = _incorrectDialogue.Length - 1;
        }
    }

    public void playCorrectDialogue() {
        // close this puzzle's canvas
        _canvas.enabled = false;

        GUIDialogue.Instance.HandleDialogueStartRequest((int)DialoguerDialogues.acropolis_oracle_puzzleTwo_good);

        // OVERRIDE 
        GUICrosshair.CrosshairCursorLocked = false;

        //Dialoguer.StartDialogue((int)DialoguerDialogues.acropolis_oracle_puzzleTwo_good);

        // event call for puzzle completed correctly -- unlocks the crosshair
        UIPuzzleEventArgs e = new UIPuzzleEventArgs();
        e.ErrorCount = _incorrectCounter;
        e.LevelName = "acropolis";
        UIPuzzle.onPuzzleStarted(this, e);
    }

}
