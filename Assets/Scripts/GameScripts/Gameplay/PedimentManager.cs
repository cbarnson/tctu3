using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

// structure
//==========
// Pediment
//      startPosition
//      endPosition
//      PedimentBase
//          slot0
//              figureX
//          ...

public class PedimentManager : MonoBehaviour {

    public Transform front;             // position of the main pediment when puzzle is active
    public Transform back;              // position of the main pediment when attach to temple
    public SliderManager sliderManager;

    private float _moveRate = 10f;      // speed of pediment when changing positions

    private void Start() {
        Dialoguer.events.onMessageEvent += onMessageEvent;
        sliderManager.SliderEnd += onSliderEnd;
    }

    // slider puzzle complete, return the sliderManager gameObject to its original starting point
    private void onSliderEnd(object sender, SliderEventArgs e) {
        StartCoroutine(LerpUtility.MoveTowards_V3(sliderManager.gameObject, back.position, _moveRate));
    }

    private void onMessageEvent(string message, string metadata) {
        if (message == "startTemplePuzzle") {
            sliderManager.onSliderStart();

            // move the sliderManager gameObject
            StartCoroutine(LerpUtility.MoveTowards_V3(sliderManager.gameObject, front.position, _moveRate));
        }
    }
}