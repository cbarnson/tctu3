using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using System.Collections;
using System;

public class PickupEventArgs : EventArgs {
    public bool firstPickup { get; set; }
}

public class baseItem : MonoBehaviour {

    public static event EventHandler<PickupEventArgs> ItemPickup;

    public bool pickable = false;
    public int _ID;
    public string _date;
    public string _culture;

    private bool firstPick = false;

    private void Start () {
        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    private void OnDestroy () {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    protected virtual void OnItemPickup() {
        if (ItemPickup != null) {
            ItemPickup(this, new PickupEventArgs() { firstPickup = firstPick });
        }
    }



    private void onMessageEvent (string message, string metadata) {
        if (message == "makeInteractable") {
            pickable = true;
            firstPick = true;
        }
    }

    public void OnDisable() {
        string [] temp = { };
        EventManagerHUD.displayDescription (temp);
    }

    private void OnMouseDown () {
        if (pickable) {
            //if (firstPick) {
            //    TaskManager.Instance.AdvanceTask(TaskType.museum_pickup);
            //}
            OnItemPickup();
            GameObject obj = Instantiate(museManager.Label[_ID]);
            GUIFieldbook.addToInventory(obj);
            Destroy(gameObject);
        }
    }

    private void OnMouseEnter() {
        //if (Fieldbook.m_canvas.enabled || Pause.m_canvas.enabled) { return; }
        string [] temp = { _date, _culture };
        EventManagerHUD.displayDescription (temp);
    }

    private void OnMouseExit() {
        //if (Fieldbook.m_canvas.enabled || Pause.m_canvas.enabled) { return; }
        string [] temp = { };
        EventManagerHUD.displayDescription (temp);
    }

}
