﻿using UnityEngine;
using System.Collections;

public class EndlessRotate : MonoBehaviour {

    public float degreesPerUpdate = 10f;

    protected void Update() {
        transform.RotateAround(transform.position, Vector3.up, degreesPerUpdate * Time.deltaTime);
    }

}
