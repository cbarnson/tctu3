﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(HeadLookController))]
public class HeadLookTarget : MonoBehaviour {

    public Transform target;
    private HeadLookController _headlook;

    private void Start () {
        _headlook = GetComponent<HeadLookController>();
    }

    private void LateUpdate () {
        if (target != null) {
            _headlook.target = target.position;
        }
    }

    public void setTarget(Transform t) {
        target = t;
    }

    public void setTargetToPlayer() {
        target = Player.m_camera.transform;
    }
}
