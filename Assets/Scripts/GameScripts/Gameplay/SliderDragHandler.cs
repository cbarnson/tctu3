using UnityEngine;
using System.Collections;

public class SliderDragHandler : MonoBehaviour {

    public int ID;

    public bool lockPositionX = false;
    public bool lockPositionY = false;
    public bool lockPositionZ = false;

    private float _moveRate = 4f;
    private float _distToScreen;
    //private bool _holding = false;
    //private Rigidbody _rigidbody;

    public bool Locked { get; set; }            // starts out locked

    private void Awake() {
        //_rigidbody = GetComponent<Rigidbody>();
        Locked = true;
    }

    public void moveToCenter() {
        StartCoroutine(LerpUtility.MoveTowardsLocal_V3(gameObject, Vector3.zero, _moveRate));
    }

    private void OnMouseDown() {
        if (Locked) { return; }
        _distToScreen = Camera.main.WorldToScreenPoint(transform.position).z;
        //_holding = true;
    }

    private void OnMouseDrag() {
        if (Locked) { return; }
        transform.position = FPController.Instance.cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _distToScreen));
        float x = transform.localPosition.x;
        float y = transform.localPosition.y;
        float z = transform.localPosition.z;
        if (lockPositionX) { x = 0; }
        if (lockPositionY) { y = 0; }
        if (lockPositionZ) { z = 0; }
        transform.localPosition = new Vector3(x, y, z);
    }

    //void OnMouseUpAsButton() {
    //    D.log("on mouse up as button");
    //    moveToCenter();
    //    _holding = false;
    //}

    private void OnMouseUp() {
        moveToCenter();
        //D.log("on mouse up");
        //if (_holding) {
        //    moveToCenter();
        //}
        //_holding = false;
    }
}
