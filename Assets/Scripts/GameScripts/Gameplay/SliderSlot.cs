using UnityEngine;
using System.Collections;

public class SliderSlot : MonoBehaviour {

    public int ID;

    void OnTriggerEnter(Collider other) {
        if (other.GetComponent<SliderDragHandler>() as SliderDragHandler != null) {
            D.assert(transform.childCount > 0);

            // set this objects child's parent to other's parent
            GameObject obj = transform.GetChild(0).gameObject;
            obj.transform.parent = other.transform.parent;
            obj.GetComponent<SliderDragHandler>().moveToCenter();

            // set other's parent to this
            other.transform.parent = transform;
        }
    }

    // helper to set the state of first child with a SliderDragHandler component
    public void setDragHandlerLockedState(bool state) {
        if (transform.childCount > 0) {
            SliderDragHandler sdh = transform.GetChild(0).GetComponent<SliderDragHandler>();
            if (sdh != null) {
                sdh.Locked = state;
            }
        }
    }
}
