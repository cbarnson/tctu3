using UnityEngine;
using System.Collections;


public class FPFootstepManager : MonoBehaviour {

    public enum GroundType {
        DIRT,
        GRASS,
        GRAVEL,
        HARDSURFACE,
        MARBLE,
        WOOD,
        OTHER
    }

    public AudioClip jumpClip;
    public AudioClip landClip;
    public AudioClip[] dirtClips;
    public AudioClip[] grassClips;
    public AudioClip[] gravelClips;
    public AudioClip[] hardsurfaceClips;
    public AudioClip[] marbleClips;
    public AudioClip[] woodClips;
    public AudioClip[] otherClips;
    public float footstepVolume = 1f;
    public float jumpVolume = 1f;
    public float landVolume = 1f;

    public bool IsJump {
        get { return _isJump; }
    }
    public bool IsGrounded {
        get { return _isGrounded; }
    }

    private AudioSource _audioSource;
    private GroundType _currentGroundType = GroundType.OTHER;
    private bool _isJump = false;
    private bool _isGrounded = true;

    private void Awake() {
        _audioSource = GetComponent<AudioSource>();
    }

    // called from FPController each FixedUpdate whenever _grounded is true and velocityChange is not Vector3.zero
    public void PlayFootstepAudio() {
        // not playing, or the previous is done, so choose the next
        if (!_audioSource.isPlaying && !_isJump) {          
            if (_currentGroundType == GroundType.DIRT) {                    // DIRT
                SelectRandomPlay(ref dirtClips);
            } else if (_currentGroundType == GroundType.GRASS) {            // GRASS
                SelectRandomPlay(ref grassClips);
            } else if (_currentGroundType == GroundType.GRAVEL) {           // GRAVEL
                SelectRandomPlay(ref gravelClips);
            } else if (_currentGroundType == GroundType.HARDSURFACE) {      // HARDSURFACE
                SelectRandomPlay(ref hardsurfaceClips);
            } else if (_currentGroundType == GroundType.MARBLE) {           // MARBLE
                SelectRandomPlay(ref marbleClips);
            } else if (_currentGroundType == GroundType.WOOD) {             // WOOD
                SelectRandomPlay(ref woodClips);
            } else {                                                        // OTHER
                SelectRandomPlay(ref otherClips);
            }
        }
    }

    public bool IsValidJump() {
        if (_isGrounded && !_isJump) {
            PlayAudioJump();
            _isJump = true;
            _isGrounded = false;
            return true;
        }
        return false;
    }

    private void SelectRandomPlay(ref AudioClip[] clips) {
        int size = clips.Length;
        // choose random from 1 to clips.Length, set to audioSource, then swap so the same doesn't play multiple times
        int n = Random.Range(1, clips.Length);
        _audioSource.clip = clips[n];
        _audioSource.PlayOneShot(_audioSource.clip, footstepVolume);
        // swap
        clips[n] = clips[0];
        clips[0] = _audioSource.clip;
    }
    
    private void PlayAudioLand() {
        //D.log("PlayAudioLand, isGrounded = true");
        AudioSource.PlayClipAtPoint(landClip, transform.position, landVolume);
        _isGrounded = true;
        _isJump = false;
    }

    private void PlayAudioJump() {
        //D.log("PlayAudioJump, isGrounded = false");
        AudioSource.PlayClipAtPoint(jumpClip, transform.position, jumpVolume);
        _isGrounded = false;
        _isJump = true;
    }

    // occurs when landing from jump, or when entering a different collider (may or may not be different GroundType)
    private void OnTriggerEnter(Collider other) {
        if (_isJump) {
            if (!_isGrounded) {
                PlayAudioLand();
            }
            _isJump = false;
        }
        //if (_isJump && !_audioSource.isPlaying) {
        //    PlayAudioLand();
        //}
        //_isJump = false;
    }

    // called from OnTriggerEnter every time the 'feet' collider enters a new ground collider
    private void GetGroundType(string tag) {
        switch(tag) {
            case "Dirt":
                _currentGroundType = GroundType.DIRT;
                break;
            case "Grass":
                _currentGroundType = GroundType.GRASS;
                break;
            case "Gravel":
                _currentGroundType = GroundType.GRAVEL;
                break;
            case "Hardsurface":
                _currentGroundType = GroundType.HARDSURFACE;
                break;
            case "Marble":
                _currentGroundType = GroundType.MARBLE;
                break;
            case "Wood":
                _currentGroundType = GroundType.WOOD;
                break;
            default:
                _currentGroundType = GroundType.OTHER;
                break;
        }
    }


}
