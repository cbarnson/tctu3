using UnityEngine;
using System.Collections;

// not present in the scene, only facilitates message events to tell Player.cs to move the camera
public static class CameraController {

    public delegate void CameraViewHandler (Vector3 target);
    public static event CameraViewHandler onSpeakerFocus;

    public static void focus (Vector3 target) {
        if (onSpeakerFocus != null) {
            onSpeakerFocus (target);
        }
    }
}
