using UnityEngine;
using System.Collections;

public class FPMouseLook : MonoBehaviour {

    public float sensitivity = 2f;
    public float minimumX = -90f;           // max angle allowed to look UPWARDS
    public float maximumX = 90f;            // max angle allowed to look DOWNWARDS
    public bool clampVerticalRotation = true;

    private Quaternion _characterTargetRot;
    private Quaternion _cameraTargetRot;

    public void Init(Transform character, Transform camera) {
        _characterTargetRot = character.localRotation;
        _cameraTargetRot = camera.localRotation;
    }

    // called once per Update, from FPController, assuming mouse is not locked
    public void LookRotation(Transform character, Transform camera) {

        float yRot = Input.GetAxis("Mouse X") * sensitivity;
        float xRot = Input.GetAxis("Mouse Y") * sensitivity;

        _characterTargetRot *= Quaternion.Euler(0f, yRot, 0f);
        _cameraTargetRot *= Quaternion.Euler(-xRot, 0f, 0f);

        if (clampVerticalRotation)
            _cameraTargetRot = ClampRotationAroundXAxis(_cameraTargetRot);

        character.localRotation = _characterTargetRot;
        camera.localRotation = _cameraTargetRot;
    }


    Quaternion ClampRotationAroundXAxis(Quaternion q) {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
        angleX = Mathf.Clamp(angleX, minimumX, maximumX);
        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }

}
