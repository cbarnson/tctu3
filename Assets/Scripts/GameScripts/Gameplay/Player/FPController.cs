using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(FPMouseLook))]
public class FPController : MonoBehaviour {

    // test using bools to lock parts of player control
    public static bool isMouseLocked = false;
    public static bool isMoveLocked = false;

    public float speedFactor = 5.0f;
    public float maxSpeed = 10f;
    public float gravity = 10f;
    public bool canJump = true;
    public float jumpHeight = 2.0f;
    public Camera cam;

    private bool _grounded = false;
    private Rigidbody _rigidbody;
    private FPMouseLook _mouseLook;
    private FPFootstepManager _footstepManager;

    private static FPController _instance;
    public static FPController Instance {
        get {
            return _instance;
        }
    }

    public void HaltMovement() {
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = Vector3.zero;
    }

    private void Awake() {
        _instance = this;
        _mouseLook = GetComponent<FPMouseLook>();
        _mouseLook.Init(transform, cam.transform);
        _footstepManager = GetComponentInChildren<FPFootstepManager>();
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.freezeRotation = true;
        _rigidbody.useGravity = true;               // online said set to false, gives us more tuning control, but I think I want gravity
    }

    private void Start() {
        CameraController.onSpeakerFocus += onSpeakerFocus;
    }

    private void OnDestroy() {
        CameraController.onSpeakerFocus -= onSpeakerFocus;
    }

    private void onSpeakerFocus(Vector3 pos) {
        Vector3 targetPos = new Vector3(pos.x, cam.transform.position.y, pos.z);
        StopAllCoroutines();
        StartCoroutine(smoothLook(targetPos));
    }

    IEnumerator smoothLook(Vector3 target) {
        Vector3 current = cam.ScreenToWorldPoint(new Vector3(cam.pixelWidth / 2, cam.pixelHeight / 2, 0.1f));
        while (current != target) {
            current = Vector3.MoveTowards(current, target, 20f * Time.deltaTime);
            cam.transform.LookAt(current);
            yield return null;
        }
    }

    //private void onSpeakerFocus(Vector3 target) {
    //    // rotate player y only
    //    //Vector3 targetPos = new Vector3(target.x, this.transform.position.y, target.z);
    //    //StartCoroutine(PlayerLookAtYOnly(targetPos));

    //    // debug
    //    //D.log("cam local position: " + cam.transform.localPosition);
    //    //D.log("cam position: " + cam.transform.position);

    //    // zoom in camera only
    //    //FPController.isMoveLocked = true;
    //    //StartCoroutine(ZoomCameraToTarget(target));
    //}

    //private void onDialogueEnded() {
    //    // lock until camera panning finishes
    //    //D.log("locking mouse and move");
    //    //FPController.isMouseLocked = true;
    //    //FPController.isMoveLocked = true;

    //    //Vector3 targetPos = new Vector3(0f, 0.5f, 0f);
    //    //D.log("dialogue ended, zoom to start");
    //    //StartCoroutine(ZoomCameraToStart());

       
    //}

    //IEnumerator ZoomCameraToTarget(Vector3 target) {
    //    while (Vector3.Distance(cam.transform.position, target) > 1.0f) {
    //        cam.transform.position += cam.transform.forward * Time.deltaTime;
    //        //cam.transform.position = Vector3.MoveTowards(cam.transform.position, target, 5f * Time.deltaTime);
    //        yield return null;
    //    }
    //    D.log("zoom camera coroutine complete");
    //    yield return null;
    //}

    //IEnumerator ZoomCameraToStart() {
    //    while (cam.transform.localPosition != cameraStartPosition) {
    //        //cam.transform.localPosition += -cam.transform.forward * Time.deltaTime;
    //        cam.transform.localPosition = Vector3.MoveTowards(cam.transform.localPosition, cameraStartPosition, 5f * Time.deltaTime);
    //        yield return null;
    //    }
    //    D.log("zoom camera to start coroutine complete");
    //   // D.log("unlocking mouse and move");
    //    //FPController.isMouseLocked = false;
    //    //FPController.isMoveLocked = false;
    //    yield return null;
    //}

    //IEnumerator PlayerLookAtYOnly(Vector3 target) {
    //    while (this.transform.position.x != target.x || this.transform.position.z != target.z) {
    //        Vector3 curr = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
    //        curr = Vector3.MoveTowards(curr, target, 20f * Time.deltaTime);
    //        this.transform.LookAt(curr);
    //        yield return null;
    //    }
    //    D.log("player lookat y only complete");
    //    yield return null;
    //}

    private void Update() {
        // MOUSE LOOK AND CAMERA ROTATION
        if (!isMouseLocked) { UpdateMouseLook(); }
    }

    private void FixedUpdate() {
        // MOVEMENT, FOOTSTEP SOUNDS, JUMP SOUND
        if (!isMoveLocked && GUIManager.MovementEnabled) { FixedUpdateMovement(); }
    }

    // called once per physics update if Movement is not locked
    private void FixedUpdateMovement() {

        // movement --independent of grounded status

        // GetAxis returns a float between -1..1 for each component
        Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        targetVelocity = transform.TransformDirection(targetVelocity);
        // scale based on speedFactor
        targetVelocity *= speedFactor;
        // if mag of targetVelocity is greater than maxSpeed, clamp it down based on maxSpeed
        targetVelocity = Vector3.ClampMagnitude(targetVelocity, maxSpeed);
        // Apply a force that attempts to reach our target velocity
        Vector3 velocity = _rigidbody.velocity;
        Vector3 acceleration = (targetVelocity - velocity);
        acceleration.y = 0;
        // apply a force instantly as a velocity change
        _rigidbody.AddForce(acceleration, ForceMode.VelocityChange);

        if (_grounded) {
            // footsteps
            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) {
                _footstepManager.PlayFootstepAudio();
            }
            // jump
            if (Input.GetButton("Jump")) {
                if (_footstepManager.IsValidJump()) {
                    D.log("valid jump");
                    _rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
                }
                D.log("invalid");
            }
        }
        // reset grounded to false, updated to true by OnTriggerStay
        _grounded = false;

        // We apply gravity manually for more tuning control
        //_rigidbody.AddForce(new Vector3(0, -gravity * _rigidbody.mass, 0));

    }

    // called once per fixed update if mouse is not locked
    private void UpdateMouseLook() {
        if (!Cursor.visible) { _mouseLook.LookRotation(transform, cam.transform); }
    }


    private void OnTriggerStay(Collider other) {
        //D.log("FPController, OnTriggerStay other.name is " + other.name.ToString());
        _grounded = true;
    }

    float CalculateJumpVerticalSpeed() {
        // From the jump height and gravity we deduce the upwards speed 
        // for the character to reach at the apex.
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }
}
