using UnityEngine;
using System.Collections;

[RequireComponent(typeof(HeadLookController))]
public class HeadLookFollowPlayer : MonoBehaviour {

    private Transform _target;
    private HeadLookController _headlook;

    // Use this for initialization
    void Start () {
        _target = FPController.Instance.cam.transform;
        _headlook = GetComponent<HeadLookController>();
    }

    private void LateUpdate() {
        if (_target != null) {
            _headlook.target = _target.position;
        }
    }

}
