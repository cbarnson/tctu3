using UnityEngine;
using System.Collections;
using System;

public class StatueManager : MonoBehaviour {

    public SliderManager sliderManager;
    public DialoguerDialogues statueCompleteDialogue;

	// Use this for initialization
	void Start () {
        Dialoguer.events.onMessageEvent += onMessageEvent;
        sliderManager.SliderEnd += onSliderEnd;
	}

    void OnDestroy() {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
        sliderManager.SliderEnd -= onSliderEnd;
    }

    // on puzzle complete, start dialogue, no need to use CameraController.focus here
    private void onSliderEnd(object sender, SliderEventArgs e) {
        GUIDialogue.Instance.HandleDialogueStartRequestForced((int) statueCompleteDialogue);
    }

    private void onMessageEvent(string message, string metadata) {
        if (message == "startStatue") {
            // enables the sliders to become draggable
            sliderManager.onSliderStart();
        }
    }

}
