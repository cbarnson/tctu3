using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// allows public access to static members for speaker image and speaker mask
public class dialogueSpeaker : MonoBehaviour {

    public static RawImage m_rawImage;
    public static Mask m_mask;

    void Awake() {
        m_rawImage = GetComponent<RawImage>();
        m_mask = GetComponent<Mask>();
    }

    
}
