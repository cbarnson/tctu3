using UnityEngine;
using System.Collections;
using System;

public class OraclePropylaia : DialogueActor {

    public DialoguerDialogues columnCompleteDialogue;
    public Transform headLookTarget;
    private bool _disabled = false;

    protected override void Start() {
        Dialoguer.events.onMessageEvent += onDialogueMessageEvent;
        ColumnManager.ColumnPuzzleCompleted += OnColumnPuzzleCompleted;
    }

    private void OnColumnPuzzleCompleted(object sender, EventArgs e) {
        D.log("column puzzle has been completed, initiating dialogue with oracle");
        GUIDialogue.Instance.HandleDialogueStartRequest((int) columnCompleteDialogue, headLookTarget.position);
    }

    protected override void OnDestroy() {
        Dialoguer.events.onMessageEvent -= onDialogueMessageEvent;
        ColumnManager.ColumnPuzzleCompleted -= OnColumnPuzzleCompleted;
    }

    private void onDialogueMessageEvent(string message, string metadata) {
        if (!string.IsNullOrEmpty(message)) {
            if (message == "columnDialogueStart") {
                _disabled = true;
            }
        }
    }

    // for the dialogue BEFORE the column puzzle starts
    protected override void OnMouseDown() {
        if (_disabled) { return; }
        GUIDialogue.Instance.HandleDialogueStartRequest((int) actorDialogue, headLookTarget.position);
    }
}
