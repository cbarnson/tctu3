﻿using UnityEngine;
using System.Collections;
using System;

public class OracleAcropolis : DialogueActor {

    public Transform headLookTarget;
    private bool _disabled = false;

    protected override void Start() {
        Dialoguer.events.onMessageEvent += onDialogueMessageEvent;
    }

    private void onDialogueMessageEvent(string message, string metadata) {
        if (message == "startPuzzleTwo") {
            _disabled = true;
        }
    }

    protected override void OnMouseDown() {
        if (_disabled) { return; }
        GUIDialogue.Instance.HandleDialogueStartRequest((int)actorDialogue, headLookTarget.position);
    }
}
