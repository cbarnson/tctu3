using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Collider))]
public class DialogueZone : MonoBehaviour {

    public DialoguerDialogues zoneDialogue;
    public bool singleUse = true;

    private bool _dialogueComplete = false;

    protected virtual void Awake() {

    }

    protected virtual void Start() {

    }

    protected virtual void OnDestroy() {

    }

    protected virtual void OnTriggerEnter(Collider other) {
        if (singleUse && _dialogueComplete) { return; }
        if (other.tag == "Player") {
            // keep player from sliding when dialogue starts
            FPController.Instance.HaltMovement();
            GUIDialogue.Instance.HandleDialogueStartRequest((int)zoneDialogue);
            _dialogueComplete = true;
        }
    }

}