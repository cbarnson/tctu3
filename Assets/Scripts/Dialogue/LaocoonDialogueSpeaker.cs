﻿using UnityEngine;
using System.Collections;

public class LaocoonDialogueSpeaker : MonoBehaviour {

    public Transform headLookTarget;
    public laocoon_speaker speakerType;

	public enum laocoon_speaker {
        polydoros,
        athenodoros,
        hagesandros
    }
}
