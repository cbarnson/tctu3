using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// allows public access to static members for player image and player mask
public class dialoguePlayer : MonoBehaviour {

    public static RawImage m_rawImage;
    public static Mask m_mask;

    void Awake () {
        m_rawImage = GetComponent<RawImage>();
        m_mask = GetComponent<Mask>();
        m_mask.showMaskGraphic = false;
    }


}
