using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisableOnPause : MonoBehaviour {

    public Button _button;

	// Use this for initialization
	void Start () {
        Pause.onPause += onPause;
	}

    void OnDestroy () {
        Pause.onPause -= onPause;
    }

    public void onPause (bool isPauseEnabled) {
        if (isPauseEnabled) {
            // disable button
            _button.interactable = false;
        } else {
            // enable button
            _button.interactable = true;
        }
    }
	
}
