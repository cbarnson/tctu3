﻿using UnityEngine;
using System.Collections;
using System;

public class OracleAphaia : DialogueActor {

    public DialoguerDialogues actorDialoguePedimentComplete;
    public Transform headLookTarget;

    private SliderManager _sliderManager;
    private bool _firstDialogueComplete = false;

    protected override void Start() {
        _sliderManager = FindObjectOfType<SliderManager>();
        _sliderManager.SliderEnd += onSliderEnd;
    }

    protected override void OnMouseDown() {
        if (_firstDialogueComplete) { return; }
        if (GUIDialogue.Instance.IsDialogueStartRequestValid()) {
            base.OnMouseDown();
            _firstDialogueComplete = true;
        }
    }

    private void onSliderEnd(object sender, SliderEventArgs e) {
        GUIDialogue.Instance.HandleDialogueStartRequestForced((int)actorDialoguePedimentComplete, headLookTarget.position);
    }
}
