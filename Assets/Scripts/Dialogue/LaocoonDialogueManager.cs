﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class LaocoonDialogueManager : MonoBehaviour {

    public GameObject oracle;
    public CanvasGroup cvgFadeToBlackOnCompletion;

    public LaocoonDialogueSpeaker[] speakers;
    public Texture2D[] laocoonPortraits;        // should be size 5, with images labeled 1 through 5, in the same order in the array
    public CanvasGroup laocoonCanvasGroup;
    public RawImage laocoonRawImage;

    void Start() {
        laocoonCanvasGroup.alpha = 0.0f;
        Dialoguer.events.onTextPhase += onTextPhase;
        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    void OnDestroy() {
        Dialoguer.events.onTextPhase -= onTextPhase;
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    private void onMessageEvent(string message, string metadata) {
        if (message == "hide") {
            StopAllCoroutines();
            StartCoroutine(LerpUtility.FadeCanvasGroupOut(laocoonCanvasGroup));
        } else if (message == "show") {
            D.log("show event received");
            int index = 0;
            switch (metadata) {
                case "1":
                    index = 0;
                    break;
                case "2":
                    index = 1;
                    break;
                case "3":
                    index = 2;
                    break;
                case "4":
                    index = 3;
                    break;
                case "5":
                    index = 4;
                    break;
            }
            if (index > laocoonPortraits.Length) {
                D.log("error, laocoon puzzle texture index exceeds length of texture array");
                index = 0;
            }
            laocoonRawImage.texture = laocoonPortraits[index];
            StopAllCoroutines();
            StartCoroutine(LerpUtility.FadeCanvasGroupIn(laocoonCanvasGroup));
        } else if (message == "laocoonComplete") {
            // disable player movement
            FPController.isMoveLocked = true;
            FPController.isMouseLocked = true;
            StartCoroutine(LerpUtility.FadeCanvasGroupIn(cvgFadeToBlackOnCompletion, 2.0f));
            StartCoroutine(disableSpeakersEnableOracle(3.0f));
        }
    }

    IEnumerator fadeCanvasGroupOutAfterDelay(float t) {
        // wait time t, which should be long enough to allow the canvas group to fully fade in, wait for go's to be 
        // activated or deactivated, then proceed to fade out
        yield return new WaitForSeconds(t);

        // stop all routines, fade the dark cvg out
        StopAllCoroutines();
        StartCoroutine(LerpUtility.FadeCanvasGroupOut(cvgFadeToBlackOnCompletion, 2.0f));

        // restore movement again
        FPController.isMoveLocked = false;
        FPController.isMouseLocked = false;
    }

    IEnumerator disableSpeakersEnableOracle(float t) {
        // wait a short delay, enough to allow the blackout canvas to fade in
        yield return new WaitForSeconds(t);

        // disable each speaker gameobject
        foreach (LaocoonDialogueSpeaker s in speakers) {
            s.gameObject.SetActive(false);
        }

        // enable oracle gameobject
        oracle.SetActive(true);

        // after a delay, have the dark canvas fade out again, revealing our changes in gameobject active statuses
        StartCoroutine(fadeCanvasGroupOutAfterDelay(2.0f));
    }

    private void onTextPhase(DialoguerTextData data) {
        // if there is some metadata
        if (data.metadata != "") {
            string str = data.metadata;
            switch (str) {
                case "polydoros":
                    lookAtSpeaker(0);
                    break;
                case "athenodoros":
                    lookAtSpeaker(1);
                    break;
                case "hagesandros":
                    lookAtSpeaker(2);
                    break;
            }
        }
    }

    public void lookAtSpeaker(int n) {
        //D.assert (!(guardNum >= guards.Length), "guardNum exceeds number of active guards", true);
        CameraController.focus(speakers[n].headLookTarget.position);
    }
}
