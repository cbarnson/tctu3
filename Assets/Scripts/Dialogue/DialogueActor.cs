﻿using UnityEngine;
using System.Collections;

// base class for dialogue actors such as Collectress, Oracle, etc.
public class DialogueActor : MonoBehaviour {

    public DialoguerDialogues actorDialogue;

    protected virtual void Awake() {

    }

    protected virtual void Start() {

    }

    protected virtual void OnDestroy() {

    }

    protected virtual void OnMouseDown() {
        if (GUIManager.IsPaused) { return; }
        GUIDialogue.Instance.HandleDialogueStartRequest((int)actorDialogue);
    }
}
