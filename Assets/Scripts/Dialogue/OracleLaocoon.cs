﻿using UnityEngine;
using System.Collections;
using System;

public class OracleLaocoon : DialogueActor {

    //public DialoguerDialogues actorDialogue;
    public Transform headLookTarget;
    private bool _disabled = false;

    protected override void Start() {
        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    protected override void OnDestroy() {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    private void onMessageEvent(string message, string metadata) {
        if (!string.IsNullOrEmpty(message)) {
            if (message == "advanceSpeakLaocoonOracle") {
                _disabled = true;
            }
        }
    }

    protected override void OnMouseDown() { 
        if (_disabled) { return; }
        GUIDialogue.Instance.HandleDialogueStartRequest((int) actorDialogue, headLookTarget.position);
    }
}
