using UnityEngine;
using System.Collections;

public class NPCPropylaia : DialogueActor {

    private bool _dialogueComplete = false;

    protected override void OnMouseDown() {
        if (_dialogueComplete) { return; }
        if (GUIDialogue.Instance.IsDialogueStartRequestValid()) {
            base.OnMouseDown();
            _dialogueComplete = true;
        }
    }
}
