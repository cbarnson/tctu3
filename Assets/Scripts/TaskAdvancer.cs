using UnityEngine;
using System.Collections;
using System;

public class TaskAdvancer : MonoBehaviour {

    [System.Serializable]
    public class TaskAdvanced {
        public TaskType taskType;
    }
    [System.Serializable]
    public class OnInventoryTrigger : TaskAdvanced {
        //public bool isUsed = false;
    }
    [System.Serializable]
    public class OnMessageTrigger : TaskAdvanced {
        public bool isUsed = false;
        public string messageEvent;
    }
    [System.Serializable]
    public class OnItemPickupTrigger : TaskAdvanced {

    }

    public OnInventoryTrigger[] onInventoryTriggers;
    public OnMessageTrigger[] onMessageTriggers;
    public OnItemPickupTrigger[] onItemPickupTriggers;

    /// <summary>
    /// Subscribe to necessary event publishers.
    /// </summary>
    protected void Start() {
        Dialoguer.events.onMessageEvent += onMessageEvent;
        InventoryBehaviour.InventoryEntered += onInventoryEntered;
        baseItem.ItemPickup += onItemPickup;
    }

    /// <summary>
    /// Unhook from listening for events.
    /// </summary>
    protected void OnDestroy() {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
        InventoryBehaviour.InventoryEntered -= onInventoryEntered;
        baseItem.ItemPickup -= onItemPickup;
    }

    private void onInventoryEntered(object sender, EventArgs e) {
        for (int i = 0; i < onInventoryTriggers.Length; i++) {
            D.log("TaskAdvancer, advancing task " + onInventoryTriggers[i].taskType.ToString());
            TaskManager.Instance.AdvanceTask(onInventoryTriggers[i].taskType);
        }
    }

    private void onMessageEvent(string message, string metadata) {
        for (int i = 0; i < onMessageTriggers.Length; i++) {
            if (!onMessageTriggers[i].isUsed && onMessageTriggers[i].messageEvent == message) {
                D.log("TaskAdvancer, messageEvent: " + message + ", advancing task " + onMessageTriggers[i].taskType.ToString());
                TaskManager.Instance.AdvanceTask(onMessageTriggers[i].taskType);
                onMessageTriggers[i].isUsed = true;
            }
        }
    }

    private void onItemPickup(object sender, PickupEventArgs e) {
        for (int i = 0; i < onItemPickupTriggers.Length; i++) {
            if (e.firstPickup) {
                D.log("TaskAdvancer, ItemPickup, advancing task " + onInventoryTriggers[i].taskType.ToString());
                TaskManager.Instance.AdvanceTask(onItemPickupTriggers[i].taskType);
            }
        }
    }
}
