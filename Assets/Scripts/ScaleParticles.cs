﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ScaleParticles : MonoBehaviour {
    void Update() {
        //particleSystem.startSize = transform.lossyScale.magnitude;
        if (transform.lossyScale.x < 0) {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }
    }
    //public void OnWillRenderObject() {
        //if (transform.lossyScale.x < 0) {
        //    transform.localScale = newVector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        //}
    //}
}
