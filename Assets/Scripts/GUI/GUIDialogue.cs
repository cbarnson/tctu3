using UnityEngine;
using System.Collections;
using System;
using util;

public class GUIDialogue : MonoBehaviour {

    public static GUIDialogue Instance {
        get {
            return _instance;
        }
    }

    private static Canvas _canvas;
    private static GUIDialogue _instance;

    private bool _setToCrosshairOnDialogueEnded = true;

    //-----------------------------------------------
    // Unity functions
    //-----------------------------------------------
    protected void Awake() {
        _instance = this;
        _canvas = GetComponent<Canvas>();
        //_canvas.enabled = false;
        Dialoguer.Initialize();
    }
    protected void Start() {
        Dialoguer.events.onStarted += onDialogueStarted;
        Dialoguer.events.onEnded += onDialogueEnded;
        Dialoguer.events.onTextPhase += onDialogueTextPhase;
        Dialoguer.events.onMessageEvent += onDialogueMessageEvent;

        _canvas.enabled = false;
    }
    protected void OnDestroy() {
        Dialoguer.events.onStarted -= onDialogueStarted;
        Dialoguer.events.onEnded -= onDialogueEnded;
        Dialoguer.events.onTextPhase -= onDialogueTextPhase;
        Dialoguer.events.onMessageEvent -= onDialogueMessageEvent;
        if (_instance != null) {
            _instance = null;
        }
    }

    //-----------------------------------------------
    // Public methods
    //-----------------------------------------------
    // repeatedly try to transition to dialogue state until successful
    public void HandleDialogueStartRequestForced(int dialogueID) {
        StartCoroutine(TryDialogueTransition(dialogueID));
    }
    public void HandleDialogueStartRequestForced(int dialogueID, Vector3 lookTarget) {
        StartCoroutine(TryDialogueTransition(dialogueID, lookTarget));
    }
    IEnumerator TryDialogueTransition(int dialogueID) {
        while (!GUIManager.Instance.HandleStateTransitionRequest("ToDialogue", GUIState.Crosshair)) {
            yield return null;
        }
        Dialoguer.StartDialogue(dialogueID);
    }
    IEnumerator TryDialogueTransition(int dialogueID, Vector3 lookTarget) {
        while (!GUIManager.Instance.HandleStateTransitionRequest("ToDialogue", GUIState.Crosshair)) {
            yield return null;
        }
        Dialoguer.StartDialogue(dialogueID);
        CameraController.focus(lookTarget);
    }

    public void HandleDialogueStartRequest(int dialogueID) {
        if (GUIManager.Instance.HandleStateTransitionRequest("ToDialogue", GUIState.Crosshair)) {
            Dialoguer.StartDialogue(dialogueID);
        }
    }
    public void HandleDialogueStartRequest(int dialogueID, Vector3 lookTarget) {
        if (GUIManager.Instance.HandleStateTransitionRequest("ToDialogue", GUIState.Crosshair)) {
            Dialoguer.StartDialogue(dialogueID);
            CameraController.focus(lookTarget);
        }
    }

    public bool IsDialogueStartRequestValid() {
        return GUIManager.Instance.HandleStateTransitionRequest("ToDialogue", GUIState.Crosshair);
    }

    // move to next dialogue within a sequence
    public void nextDialogue() {
        if (GUIManager.IsPaused) return;
        Dialoguer.ContinueDialogue();
    }
    // move to next dialogue with ID == choiceID within a sequence
    public void nextDialogue(int choiceID) {
        if (GUIManager.IsPaused) return;
        Dialoguer.ContinueDialogue(choiceID);
    }

    public void Show() {
        _canvas.enabled = true;
    }

    public void Hide() {
        _canvas.enabled = false;
    }

    //-----------------------------------------------
    // Private helper methods
    //-----------------------------------------------

    // dialogue sequence first started
    private void onDialogueStarted() {
        _canvas.enabled = true;
    }

    // dialogue sequence just ended
    private void onDialogueEnded() {
        _canvas.enabled = false;
        if (_setToCrosshairOnDialogueEnded) {
            GUIManager.Instance.HandleStateTransition("ToCrosshair");
        }
    }

    // for each new text block within a sequence
    private void onDialogueTextPhase(DialoguerTextData data) {
        dialogueText.m_text.text = data.text;
        selectPortrait(data);
    }

    private void onDialogueMessageEvent(string message, string metadata) {
        if (message == "reloadCurrentScene") {
            tctu.reloadCurrentScene();
        } else if (message == "loadLevelSummary") {
            //D.log("load level summary");
            tctu.loadLevelSummary();
        } else if (message == "startTimeline") {            // NOTE: MAY CHANGE THIS WITH NEW SYSTEM
            _canvas.enabled = false;
        } else if (message == "loadNextScene") {
            tctu.loadNextScene();
        }
    }

    // select portrait according to data.name
    private void selectPortrait(DialoguerTextData data) {

        if (data.name == "curator") {
            dialoguePlayer.m_rawImage.texture = ResourceManager.Instance.PortraitGetTexture(data.name, data.portrait);
            dialoguePlayer.m_mask.showMaskGraphic = true;
            dialogueSpeaker.m_mask.showMaskGraphic = false;
        } else {
            dialogueSpeaker.m_rawImage.texture = ResourceManager.Instance.PortraitGetTexture(data.name, data.portrait);
            dialoguePlayer.m_mask.showMaskGraphic = false;
            dialogueSpeaker.m_mask.showMaskGraphic = true;
        }
    }

}
