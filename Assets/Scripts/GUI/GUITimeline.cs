using UnityEngine;
using System.Collections;
using System;

public class GUITimeline : MonoBehaviour {

    public static GUITimeline Instance {
        get {
            return _instance;
        }
    }

    public bool TimelineLabelsModifiable = false;
    public RectTransform timelineRectTransform;

    private float _timelineSlideRate = 350f;
    private Vector2 _timelineActiveAnchoredPosition;
    private Vector2 _timelineInactiveAnchoredPosition;
    private static GUITimeline _instance;
    private static Canvas _canvas;

    //-----------------------------------------------
    // Unity functions
    //-----------------------------------------------

    protected void Awake() {
        _instance = this;
        _canvas = GetComponent<Canvas>();
        _canvas.enabled = false;
        _timelineActiveAnchoredPosition = timelineRectTransform.anchoredPosition;
        _timelineInactiveAnchoredPosition = timelineRectTransform.anchoredPosition + Vector2.down * 110f;
    }

    protected void Start() {
        _canvas.enabled = false;
        Hide();
        Dialoguer.events.onMessageEvent += OnMessageEvent;
    }

    protected void OnDestroy() {
        Dialoguer.events.onMessageEvent -= OnMessageEvent;
        if (_instance != null) {
            _instance = null;
        }
    }

    private void OnMessageEvent(string message, string metadata) {
        if (string.IsNullOrEmpty(message)) return;
        if (message.Equals("timelineComplete", StringComparison.Ordinal)) {
            TimelineLabelsModifiable = false;
        }
    }

    public void Show() {
        StopAllCoroutines();
        StartCoroutine(LerpUtility.MoveTowardsAnchored_V2(timelineRectTransform, _timelineActiveAnchoredPosition, _timelineSlideRate));
        _canvas.enabled = true;
    }

    public void Hide() {
        StopAllCoroutines();
        StartCoroutine(LerpUtility.MoveTowardsAnchored_V2_DisableCanvasOnCompletion(timelineRectTransform, _timelineInactiveAnchoredPosition, _timelineSlideRate, _canvas));
    }

    //IEnumerator slideOut() {
    //    while (timelineRectTransform.anchoredPosition != _timelineInactiveAnchoredPosition) {
    //        timelineRectTransform.anchoredPosition = Vector2.MoveTowards(timelineRectTransform.anchoredPosition, _timelineInactiveAnchoredPosition, _timelineSlideRate * Time.deltaTime);
    //        yield return null;
    //    }
    //    _canvas.enabled = false;
    //    //yield return null;
    //}

    //IEnumerator slideIn() {
    //    while (timelineRectTransform.anchoredPosition != _timelineActiveAnchoredPosition) {
    //        timelineRectTransform.anchoredPosition = Vector2.MoveTowards(timelineRectTransform.anchoredPosition, _timelineActiveAnchoredPosition, _timelineSlideRate * Time.deltaTime);
    //        yield return null;
    //    }
    //    //D.log("slide in complete");
    //    //yield return null;
    //}

    //public void activate() {
    //    m_canvas.enabled = true;
    //    StopAllCoroutines();
    //    StartCoroutine(slideIn());
    //}

    //public void deactivate() {
    //    StopAllCoroutines();
    //    StartCoroutine(slideOut());
    //}
}
