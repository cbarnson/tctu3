using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUILevelSummary : MonoBehaviour {

    private enum levelName {
        museum,
        acropolis,
        aphaia,
        propylaia,
        count
    }

    // Text child objects for data entry
    public Text txtTimeCompleted;
    public Text txtLevelName;

    public RawImage backdropRawImage;
    public Texture2D[] levelTextures;
    private levelName levelCurrent = levelName.museum;      // default
    private Texture2D levelTextureCurrent;

    private static GUILevelSummary _instance = null;
    private static Canvas _canvas;
    private static CanvasGroup _canvasGroup;
    //private static Text _timeText;
    //private static Text _levelText;

    protected void Awake() {
        _instance = this;
        _canvas = GetComponent<Canvas>();
        _canvas.enabled = false;
        _canvasGroup = GetComponent<CanvasGroup>();
        _canvasGroup.alpha = 0.0f;

        //Text[] allText = GetComponentsInChildren<Text>();
        //foreach (Text t in allText) {
        //    if (t.tag == "Time") {
        //        _timeText = t;
        //    } else if (t.tag == "Level") {
        //        _levelText = t;
        //    }
        //}

        // setup level name
        string n = util.tctu.getCurrentSceneName();
        for (int i = 0; i < (int)levelName.count; i++) {
            string temp = ((levelName) i).ToString();
            if (temp == n) {
                levelCurrent = ((levelName) i);
                levelTextureCurrent = levelTextures[i];         // assumes i is not null...
                break;
            }
        }

        // set backdrop image
        backdropRawImage.texture = levelTextureCurrent;
    }

    protected void Start() {
        Show();
        txtLevelName.text =     "level name: " + util.tctu.getCurrentSceneName();
        txtTimeCompleted.text = "time completed: " + util.tctu.GenTimeSpanFromSeconds((int)Time.timeSinceLevelLoad);
        //_timeText.text = Time.timeSinceLevelLoad.ToString();
        //_levelText.text = util.tctu.getCurrentSceneName();
    }

    protected void OnDestroy() {
        if (_instance) {
            _instance = null;
        }
    }

    protected void Update() {
        if (Input.anyKeyDown) {
            D.log("anykeyhit, at this point, from GUILevelSummary, we call tctu.loadNextScene");
            util.tctu.loadNextScene();
        }
    }

    private void Show() {
        _canvas.enabled = true;
        StopAllCoroutines();
        StartCoroutine(LerpUtility.FadeCanvasGroupIn(_canvasGroup));
    }

    private void Hide() {
        StopAllCoroutines();
        StartCoroutine(LerpUtility.FadeCanvasGroupOut_DisableCanvasOnCompletion(_canvasGroup, _canvas));
    }



}
