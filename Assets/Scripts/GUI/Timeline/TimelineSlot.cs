using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class TimelineSlot : MonoBehaviour, IDropHandler {
    // this is a property
    public GameObject item {
        get {
            if (transform.childCount > 0) {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    #region IDropHandler implementation
    public void OnDrop (PointerEventData eventData) {

        if ((GetComponentInChildren<DragTimelineLabel>() == null) && (GUIDragging.IsDragging)) {
            //D.log("dropped timeline label");
            GUIDragging.ItemUI.transform.SetParent(transform);
            ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged()/*lambda function to call our function*/);
        }

    }
    #endregion
}
