using System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TimelinePuzzle : MonoBehaviour, IHasChanged {

    //public static event EventHandler<TimelineCompleteEventArgs> TimelineComplete;

    // ALWAYS use serializeField instead of public, unless you need access from other scripts
    [SerializeField]
    Transform slots;

    public GameObject toBeDeactivated;
    public Fade errorFlash;

    private string inventoryText = null;
    private int _errorCount = 0;

    // UNITY FUNCTIONS
    void Start() {
        Assert.IsNotNull(toBeDeactivated);
        if (toBeDeactivated.activeSelf) { toBeDeactivated.SetActive(false); }
        HasChanged();

        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    void OnDestroy () {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    private void onMessageEvent (string message, string metadata) {
        if (message == "startTimeline") {
            toBeDeactivated.SetActive (true);

            UIPuzzleEventArgs e = new UIPuzzleEventArgs();
            e.ErrorCount = 0;                               // always 0 on start
            e.LevelName = "acropolis";                      // timeline puzzle only occurs in acropolis
            e.PuzzleName = "timeline";
            UIPuzzle.onPuzzleStarted(this, e);

            // OVERRIDE
            GUIDialogue.Instance.Hide();
            GUITimeline.Instance.Show();
        }
    }

    private bool isGoalConfig() {
        if (inventoryText == "123456") {
            return true;
        }
        return false;
    }

    #region IHasChanged implementation
    public void HasChanged() {
        // when you do multiple +'s in a loop, can really eat up memory for GC
        // stringbuilder is more efficient
        //D.log("timeline content has changed");
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        foreach (Transform slotTransform in slots) {
            GameObject item = slotTransform.GetComponent<TimelineSlot>().item;
            if (item) {
                builder.Append(item.name);
            }
        }
        inventoryText = builder.ToString();

        if (inventoryText.Length > 5) {
            if (isGoalConfig()) {
                D.log("the puzzle is correct");
                if (toBeDeactivated != null) {
                    Destroy(toBeDeactivated);
                }
                // event for puzzle complete
                UIPuzzleEventArgs e = new UIPuzzleEventArgs();
                e.ErrorCount = _errorCount;
                e.LevelName = "acropolis";                      // timeline puzzle only occurs in acropolis
                e.PuzzleName = "timeline";
                UIPuzzle.onPuzzleEnded(this, e);

                // skip the buffer dialogue we added
                GUIDialogue.Instance.nextDialogue();

                // OVERRIDE
                GUIDialogue.Instance.Show();
                GUITimeline.Instance.Hide();
                toBeDeactivated.SetActive(false);

            } else {
                // red screen flash
                if (errorFlash.isActiveAndEnabled) {
                    errorFlash.FadeMeInOut();
                    _errorCount++;
                }
            }
        }

    }
    #endregion
}

// we add our own interface
namespace UnityEngine.EventSystems {
    public interface IHasChanged : IEventSystemHandler { // must inherit from IEventSystemHandler
        void HasChanged();
    }
}