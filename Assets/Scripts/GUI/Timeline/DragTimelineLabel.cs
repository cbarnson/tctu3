using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class DragTimelineLabel : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    private Vector3 _startPosition;
    private Transform _startParent;
    private CanvasGroup _canvasGroup;

    void Awake () {
        _canvasGroup = GetComponent<CanvasGroup>();
    }


    #region IBeginDragHandler implementation
    public void OnBeginDrag (PointerEventData eventData) {
        if (!GUITimeline.Instance.TimelineLabelsModifiable) { return; }
        GUIDragging.ItemUI = gameObject;
        _startPosition = transform.position;
        _startParent = transform.parent;
        _canvasGroup.blocksRaycasts = false;
    }
    #endregion


    #region IDragHandler implementation
    public void OnDrag (PointerEventData eventData) {
        if (!GUITimeline.Instance.TimelineLabelsModifiable) { return; }
        transform.position = eventData.position;
    }
    #endregion


    #region IEndDragHandler implementation
    public void OnEndDrag (PointerEventData eventData) {
        if (!GUITimeline.Instance.TimelineLabelsModifiable) { return; }
        GUIDragging.ItemUI = null;
        _canvasGroup.blocksRaycasts = true; // reset back to true
        if (transform.parent == _startParent) {
            transform.position = _startPosition;
        }
    }
    #endregion
}
