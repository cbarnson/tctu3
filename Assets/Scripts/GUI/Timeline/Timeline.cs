using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;
using System;

public class Timeline : MonoBehaviour {

    public static Canvas m_canvas;

    public RectTransform timelineUI;
    public RectTransform activePosition;
    public RectTransform deactivePosition;
    public float slideRate = 300f;

    void Awake () {
        m_canvas = gameObject.GetComponent<Canvas>();
        m_canvas.enabled = false;
    }

    void Start () {
        timelineUI.anchoredPosition = deactivePosition.anchoredPosition;
        Dialoguer.events.onMessageEvent += onMessageEvent;
        //Fieldbook.onFieldbook += onFieldbook;
        UIPuzzle.PuzzleEnded += onPuzzleEnded;
    }

    private void onPuzzleEnded(object sender, UIPuzzleEventArgs e) {
        if (e.PuzzleName == "timeline") {
            deactivate();

            // re-enable the canvas for dialogue to continue
            //baseDialogue.m_canvas.enabled = true;
        }
    }

    void OnDestroy () {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
        //Fieldbook.onFieldbook -= onFieldbook;
    }

    //private void onFieldbook (bool isFieldbookOpen) {
    //    if (isFieldbookOpen) {
    //        activate ();
    //    } else {
    //        deactivate ();
    //    }
    //}

    

    private void onMessageEvent (string message, string metadata) {
        if (message == "startKorePuzzle") {
            activate ();
        } else if (message == "startTimeline") {
            activate ();
        }
    }

    public void activate() {
        m_canvas.enabled = true;
        StopAllCoroutines();
        StartCoroutine(slideIn());
    }

    public void deactivate() {
        StopAllCoroutines();
        StartCoroutine(slideOut());
    }

    IEnumerator slideOut () {
        while (timelineUI.anchoredPosition != deactivePosition.anchoredPosition) {
            timelineUI.anchoredPosition = Vector2.MoveTowards(timelineUI.anchoredPosition, deactivePosition.anchoredPosition, slideRate * Time.deltaTime);
            yield return null;
        }
        m_canvas.enabled = false;
        yield return null;
    }

    IEnumerator slideIn () {
        while (timelineUI.anchoredPosition != activePosition.anchoredPosition) {
            timelineUI.anchoredPosition = Vector2.MoveTowards(timelineUI.anchoredPosition, activePosition.anchoredPosition, slideRate * Time.deltaTime);
            yield return null;
        }
        D.log("slide in complete");
        yield return null;
    }

}
