using UnityEngine;
using System.Collections;

public class GUIDescription : MonoBehaviour {

    public static GUIDescription Instance {
        get {
            return _instance;
        }
    }

    private static GUIDescription _instance;
    private static Canvas _canvas;

    protected void Awake() {
        _instance = this;
        _canvas = GetComponent<Canvas>();
    }

    protected void OnDestroy() {
        if (_instance != null) {
            _instance = null;
        }
    }
}
