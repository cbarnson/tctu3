using UnityEngine;
using System.Collections;
using System;

public class GUIDebugger : MonoBehaviour {

    public static GUIDebugger Instance {
        get {
            if (!_instance) {
                // check if an JobManager is already available in the scene graph
                _instance = FindObjectOfType(typeof(GUIDebugger)) as GUIDebugger;

                // nope, create a new one
                if (!_instance) {
                    var obj = new GameObject("GUIDebugger");
                    _instance = obj.AddComponent<GUIDebugger>();
                }
            }
            return _instance;
        }
    }

    private static GUIDebugger _instance;

    protected void Awake() {
        _instance = this;
    }

	protected void Start() {
        GUIManager.InvalidGUIStateTransitionRequest += OnInvalidGUIStateTransitionRequest;
    }

    private void OnInvalidGUIStateTransitionRequest(GUIState requestedState) {
        D.log("Invalid GUIState Transition request event caught, requested state was " + requestedState.ToString());
    }

    protected void OnDestroy() {
        if (_instance != null) {
            _instance = null;
        }
    }
}
