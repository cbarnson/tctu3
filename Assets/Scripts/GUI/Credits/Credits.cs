using UnityEngine;
using System.Collections;

public class Credits : MonoBehaviour {

    public RectTransform contents;
    public RectTransform destination;
    public RectTransform source;
    public float startDelay = 1.0f;
    public float endDelay = 1.0f;
    public float transitionSpeed = 500f;
    
    protected void Start() {
        contents.anchoredPosition = source.anchoredPosition;
        StartTransition();
    }

    private void StartTransition() {
        StartCoroutine(Transition());
    }

    private void EndTransition() {
        StartCoroutine(LoadMenuAfterDelay());
    }

    IEnumerator Transition() {
        yield return new WaitForSeconds(startDelay);
        while (contents.anchoredPosition != destination.anchoredPosition) {
            contents.anchoredPosition = Vector2.MoveTowards(contents.anchoredPosition, destination.anchoredPosition, 
                transitionSpeed * Time.deltaTime);
            yield return null;
        }
        EndTransition();
    }

    IEnumerator LoadMenuAfterDelay() {
        yield return new WaitForSeconds(endDelay);
        util.tctu.loadMenu();
    }

}
