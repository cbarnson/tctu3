using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;

public enum GUIState {
    Dialogue,
    Dragging,
    Inventory, 
    Crosshair,
}

public class GUIManager : MonoBehaviour {

    public delegate void InvalidRequestHandler(GUIState requestedState);
    public static event InvalidRequestHandler InvalidGUIStateTransitionRequest;

    // debug
    public const bool GUIMANAGER_DEBUG = true;

    [HideInInspector]
    public GUIState currentGUIState = GUIState.Crosshair;

    //-----------------------------------------------
    // Public static properties
    //-----------------------------------------------

    public static GUIManager Instance {
        get { return _instance; }
    }

    public static GUIState Current {
        get { return Instance.currentGUIState; }
    }

    public static bool MovementEnabled {
        get { return GUICrosshair.MovementEnabled; }
    }

    public static bool IsPaused {
        get { return _animator.GetBool("IsPaused"); }
    }

    //-----------------------------------------------
    // Private members
    //-----------------------------------------------

    private string prev;
    private static GUIManager _instance;
    private static Animator _animator;

    //-----------------------------------------------
    // Unity functions
    //-----------------------------------------------

    protected void Awake() {
        _instance = this;
        _animator = GetComponent<Animator>();
    }

    protected void Start() {
        HandleStateTransitionRequest("ToCrosshair", GUIState.Crosshair);
        GUIPause.PauseResumed += OnPauseResumed;
        //PauseableStateMachineBehaviour.PauseEntered += OnPausedEntered;
        //PauseableStateMachineBehaviour.PauseExited += OnPauseExited;
        //GUIPause.PauseStateChange += onPauseStateChange;
    }

    protected void Update() {

    }

    protected void OnDestroy() {
        GUIPause.PauseResumed -= OnPauseResumed;
        //PauseableStateMachineBehaviour.PauseEntered -= OnPausedEntered;
        //PauseableStateMachineBehaviour.PauseExited -= OnPauseExited;
        //GUIPause.PauseStateChange -= onPauseStateChange;
        if (_instance != null) {
            _instance = null;
        }
    }

    private void OnPauseResumed(object sender, EventArgs e) {
        D.log("In GUIManager, OnPauseResumed; setting IsPaused to false");
        D.assert(_animator.GetBool("IsPaused") == true);
        _animator.SetBool("IsPaused", false);
    }

    //private void OnPausedEntered(object sender, EventArgs e) {
    //    //GUIPause.Instance.PauseActive = newPauseState;
    //}

    //private void OnPauseExited(object sender, EventArgs e) {
    //    //GUIPause.Instance.PauseActive = newPauseState;
    //}

    //-----------------------------------------------
    // Public methods
    //-----------------------------------------------

    // called only by derived StateMachineBehaviour classes in the OnStateEnter method
    public void ChangeGUIState(GUIState newGUIState) {
        currentGUIState = newGUIState;
    }

    // pass the name of the trigger to be set, and the state from which the transition should begin if it is a valid transition
    // call via GUIManager.Instance.HandleStateTransitionRequest
    public bool HandleStateTransitionRequest(string stateToTrigger, GUIState fromState) {
        if (currentGUIState != fromState || _animator.GetBool("IsPaused")) { return false; }
        if (!string.IsNullOrEmpty(prev)) { _animator.ResetTrigger(prev); }
        _animator.SetTrigger(stateToTrigger);
        prev = stateToTrigger;
        return true;
    }

    // THIS IS THE MOST UP TO DATE FUNCTION
    public void HandleStateTransitionRequest(GUIState destinationState) {
        if (!IsValidTransition(destinationState)) {
            OnInvalidGUIStateTransitionRequest(destinationState);
            return;
        }
        string triggerName = "To" + destinationState.ToString();
        if (!string.IsNullOrEmpty(prev)) { _animator.ResetTrigger(prev); }
        _animator.SetTrigger(triggerName);
        prev = triggerName;
    }




    // only use when GUARANTEED the transition is valid
    public void HandleStateTransition(string stateToTrigger) {
        if (IsPaused) { return; }
        if (!string.IsNullOrEmpty(prev)) { _animator.ResetTrigger(prev); }
        _animator.SetTrigger(stateToTrigger);
        prev = stateToTrigger;
    }

    // transition to dialogue and start using param dialoguer ID
    //public void HandleStateTransitionToDialogue(int dialogueID) {
    //    D.assert(currentGUIState == GUIState.Crosshair, "error, cannot start dialogue from the current state", true);
    //    if (!string.IsNullOrEmpty(prev)) { _animator.ResetTrigger(prev); }
    //    _animator.SetTrigger("ToDialogue");
    //    prev = "ToDialogue";
    //    Dialoguer.StartDialogue(dialogueID);
    //}

    //public void HandlePauseTransition(bool newPauseState) {
    //    GUIPause.Instance.PauseActive = newPauseState;
    //}

    public void OnInvalidGUIStateTransitionRequest(GUIState requestedState) {
        if (InvalidGUIStateTransitionRequest != null) {
            InvalidGUIStateTransitionRequest(requestedState);
        }
    }

    //-----------------------------------------------
    // Private helper functions
    //-----------------------------------------------

    // GUIPause event, called every time GetKeyDown for Pause toggle KeyCode pressed or resume button from Pause menu is clicked
    //private void onPauseStateChange(bool newState) {
    //    _animator.SetBool("IsPaused", newState);
    //}

    private bool IsValidTransition(GUIState destinationState) {
        // new-----------
        if (IsPaused) return false;
        //--------------
        switch(destinationState) {
            case GUIState.Crosshair:
                if (currentGUIState != GUIState.Crosshair) { return true; }
                break;
            case GUIState.Dialogue:
                if (currentGUIState == GUIState.Crosshair) { return true; }
                break;
            case GUIState.Inventory:
                if (currentGUIState == GUIState.Crosshair) { return true; }
                break;
            case GUIState.Dragging:
                if (currentGUIState == GUIState.Inventory) { return true; }
                //if (currentGUIState == GUIState.Inventory && !GUIManager.IsPaused) { return true; }
                break;
        }
        return false;
    }

}
