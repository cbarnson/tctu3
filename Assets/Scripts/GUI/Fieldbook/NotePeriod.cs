using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NotePeriod : MonoBehaviour {

    public NotePage[] notePage;
    public Text txtPageNavigation;

    private void OnEnable() {
        foreach (NotePage np in notePage) {
            np.gameObject.SetActive(false);
        }
        if (notePage.Length > 0) {
            notePage[0].gameObject.SetActive(true);
        }
        UpdatePageNavigation();
    }

    public void btnClick_NextPage() {
        //D.log("Next page clicked for NotePeriod gameobject " + gameObject.name);
        int last = GetActiveIndex();
        if (last == -1) return;
        int next = (last + 1) % notePage.Length;
        notePage[last].gameObject.SetActive(false);
        notePage[next].gameObject.SetActive(true);
        UpdatePageNavigation();
    }

    public void btnClick_PrevPage() {
        //D.log("Prev page clicked for NotePeriod gameobject " + gameObject.name);
        int last = GetActiveIndex();
        if (last == -1) return;
        int next = last - 1; // here, next is really the previous page
        if (next < 0) next = notePage.Length - 1;
        notePage[last].gameObject.SetActive(false);
        notePage[next].gameObject.SetActive(true);
        UpdatePageNavigation();
    }

    private void UpdatePageNavigation() {
        int i = GetActiveIndex();
        if (i == -1) {
            txtPageNavigation.text = "0 / " + notePage.Length.ToString();
            return;
        }
        txtPageNavigation.text = (i + 1).ToString() + " / " + notePage.Length.ToString();
    }

    private int GetActiveIndex() {
        int i;
        for (i = 0; i < notePage.Length; i++) {
            if (notePage[i].gameObject.activeInHierarchy) { return i; }
        }
        return -1; // none active
    }

}
