using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Collections;
using System.Collections.Generic;

public class CatalogPanel : MonoBehaviour {

    [SerializeField]
    Transform[] slots;

    public Fade itemDescription;

    private bool _itemDescriptionActive = false;
    private Text itemText;

    // searches for a valid slot, sets the param obj parent to the appropriate slot
    public void addItem (GameObject obj) {
        for (int i = 0; i < slots.Length; i++) {
            if (slots[i].childCount < 1) {
                obj.transform.SetParent(slots[i], false);
                D.log("addItem, set parent to slot number: " + i);
                return;
            }
        }
        // if we get here, either the inventory is full, or failed to set parent
        D.log("warning, item was not moved to inventory");
    }

    // returns true if the inventory contains an item in every available slot
    public bool isFull() {
        bool full = true;
        foreach (Transform s in slots) {
            if (s.childCount < 1) {
                full = false;
                break;
            }
        }
        return full;
    }

    // returns true if the inventory does not contain an item in ANY available slot
    public bool isEmpty() {
        bool empty = true;
        foreach (Transform s in slots) {
            if (s.childCount > 0) {
                empty = false;
                break;
            }
        }
        return empty;
    }

    public int countItems() {
        int count = 0;
        for (int i = 0; i < slots.Length; i++) {
            count += slots[i].childCount;
        }
        return count;
    }

    // re-arranges inventory, such that the first empty slot follows the last slot that contains a child
    private void organizeInventory() {
        if (isEmpty()) { return; }
        List<GameObject> items = new List<GameObject>();
        for (int i = 0; i < slots.Length; i++) {
            if (slots[i].childCount > 0) {
                items.Add(slots[i].GetChild(0).gameObject);
            }
        }
        // add items from list back into the slots
        for (int i = 0; i < items.Count; i++) {
            items[i].transform.SetParent(slots[i]);
        }
    }

    void Awake () {
        Assert.IsNotNull(itemDescription);
        itemText = itemDescription.GetComponentInChildren<Text>();
        Assert.IsNotNull(itemText);
    }

    private void Start() {
        dragHandlerFieldbook.ItemDropped += onItemDropped;
    }

    private void OnDestroy() {
        dragHandlerFieldbook.ItemDropped -= onItemDropped;
    }

    private void onItemDropped(object sender, ItemDroppedEventArgs e) {
        // if the item was dropped and another spawned to take its place, re-sort the items in inventory slots
        if (e.SpawnedAnother) {
            organizeInventory();
        }
    }

    // fade in the panel holding the item description
    public void show (string str) {
        //D.log("show itemDescription");
        itemText.text = str;
        itemDescription.CancelRoutines();
        itemDescription.FadeMeIn();
        _itemDescriptionActive = true;
    }

    // fade out the panel
    public void hide () {
        //D.log("hide itemDescription");
        itemDescription.CancelRoutines();
        itemDescription.FadeMeOut();
        _itemDescriptionActive = false;
    }

    // while the description is active, make its position follow the cursor
    void Update () {
        if (_itemDescriptionActive) {
            itemDescription.transform.position = Input.mousePosition;
        }
    }

}
