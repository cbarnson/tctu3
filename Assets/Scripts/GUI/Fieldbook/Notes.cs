using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using System.Collections;

using util;

public class Notes : MonoBehaviour {

    public Button _nextButton;
    public Button _prevButton;
    public Entry[] entries;

    public AudioClip clip;                  // page turn sound effect
    private float _clipVolume = 1.0f;

    private int currPage = 0;
    private int museumLevelIndex = 2;
    private int acropolisLevelIndex = 5;
    private int aphaiaLevelIndex = 6;
    private int propylaiaLevelIndex = 7;

    public virtual void Start () {
        foreach (Entry e in entries) {
            // initially set all Entry objects to disabled
            e.restricted.SetActive(false);
            e.classified.SetActive(false);
            Scene current = SceneManager.GetActiveScene();
            if (current.buildIndex > museumLevelIndex && e.level == "museum") { 
                e.isRestricted = false; // set note entry to the classified version
            } else if (current.buildIndex > acropolisLevelIndex && e.level == "acropolis") {
                e.isRestricted = false;
            } else if (current.buildIndex > aphaiaLevelIndex && e.level == "aphaia") {
                e.isRestricted = false;
            } else if (current.buildIndex > propylaiaLevelIndex && e.level == "propylaia") {
                e.isRestricted = false;
            }
            // add more entries for a given level here
        }
    }

    // enable first entry
    public virtual void OnEnable () {
        // set our current page
        currPage = 0;
        // update the active entry
        enableCurrEntry();
    }

    public virtual void OnDisable () {
        disableAllEntries();
    }

    public void nextPage () {
        disableCurrEntry();
        currPage = (currPage + 1) % entries.Length;
        enableCurrEntry();
        //AudioSource.PlayClipAtPoint(clip, Player.m_camera.transform.position, _clipVolume);
    }

    public void prevPage () {
        disableCurrEntry();
        if (currPage == 0) { currPage = entries.Length - 1; } 
        else { currPage--; }
        enableCurrEntry();
        //AudioSource.PlayClipAtPoint(clip, Player.m_camera.transform.position, _clipVolume);
    }

    private void disableCurrEntry() {
        // disable the current entry
        if (entries[currPage].isRestricted) {
            entries[currPage].restricted.SetActive(false);
        } else {
            entries[currPage].classified.SetActive(false);
        }
    }

    private void enableCurrEntry() {
        // enable the current entry
        D.log("enable curr entry");
        if (entries[currPage].isRestricted) {
            // enable the restricted version
            entries[currPage].restricted.SetActive(true);
        } else {
            entries[currPage].classified.SetActive(true);
        }
    }

    private void disableAllEntries() {
        foreach(Entry e in entries) {
            e.restricted.SetActive(false);
            e.classified.SetActive(false);
        }
    }

    
}
