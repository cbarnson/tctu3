using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NoteSection : MonoBehaviour {

    public NotePeriod[] notePeriod;
    public GameObject parentJumpToPeriod;
    public GameObject parentJumpToPeriodSelection;

    private void OnEnable() {
        foreach (NotePeriod ns in notePeriod) {
            //D.log("NotePeriod gameobject " + ns.gameObject.name + " set active to false");
            ns.gameObject.SetActive(false);
        }
        // deactivate jump to selection button
        parentJumpToPeriodSelection.SetActive(false);
        //D.log("parentJumpToPeriod set active to true");
        parentJumpToPeriod.SetActive(true);
    }

    public void btnClick_JumpToPeriodSelection() {
        foreach (NotePeriod ns in notePeriod) {
            //D.log("NotePeriod gameobject " + ns.gameObject.name + " set active to false");
            ns.gameObject.SetActive(false);
        }
        //D.log("parentJumpToPeriod set active to true");
        parentJumpToPeriod.SetActive(true);
        // deactivate the button we just hit
        parentJumpToPeriodSelection.SetActive(false);
    }

    public void btnClick_JumpToPeriod(int i) {
        //D.log("parentJumpToPeriod set active to false");
        if (i < 0 || i >= notePeriod.Length) {
            D.warn("Warning, btnClick_JumpToPeriod received invalid parameter, index must be between 0 and array length - 1");
            return;
        }
        parentJumpToPeriod.SetActive(false);
        //D.assert(i >= 0 && i < notePeriod.Length);
        //D.log("NotePeriod gameobject[" + i + "] set active to true");
        notePeriod[i].gameObject.SetActive(true);
        // activate jump to selection button
        parentJumpToPeriodSelection.SetActive(true);
    }

    
}
