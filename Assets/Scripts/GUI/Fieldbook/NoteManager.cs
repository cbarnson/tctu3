using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;


public class NoteManager : MonoBehaviour {

    public NoteSection[] noteSection;
    public GameObject parentJumpToSection;
    public GameObject parentJumpToSectionSelection;

    private void OnEnable() {
        foreach (NoteSection ns in noteSection) {
            D.log("NoteSection gameobject " + ns.gameObject.name + " set active to false");
            ns.gameObject.SetActive(false);
        }
        // deactivate jump to selection button
        parentJumpToSectionSelection.SetActive(false);
        D.log("parentJumpToSection set active to true");
        parentJumpToSection.SetActive(true);
    }

    public void btnClick_JumpToSectionSelection() {
        foreach (NoteSection ns in noteSection) {
            D.log("NoteSection gameobject " + ns.gameObject.name + " set active to false");
            ns.gameObject.SetActive(false);
        }
        D.log("parentJumpToSection set active to true");
        parentJumpToSection.SetActive(true);
        // deactivate the button we just hit
        parentJumpToSectionSelection.SetActive(false);
    }

    public void btnClick_JumpToSection(int i) {
        D.log("parentJumpToSection set active to false");
        parentJumpToSection.SetActive(false);
        D.assert(i >= 0 && i < noteSection.Length);
        D.log("NoteSection gameobject[" + i + "] set active to true");
        noteSection[i].gameObject.SetActive(true);
        // activate jump to selection button
        parentJumpToSectionSelection.SetActive(true);
    }

    

}