using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;
using UnityEngine.EventSystems;
using System;

// dropzones are tagged "dropzone"

public enum ItemType {
    UI,
    WORLD
}

public class ItemDroppedEventArgs : System.EventArgs {
    public ItemType Type { get; set; }
    public GameObject Dropped { get; set; }
    public bool SpawnedAnother { get; set; }
    public bool ResetCrosshair { get; set; }
}

public class dragHandlerFieldbook : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler {

    public static EventHandler<ItemDroppedEventArgs> ItemDropped;

    private Vector3 _startPosition;                 // stores original position
    private Transform _startParent;                 // stores original parent
    private string _dropzoneTag = "dropzone";               

    private ItemDescription _description;

    void Awake () {
        _description = GetComponent<ItemDescription>();
        D.assert(_description != null, "error, ItemDescription member of dragHandlerFieldbook is null", true);
    }

    public void OnPointerEnter (PointerEventData eventData) {
        CatalogPanel _parent = GetComponentInParent<CatalogPanel>();
        if (_parent) {
            _parent.show(_description.description);
        }
    }

    public void OnPointerExit (PointerEventData eventData) {
        CatalogPanel _parent = GetComponentInParent<CatalogPanel>();
        if (_parent) {
            _parent.hide();
        }
    }

    #region IBeginDragHandler implementation
    // initialize static member itemBeingDragged, store the original parent and position of the object
    public void OnBeginDrag(PointerEventData eventData) {
        GUIDragging.Item = gameObject;
        _startPosition = transform.position;
        _startParent = transform.parent;

        // stop displaying the info panel for this object once the drag begins
        CatalogPanel _parent = GetComponentInParent<CatalogPanel>();
        if (_parent) {
            _parent.hide();
        }

        transform.SetParent(GUIDragging.Instance.transform);

        gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }
    #endregion

    #region IDragHandler implementation
    // attach the object to the cursor while it is being dragged and disable Fieldbook and Timeline
    public void OnDrag(PointerEventData eventData) {
        transform.position = eventData.position;
    }
    #endregion

    #region IEndDragHandler implementation
    // handles resetting of the cursor/crosshair and handling of the UI element that was dragged
    // checkForDropzone will return true if a dropzone was detected and object instantiated
    public void OnEndDrag(PointerEventData eventData) {
        bool spawned = false;

        Ray ray = Camera.main.ScreenPointToRay(eventData.position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 5.0f)) {
            if (hit.transform.tag == _dropzoneTag && hit.transform.childCount < 1) {
                int zoneIndex = hit.transform.gameObject.GetComponent<zone>()._zoneID;
                // spawn the object, parent to hit.transform and pass the index of the zone hit for positioning details
                spawnObject(hit.transform, zoneIndex);
                spawned = true;
            }
        }

        // create event arguments
        ItemDroppedEventArgs args = new ItemDroppedEventArgs();
        args.Type = ItemType.UI;
        args.Dropped = gameObject;
        if (spawned) { args.SpawnedAnother = true; }
        else { args.SpawnedAnother = false; }
        args.ResetCrosshair = true;

        // call our delegate
        onItemDropped(args);
        GUIDragging.Item = null;

        if (spawned) {
            Destroy(gameObject);
        } else {
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            transform.SetParent(_startParent);
            transform.position = _startPosition;
        }

    }
    #endregion


    private void onItemDropped(ItemDroppedEventArgs e) {
        if (ItemDropped != null) {
            ItemDropped(this, e);
        }
    }


    private void spawnObject(Transform newParent, int hitZoneIndex) {
        for (int i = 0; i < museManager.Artifact.Length; i++) {
            if (GUIDragging.Item.tag == museManager.Artifact[i].tag) {
                GameObject j = Instantiate(museManager.Artifact[i]);

                // use the hit index to determine the proper position and rotation
                j.transform.position = museManager.Artifact[hitZoneIndex].transform.position;
                j.transform.rotation = museManager.Artifact[hitZoneIndex].transform.rotation;

                // set the new object's parent to what was hit
                j.transform.SetParent(newParent);

                // make the object pickable
                j.GetComponent<baseItem>().pickable = true;
                break;
            }
        }
    }

}
