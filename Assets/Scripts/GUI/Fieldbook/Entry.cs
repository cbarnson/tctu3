using UnityEngine;
using System.Collections;
using util;

public class Entry : MonoBehaviour {

    public GameObject restricted; // contains partial information
    public GameObject classified; // contains all information
    public bool isRestricted = true;
    public string level;

}
