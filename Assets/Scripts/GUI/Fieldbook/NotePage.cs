using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NotePage : MonoBehaviour {

    public TextAsset taName;
    public TextAsset taPrimary;
    public TextAsset taSecondary;
    public Texture2D t2dTexture;

    public Text txtName;
    public Text txtPrimary;
    public Text txtSecondary;
    public RawImage rImage;

    private void Start() {
        if (taName) txtName.text = taName.text;
        if (taPrimary) txtPrimary.text = taPrimary.text;
        if (taSecondary) txtSecondary.text = taSecondary.text;
        if (t2dTexture) rImage.texture = t2dTexture;
    }

}
