using UnityEngine;
using System;
using System.Collections;

public class InventoryBehaviour : PauseableStateMachineBehaviour {

    public static event EventHandler InventoryEntered;
    public static event EventHandler InventoryExited;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        // prevents glide
        FPController.Instance.HaltMovement();
        GUIManager.Instance.ChangeGUIState(GUIState.Inventory);
        GUIFieldbook.Instance.Show();
        GUITimeline.Instance.Show();
        OnInventoryEntered();
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        GUIFieldbook.Instance.Hide();
        GUITimeline.Instance.Hide();
        OnInventoryExited();
    }

    //protected override void OnPauseEntered() {
    //    D.log("In InventoryBehaviour, OnPauseEntered, base.OnPauseEntered publisher");
    //    base.OnPauseEntered();
    //}

    //protected override void OnPauseExited() {
    //    D.log("In InventoryBehaviour, OnPauseExited, base.OnPauseExited publisher");
    //    base.OnPauseExited();
    //}

    protected virtual void OnInventoryEntered() {
        D.log("In InventoryBehaviour, OnInventoryEntered publisher");
        if (InventoryEntered != null) {
            InventoryEntered(this, EventArgs.Empty);
        }
    }

    protected virtual void OnInventoryExited() {
        D.log("In InventoryBehaviour, OnInventoryExited publisher");
        if (InventoryExited != null) {
            InventoryExited(this, EventArgs.Empty);
        }
    }

}
