using UnityEngine;
using System.Collections;

public class DialogueBehaviour : PauseableStateMachineBehaviour {

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        // prevents glide
        FPController.Instance.HaltMovement();
        GUIManager.Instance.ChangeGUIState(GUIState.Dialogue);
        GUIDialogue.Instance.Show();
    }

    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //    base.OnStateUpdate(animator, stateInfo, layerIndex);
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        GUIDialogue.Instance.Hide();
    }

}
