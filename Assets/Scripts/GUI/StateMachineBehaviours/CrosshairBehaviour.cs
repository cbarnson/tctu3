using UnityEngine;
using System.Collections;

public class CrosshairBehaviour : PauseableStateMachineBehaviour {

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        GUIManager.Instance.ChangeGUIState(GUIState.Crosshair);
        GUICrosshair.Show();
    }

    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //    base.OnStateUpdate(animator, stateInfo, layerIndex);
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        GUICrosshair.Hide();
    }

}
