using UnityEngine;
using System;
using System.Collections;

public class PauseableStateMachineBehaviour : StateMachineBehaviour {

    public static event EventHandler PauseEntered;
    public static event EventHandler PauseExited;

    protected static KeyCode pauseKeyControl = KeyCode.P;
    protected static KeyCode pauseKeyControlAlternate = KeyCode.Escape;

    private bool _expectedIsPaused = false;

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (_expectedIsPaused != animator.GetBool("IsPaused")) {
            // resume button behaviour, must update based on expected IsPaused value and actual
            // i.e. animator value must have been changed outside of this class (GUIManager), thus we want to handle the update
            D.log("In PauseableStateMachineBehaviour, Resume clicked; if not there is an error somewhere");
            bool actual = animator.GetBool("IsPaused");
            if (!actual && _expectedIsPaused) {
                OnPauseExited();
            } else {
                D.warn("warning in PauseableStateMachineBehaviour, OnStateUpdate");
            }
            return;
        }
        if (Input.GetKeyDown(pauseKeyControl) || Input.GetKeyDown(pauseKeyControlAlternate)) {
            bool current = animator.GetBool("IsPaused");    // the current state
            animator.SetBool("IsPaused", !current);         // new state has now been set, but we still have the last "current" value
            if (!current) {
                OnPauseEntered();
            } else {
                OnPauseExited();
            }
            //GUIManager.Instance.HandlePauseTransition(!animator.GetBool("IsPaused"));
        }
    }

    protected virtual void OnPauseEntered() {
        D.log("In PauseableStateMachineBehaviour, OnPauseEntered");
        _expectedIsPaused = true;
        if (PauseEntered != null) {
            PauseEntered(this, EventArgs.Empty);
        }
    }

    protected virtual void OnPauseExited() {
        D.log("In PauseableStateMachineBehaviour, OnPauseExited");
        _expectedIsPaused = false;
        if (PauseExited != null) {
            PauseExited(this, EventArgs.Empty);
        }
    }

}
