using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using util;


public class GUIPause : MonoBehaviour {

    //public delegate void GUIPauseStateChange(bool newState);
    //public static event GUIPauseStateChange PauseStateChange;

    public static event EventHandler PauseResumed;

    // singleton
    public static GUIPause Instance {
        get {
            return _instance;
        }
    }
    
    //public bool PauseActive {
    //    get {
    //        return _pauseActive;
    //    }
    //    set {
    //        bool newState = value;
    //        //onPauseStateChange(newState);                       // delegate call

    //        if (newState) {
    //            Time.timeScale = 0;                             // time scale 0 pauses physics
    //            _canvas.enabled = true;                         // show canvas
    //            _cursorVisibilityOnPause = Cursor.visible;      // store initial cursor status
    //            SetCursorVisibleState(true);                    // set cursor visibility settings
    //        } else {
    //            // disable pause functionality
    //            Time.timeScale = 1;
    //            _canvas.enabled = false;
    //            SetCursorVisibleState(_cursorVisibilityOnPause);
    //        }
    //        _pauseActive = newState;
    //    }
    //}

    private static GUIPause _instance;
    private static Canvas _canvas;

    private KeyCode _keyCodeControl = KeyCode.P;
    //private bool _pauseActive = false;
    private bool _cursorVisibilityOnPause;

    // quit sub menu
    public Button resumeButton;
    public Button quitButton;
    public Button helpButton;
    public Button optionButton;
    public CanvasGroup quitCanvasGroup;
    public CanvasGroup helpCanvasGroup;
    public CanvasGroup optionCanvasGroup;
    public Text helpLogText;


    //-----------------------------------------------
    // Unity functions
    //-----------------------------------------------

    protected void Awake() {
        _instance = this;
        _canvas = GetComponent<Canvas>();
        _canvas.enabled = false;
        disableCanvasGroup(quitCanvasGroup);
        disableCanvasGroup(helpCanvasGroup);
        disableCanvasGroup(optionCanvasGroup);
        helpLogText.text = "";
    }

    protected void Start() {
        MessageSenderOnMessageEvent.onDialogueMessageEvent += addToHelpLog;
        MessageSenderOnMessageEventDelayed.DialogueMessageEventDelayed += addToHelpLog;
        PauseableStateMachineBehaviour.PauseEntered += OnPauseEntered;
        PauseableStateMachineBehaviour.PauseExited += OnPauseExited;
    }

    protected void OnDestroy() {
        MessageSenderOnMessageEvent.onDialogueMessageEvent -= addToHelpLog;
        MessageSenderOnMessageEventDelayed.DialogueMessageEventDelayed -= addToHelpLog;
        PauseableStateMachineBehaviour.PauseEntered -= OnPauseEntered;
        PauseableStateMachineBehaviour.PauseExited -= OnPauseExited;
        if (_instance != null) {
            _instance = null;
        }
    }

    protected virtual void OnPauseResumed() {
        if (PauseResumed != null) {
            PauseResumed(this, EventArgs.Empty);
        }
    }

    private void OnPauseEntered(object sender, EventArgs e) {
        Time.timeScale = 0;                             // time scale 0 pauses physics
        _canvas.enabled = true;                         // show canvas
        _cursorVisibilityOnPause = Cursor.visible;      // store initial cursor status
        SetCursorVisibleState(true);                    // set cursor visibility settings
    }

    private void OnPauseExited(object sender, EventArgs e) {
        // disable pause functionality
        Time.timeScale = 1;
        _canvas.enabled = false;
        SetCursorVisibleState(_cursorVisibilityOnPause);
    }

    //private void OnPauseEnteredFromPauseable(object sender, EventArgs e) {
    //    D.log("In GUIPause, OnPauseEnteredFromPauseable receiver");
    //}

    //private void OnInventoryEntered(object sender, EventArgs e) {
    //    D.log("In GUIPause, OnInventoryEntered receiver");
    //}

    //private string GenTimeSpanFromSeconds(double seconds) {
    //    // Create a TimeSpan object and TimeSpan string from 
    //    // a number of seconds.
    //    TimeSpan interval = TimeSpan.FromSeconds(seconds);
    //    string timeInterval = interval.ToString();

    //    // Pad the end of the TimeSpan string with spaces if it 
    //    // does not contain milliseconds.
    //    int pIndex = timeInterval.IndexOf(':');
    //    pIndex = timeInterval.IndexOf('.', pIndex);
    //    // if (pIndex < 0) timeInterval += "        ";

    //    return timeInterval;
    //    //Console.WriteLine("{0,21}{1,26}", seconds, timeInterval);
    //}

    private void addToHelpLog(string msg) {
        //_helpLog.Add(msg);
        int t = (int)Time.timeSinceLevelLoad;
        string temp = util.tctu.GenTimeSpanFromSeconds(t) + '\t' + msg + "\n\n";
        helpLogText.text += temp;
    }

    //-----------------------------------------------
    // Public functions
    //-----------------------------------------------

    // close the pause menu and return to game
    public void resume() {
        //disableQuitGroup();
        OnPauseResumed();
        disableCanvasGroup(quitCanvasGroup);
        disableCanvasGroup(helpCanvasGroup);
        disableCanvasGroup(optionCanvasGroup);
        //PauseActive = false;
    }

    // open the quit sub menu and ask user if quit to desktop or quit to title screen
    public void openQuitMenu() {
        enableCanvasGroup(quitCanvasGroup);
        setAllButtons(false);
    }

    public void openHelpLog() {
        enableCanvasGroup(helpCanvasGroup);
        setAllButtons(false);
    }

    public void openOptionMenu() {
        enableCanvasGroup(optionCanvasGroup);
        setAllButtons(false);
    }

    public void cancelOptionMenu() {
        disableCanvasGroup(optionCanvasGroup);
        setAllButtons(true);
    }

    public void cancelHelpLog() {
        disableCanvasGroup(helpCanvasGroup);
        setAllButtons(true);
    }

    // cancels quit canvas group
    public void cancel() {
        disableCanvasGroup(quitCanvasGroup);
        setAllButtons(true);
    }

    public void quitToDesktop() {
        Time.timeScale = 1;
        tctu.exitGame();
    }

    public void quitToMenu() {
        Time.timeScale = 1;
        tctu.loadMenu();
    }

    //-----------------------------------------------
    // Private helper functions
    //-----------------------------------------------

    // formal delegate call
    //private void onPauseStateChange(bool newState) {
    //    if (PauseStateChange != null) {
    //        PauseStateChange(newState);
    //    }
    //}

    private void SetCursorVisibleState(bool toState) {
        if (toState) {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        } else {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    private void setAllButtons(bool set) {
        quitButton.interactable = set;
        resumeButton.interactable = set;
        helpButton.interactable = set;
        optionButton.interactable = set;
    }

    private void enableCanvasGroup(CanvasGroup cg) {
        cg.alpha = 1.0f;
        cg.interactable = true;
        cg.blocksRaycasts = true;
    }

    private void disableCanvasGroup(CanvasGroup cg) {
        cg.alpha = 0.0f;
        cg.interactable = false;
        cg.blocksRaycasts = false;
    }
}
