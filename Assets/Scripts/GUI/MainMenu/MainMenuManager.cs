﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

    public CanvasGroup cvgCopyright;
    public CanvasGroup cvgTitleScreen;
    public CanvasGroup cvgExitMenu;
    public Button btnNewGame;
    public Button btnExit;
    public Button btnQuit;
    public Button btnCancel;
    // public float fTitleScreenDelay = 5.0f;

    void Awake() {
        cvgCopyright.gameObject.SetActive(true);
        cvgTitleScreen.gameObject.SetActive(true);
        cvgExitMenu.gameObject.SetActive(true);
    }

	// Use this for initialization
	void Start () {
        Cursor.SetCursor(ResourceManager.Instance.CursorTexture, Vector2.zero, CursorMode.ForceSoftware);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        activateCanvasGroup(cvgCopyright);
        deactivateCanvasGroup(cvgTitleScreen);
        deactivateCanvasGroup(cvgExitMenu);
        StartCoroutine(enterTitleScreen());
    }

    //IEnumerator enterTitleScreen(float time) {

    //    yield return new WaitForSeconds(time);
    //    // Code to execute after the delay
    //    deactivateCanvasGroup(cvgCopyright);
    //    activateCanvasGroup(cvgTitleScreen);
    //}

    IEnumerator enterTitleScreen() {

        while (!Input.anyKeyDown) {
            yield return null;
        }

        //yield return new WaitForSeconds(time);
        // Code to execute after the delay
        deactivateCanvasGroup(cvgCopyright);
        cvgCopyright.gameObject.SetActive(false);
        activateCanvasGroup(cvgTitleScreen);
    }


    public void btnExit_Press() {
        btnNewGame.interactable = false;
        btnExit.interactable = false;
        activateCanvasGroup(cvgExitMenu);
    }

    public void btnNewGame_Press() {
        Scene current = SceneManager.GetActiveScene();
        SceneManager.LoadScene(current.buildIndex + 1);
    }

    public void btnQuit_Press() {
        Application.Quit();
    }

    public void btnCancel_Press() {
        btnNewGame.interactable = true;
        btnExit.interactable = true;
        deactivateCanvasGroup(cvgExitMenu);
    }

    private void activateCanvasGroup(CanvasGroup cvg) {
        cvg.alpha = 1.0f;
        cvg.interactable = true;
        cvg.blocksRaycasts = true;
    }

    private void deactivateCanvasGroup(CanvasGroup cvg) {
        cvg.alpha = 0.0f;
        cvg.interactable = false;
        cvg.blocksRaycasts = false;
    }
}
