﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class menuScript : MonoBehaviour {

    
    public Canvas _quitMenu;
    public Button _startButton;
    public Button _quitButton;

    // EXIT
    public void exitPress() {
        _quitMenu.enabled = true;
        _startButton.enabled = false;
        _quitButton.enabled = false;
    }

    // EXIT > CANCEL
    public void noPress() {
        _quitMenu.enabled = false;
        _startButton.enabled = true;
        _quitButton.enabled = true;
    }

    // PLAY
    public void startLevel() {
        Scene current = SceneManager.GetActiveScene();
        SceneManager.LoadScene(current.buildIndex + 1);
    }

    // EXIT > QUIT
    public void quitGame() {
        Application.Quit();
    }

	// Use this for initialization
	void Start () {
        _quitMenu.enabled = false; // initially not active
        Cursor.SetCursor(ResourceManager.Instance.CursorTexture, Vector2.zero, CursorMode.ForceSoftware);

    }

}
