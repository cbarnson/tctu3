 using UnityEngine;
using System.Collections;

public class GUIFieldbook : MonoBehaviour {

    public KeyCode toggleKey = KeyCode.T;
    public static GUIFieldbook Instance {
        get {
            return _instance;
        }
    }

    public static bool FieldbookTimelineLocked {
        get { return _locked; }
        set { _locked = value; }
    }

    private static bool _locked = false;

    public AudioClip clip;                  // sound to play on open or on close
    private float _clipVolume = 1.0f;       // sound volume, 1 = normal

    private static float _fadeRate = 4.0f;
    private static GUIFieldbook _instance;
    private static Canvas _canvas;
    private static GameObject _catalog;
    private static CatalogPanel _catalogPanel;
    private static GameObject _notes;
    private static CanvasGroup _canvasGroup;

    

    //-----------------------------------------------
    // Unity functions
    //-----------------------------------------------

    protected void Awake() {
        _instance = this;
        _canvas = GetComponent<Canvas>();
        _canvas.enabled = false;
        _catalog = GetComponentInChildren<CatalogPanel>(true).gameObject;
        _catalogPanel = _catalog.GetComponent<CatalogPanel>();
        _notes = GetComponentInChildren<NoteManager>(true).gameObject;
        _canvasGroup = GetComponent<CanvasGroup>();
        _canvasGroup.alpha = 0.0f;
    }

    protected void Start() {
        _catalog.SetActive(true);
        _notes.SetActive(false);
    }

    protected void OnDestroy() {
        if (_instance != null) {
            _instance = null;
        }
    }

    protected void Update() {
        if (_locked) { return; }
        if (!_canvas.enabled && _notes.activeSelf) {
            _notes.SetActive(false);
            _catalog.SetActive(true);
        }

        if (Input.GetKeyDown(toggleKey)) {
            if (GUIManager.Current == GUIState.Crosshair) {
                GUIManager.Instance.HandleStateTransitionRequest(GUIState.Inventory);
            } else if (GUIManager.Current == GUIState.Inventory) {
                GUIManager.Instance.HandleStateTransitionRequest(GUIState.Crosshair);
            }
        }
    }

    // return the number of items currently in the catalog inventory
    public static int InventoryCount {
        get { return _catalogPanel.countItems(); }
    }

    // add the param obj to the catalog inventory
    public static void addToInventory(GameObject obj) {
        _catalogPanel.addItem(obj);
    }

    public void Show() {
        _canvas.enabled = true;
        StopAllCoroutines();
        StartCoroutine(LerpUtility.FadeCanvasGroupIn(_canvasGroup, _fadeRate));
    }

    public void Hide() {
        StopAllCoroutines();
        StartCoroutine(LerpUtility.FadeCanvasGroupOut_DisableCanvasOnCompletion(_canvasGroup, _canvas, _fadeRate));
    }

    public void openNotes() {
        _notes.SetActive(true);
        _catalog.SetActive(false);
    }

    public void openCatalog() {
        _notes.SetActive(false);
        _catalog.SetActive(true);
    }

   // fade out, basic
   //IEnumerator fadeOut() {
   //     while (_canvasGroup.alpha > 0.0f) {
   //         _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 0.0f, _fadeRate * Time.deltaTime);
   //         yield return null;
   //     }
   //     _canvas.enabled = false;
   //     yield return null;
   // }

   // // fade in, basic
   // IEnumerator fadeIn() {
   //     while (_canvasGroup.alpha < 1.0f) {
   //         _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 1.0f, _fadeRate * Time.deltaTime);
   //         yield return null;
   //     }
   //     yield return null;
   // }
}
