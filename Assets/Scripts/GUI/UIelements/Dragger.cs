using UnityEngine;
using System.Collections;

public class Dragger : MonoBehaviour {

    public static Canvas m_canvas;
    public static Transform m_transform;
    public static GameObject m_itemBeingDragged;

    void Awake () {
        m_canvas = gameObject.GetComponent<Canvas>();
        m_transform = gameObject.transform;
    }

}
