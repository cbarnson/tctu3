using UnityEngine;
using System.Collections;

public class EventManagerHUD : MonoBehaviour {

    public delegate void DescriptionHandler (string[] str);
    public static event DescriptionHandler onDescription;

    public static void displayDescription (string[] str) {
        if (onDescription != null) {
            onDescription (str);
        }
    }

}
