using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DescriptionHUD : MonoBehaviour {

    public static Canvas m_canvas;
    private static Text [] m_texts;

    void Awake () {
        m_canvas = GetComponent<Canvas> ();
        m_texts = GetComponentsInChildren<Text> ();
    }

    void Start () {
        EventManagerHUD.onDescription += onDescription;
    }

    void OnDestroy () {
        EventManagerHUD.onDescription -= onDescription;
    }

    private void onDescription(string[] str) {
        if (str.Length == 0) {
            //D.log ("str is length 0");
            for (int i = 0; i < m_texts.Length; i++) {
                m_texts [i].text = "";
            }
            return;
        }

        for (int i = 0; i < m_texts.Length; i++) {
            if ((i + 1) > str.Length) { return; }
            m_texts [i].text = str [i];
        }
    }
}
