using UnityEngine;
using System.Collections;

public class ErrorFlash : MonoBehaviour {

    public static bool active = false;
    private static CanvasGroup _canvasGroup;
    public float fadeRate = 2.0f;

    void Awake() {
        _canvasGroup = gameObject.GetComponent<CanvasGroup>();
        _canvasGroup.alpha = 0.0f;
    }

    public static void OnError() {
        active = true;
        // to make it fade in faster than fade out
        _canvasGroup.alpha = 0.5f;
    }
	
	// Update is called once per frame
	void Update () {
        if (active && _canvasGroup.alpha == 1.0f) { // test: is active AND has reached full alpha value
            active = false;
        }
    }

    void FixedUpdate() {
        if (active) {
            // fade in
            _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 1.0f, fadeRate * Time.deltaTime);
        } else {
            // fade out
            _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 0.0f, fadeRate * Time.deltaTime);
        }
    }
}
