using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using util;

public class Pause : MonoBehaviour {

    public delegate void PauseEventHandler (bool isPaused);
    public static event PauseEventHandler onPause;

    public static Canvas m_canvas;
    public CanvasGroup quitGroup;

    // quit sub menu
    public Button resumeButton;
    public Button quitButton;
    public Button helpButton;
    public Button optionButton;

    void Awake () {
        m_canvas = gameObject.GetComponent<Canvas>();
        m_canvas.enabled = false;
        disableQuitGroup();
    }

    void Update () {
        if (Input.GetKeyDown(KeyCode.P)) {
            toggle ();
        }
    }


    private void enableQuitGroup() {
        quitGroup.alpha = 1.0f;
        quitGroup.interactable = true;
        quitGroup.blocksRaycasts = true;
    }

    private void disableQuitGroup() {
        quitGroup.alpha = 0.0f;
        quitGroup.interactable = false;
        quitGroup.blocksRaycasts = false;
    }

    // close the pause menu and return to game
    public void resume () {
        disableQuitGroup();
        toggle();
    }

    // open the quit sub menu and ask user if quit to desktop or quit to title screen
    public void openQuitMenu () {
        enableQuitGroup();
        setAllButtons(false);
    }

    public void cancel () {
        disableQuitGroup();
        setAllButtons(true);
    }

    public void quitToDesktop() {
        tctu.exitGame();
    }

    public void quitToMenu() {
        tctu.loadMenu();
    }

    // static method
    public static void toggle() {
        
        m_canvas.enabled = !m_canvas.enabled;
        // event to signal our new canvas state
        if (onPause != null) {
            onPause (m_canvas.enabled);
        }
        if (m_canvas.enabled) {
            Time.timeScale = 0;
        } else {
            Time.timeScale = 1;
        }


        //if (m_canvas.enabled) {
        //    Time.timeScale = 1;
        //    Pause.m_canvas.enabled = false;
        //    //if (Fieldbook.m_canvas.enabled || baseDialogue.m_canvas.enabled) {
        //    //    return;
        //    //}

        //    // check if timeline label puzzle is currently active - if so then do not activate the crosshair
        //    //if (Timeline.m_canvas.gameObject.GetComponent<TimelinePuzzle>().toBeDeactivated.activeSelf) {
        //    //    D.log("timeline puzzle active, do not activate crosshair");
        //    //    return;
        //    //}

        //    //Crosshair.activate();
        //} else {
        //    Time.timeScale = 0;
        //    //Crosshair.deactivate();
        //    Pause.m_canvas.enabled = true;
        //}
    }


    private void setAllButtons(bool set) {
        quitButton.interactable = set;
        resumeButton.interactable = set;
        helpButton.interactable = set;
        optionButton.interactable = set;
    }



}
