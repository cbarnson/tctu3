using UnityEngine;
using System.Collections;
using System;

public class Crosshair : MonoBehaviour {

    public static Canvas m_canvas;

    private static bool _locked = false;        // when locked, crosshair will not change from state when activate or deactivate are called

    void Awake() {
        m_canvas = gameObject.GetComponent<Canvas>();
        m_canvas.enabled = true;
    }

    void Start() {
        Dialoguer.events.onMessageEvent += onMessageEvent;
        Dialoguer.events.onStarted += onStarted;
        Dialoguer.events.onEnded += onEnded;
        //Pause.onPause += onPause;
        //Fieldbook.onFieldbook += onFieldbook;
        dragHandlerFieldbook.ItemDropped += onItemDropped;
        UIPuzzle.PuzzleStarted += onPuzzleStarted;          // to control Crosshair _locked member
        UIPuzzle.PuzzleEnded += onPuzzleEnded;              // to control Crosshair _locked member
    }

    private void onPuzzleEnded(object sender, UIPuzzleEventArgs e) {
        _locked = false;
    }

    private void onPuzzleStarted(object sender, UIPuzzleEventArgs e) {
        _locked = true;
    }

    // called when an ItemDropped event occurs from dragHandlerFieldbook
    private void onItemDropped(object sender, ItemDroppedEventArgs e) {
        // crosshair should activate when ResetCrosshair is true
        if (e.ResetCrosshair) { activate(); }
    }

    void OnDestroy() {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
        Dialoguer.events.onStarted -= onStarted;
        Dialoguer.events.onEnded -= onEnded;
       // Pause.onPause -= onPause;
        //Fieldbook.onFieldbook -= onFieldbook;
    }

    //private void onFieldbook(bool isFieldbookOpen) {
    //    if (isFieldbookOpen) {
    //        deactivate();
    //    } else {
    //        activate();
    //    }
    //}

    // deactivate crosshair when dialogue starts
    private void onStarted() {
        deactivate();
    }

    // activate crosshair when dialogue ends
    private void onEnded() {
        activate();
    }


    //public void onPause(bool isPauseEnabled) {
    //    if (!isPauseEnabled && !Fieldbook.m_canvas.enabled && !baseDialogue.m_canvas.enabled) {
    //        activate();
    //    } else {
    //        deactivate();
    //    }
    //}

    private void onMessageEvent(string message, string metadata) {
        if (message == "startTemplePuzzle") {
            activate();
        }
    }

    public static void activate() {
        if (_locked) { return; }
        m_canvas.enabled = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public static void deactivate() {
        if (_locked) { return; }
        m_canvas.enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

}
