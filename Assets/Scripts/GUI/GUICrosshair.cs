using UnityEngine;
using System.Collections;

// handles:
// setting cursor texture
public class GUICrosshair : MonoBehaviour {

    //public static GUICrosshair Instance {
    //    get {
    //        return _instance;
    //    }
    //}

    public static bool CrosshairCursorLocked {
        get {
            return _locked;
        }
        set {
            _locked = value;
        }
    }

    public static bool MovementEnabled {
        get {
            return _canvas.enabled;
        }
    }

    private static GUICrosshair _instance;
    private static Canvas _canvas;                  // makes no difference whether enabled initially or not
    private static bool _locked = false;

    //-----------------------------------------------
    // Unity functions
    //-----------------------------------------------

    protected void Awake() {
        _instance = this;
        _canvas = GetComponent<Canvas>();
    }

    protected void OnDestroy() {
        if (_instance != null) {
            _instance = null;
        }
    }

    protected void Start() {
        Cursor.SetCursor(ResourceManager.Instance.CursorTexture, Vector2.zero, CursorMode.ForceSoftware);
    }

    public static void Show() {
        if (_locked) { return; }
        _canvas.enabled = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public static void Hide() {
        if (_locked) { return; }
        _canvas.enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
}
