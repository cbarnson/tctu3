using UnityEngine;
using System.Collections;

public class GUIDragging : MonoBehaviour {

    // debug toggle
    private const bool GUIDRAGGING_DEBUG = true;

    public static GUIDragging Instance {
        get {
            return _instance;
        }
    }


    public static GameObject ItemUI {
        get {
            return _itemUI;
        }
        set {
            if (value) {
                _itemUI = value;
                _isDragging = true;
                return;
            } else {
                _itemUI = null;
                _isDragging = false;
                return;
            }
        }
    }

    public static GameObject Item {
        get {
            return _item;
        }
        set {
            
            if (value) { // "ToDragging"

                GUIManager.Instance.HandleStateTransitionRequest(GUIState.Dragging);
                _item = value;
                _isDragging = true;

                //if (GUIManager.Instance.HandleStateTransitionRequest("ToDragging", GUIState.Inventory)) {
                //    // valid, so complete the drag (i.e. set _item to value)
                //    _item = value;
                //    _isDragging = true;
                //    return;
                //}
                //if (GUIDRAGGING_DEBUG) { D.log("error, transtion ToDragging did not complete successfully"); }

            } else { // "ToCrosshair"

                GUIManager.Instance.HandleStateTransitionRequest(GUIState.Crosshair);
                _item = null;
                _isDragging = false;

                //if (GUIManager.Instance.HandleStateTransitionRequest("ToCrosshair", GUIState.Dragging)) {
                //    // valid, so complete the drag (i.e. set _item to value)
                //    _item = null;
                //    _isDragging = false;
                //    return;
                //}
                //if (GUIDRAGGING_DEBUG) { D.log("error, transtion ToCrosshair did not complete successfully"); }
            }
        }
    }



    public static bool IsDragging {
        get {
            return _isDragging;
        }
    }


    private static GUIDragging _instance;
    private static Canvas _canvas;
    private static GameObject _item;            // the item being dragged, null if no drag is occurring
    private static GameObject _itemUI;
    private static bool _isDragging = false;

    //-----------------------------------------------
    // Unity functions
    //-----------------------------------------------

    protected void Awake() {
        _instance = this;
        _canvas = GetComponent<Canvas>();
        _canvas.enabled = true;                 // should always be enabled
    }

    protected void OnDestroy() {
        if (_instance != null) {
            _instance = null;
        }
    }
}
