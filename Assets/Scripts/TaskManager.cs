#define DEBUG
#define WARN
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class TaskCompleteEventArgs : EventArgs {
    public TaskType TypeCompleted { get; set; }
}

public class TaskManager : MonoBehaviour {

    public event EventHandler<TaskCompleteEventArgs> TaskComplete;

    public GameObject prefabTaskObject;
    public Transform taskDisplayPanel;                          // parent for all instantiated task prefabs
    public float completionDelay = 1f;                          // delay from task completion to its removal from GUI

    private Dictionary<TaskType, GameObject> _dictionary;       // holds all active tasks for the GUI
    
    private static TaskManager _instance;
    public static TaskManager Instance {
        get { return _instance; }
    }

    // sound clips for task system
    [SerializeField]
    private AudioClip _onTaskCompleteClip;
    [SerializeField]
    private AudioClip _onNewTaskClip;

    protected void Awake() {
        _instance = this;                                       // allows other classes to access create/advance methods
        _dictionary = new Dictionary<TaskType, GameObject>();   // initialize dictionary
        D.assert(prefabTaskObject != null);
    }

    private void PlaySoundClip(AudioClip clip) {
        if (clip != null) {
            AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
        }
    }

    public void CreateNewTask(TaskType ttype, string ttext, int tstepsmax) {
        GameObject go = Instantiate(prefabTaskObject) as GameObject;
        go.transform.SetParent(taskDisplayPanel.transform);
        //go.transform.parent = taskDisplayPanel.transform;
        Task t = go.GetComponent<Task>();
        if (t != null) {
            t.Init(ttype, ttext, tstepsmax);
            _dictionary.Add(ttype, go);
            // play audioclip
            PlaySoundClip(_onNewTaskClip);
        }
    }

    public void AdvanceTask(TaskType ttype) {
        if (_dictionary.ContainsKey(ttype)) {
            Task t = _dictionary[ttype].GetComponent<Task>();
            if (t != null) {
                t.AdvanceTask();
                if (t.IsTaskComplete()) {
                    // coroutine to remove after delay
                    StartCoroutine(DelayedRemove(ttype));
                    // play audioclip
                    PlaySoundClip(_onTaskCompleteClip);
                }
                OnTaskComplete(ttype);
            }
        }
    }

    IEnumerator DelayedRemove(TaskType tt) {
        yield return new WaitForSeconds(completionDelay);
        GameObject go = _dictionary[tt];
        if (_dictionary.Remove(tt)) {
            // success, destroy the object
            if (go != null) Destroy(go);
            D.log("successfully removed task");
        } else {
            D.log("failed to remove task");
        }
    }

    protected void OnDestroy() {
        if (_instance) {
            _instance = null;
        }
        // destroy all gameobjects remaining in dictionary
        foreach (var item in _dictionary.Values) {
            Destroy(item);
        }
        _dictionary.Clear();
    }

    /// <summary>
    /// Event publisher called immediately upon task completion (i.e. task was created and advanced to completion). 
    /// Note that the task will not be deactivated and disappear from the screen until the delay in RemoveTask has elapsed.
    /// </summary>
    /// <param name="taskType"></param>
    protected virtual void OnTaskComplete(TaskType taskType) {
        if (TaskComplete != null) {
            TaskComplete(this, new TaskCompleteEventArgs() { TypeCompleted = taskType });
        }
    }



    /// <summary>
    /// Holds the "pool" of Task objects that will be active if in use, not active if ready for use.
    /// Fixed size of array is determined by parameters set in Unity Editor
    /// </summary>
    //public Task[] taskPool;

    

    /// <summary>
    /// There is an element in vec2StartPosition for every element in taskPool.  Initially, in Awake(), 
    /// every RectTransform associated with each element of taskPool's gameobject will have its original 
    /// anchoredPosition stored in vec2StartPosition.  These are used to visually simulate the pseudo-"queue"
    /// action of the TaskManager buffer in game.
    /// </summary>
    //private Vector2[] vec2StartPosition;
    /// <summary>
    /// How long the just completed task will remain on screen before being removed
    /// </summary>
    //private float delayRemoveAfterComplete = 1f;
    /// <summary>
    /// Maintains a list of integers representing the index of the taskPool objects in active use, and their
    /// order from earliest added to latest added.
    /// </summary>
    //private List<int> orderList = new List<int>();

    

    

    // store all initial anchoredPositions of each element of taskPool into vec2StartPosition
//    void Awake() {
//        _instance = this;
//        vec2StartPosition = new Vector2[taskPool.Length];
//        for (int i = 0; i < taskPool.Length; i++) {
//            RectTransform rt = taskPool[i].gameObject.GetComponent(typeof(RectTransform)) as RectTransform;
//            if (rt != null) {
//#if DEBUG
//                D.log("anchoredPosition " + i + " is " + rt.anchoredPosition);
//#endif
//                vec2StartPosition[i] = rt.anchoredPosition;
//            } else {
//#if WARN
//                D.warn("Could not access RectTransform from taskPool[" + i + "] gameObject");
//#endif
//            }
//            // disable the gameobjects
//            taskPool[i].gameObject.SetActive(false);
//        }
//    }

    



    /// <summary>
    /// Advances the internal counter of the first active task matching the provided TaskType parameter, if it exists in orderList.
    /// If the task was not complete, but completes after the advance step, the coroutine RemoveTask is called to remove the task
    /// after a delay.
    /// </summary>
    /// <param name="ttype"></param>
//    public void AdvanceTask(TaskType ttype) {
//        foreach (int i in orderList) {
//            if (taskPool[i].gameObject.activeSelf && taskPool[i].IsTaskType(ttype)) {
//                if (!taskPool[i].IsTaskComplete()) {
//                    taskPool[i].AdvanceTask();
//                    if (taskPool[i].IsTaskComplete()) {     // just completed
//                        StartCoroutine(RemoveTask(i));      // don't want this is fire more than once per task
//                        if (_onTaskCompleteClip != null) {
//                            AudioSource.PlayClipAtPoint(_onTaskCompleteClip, Camera.main.transform.position);
//                        }
//                        OnTaskComplete(ttype);              // call event publisher method for this TaskType
//#if DEBUG
//                        D.log("remove task, taskPool[" + i + "[");
//#endif
//                    }
//                    return;
//                }
//            }
//        }
//    }

    /// <summary>
    /// Looks for the first index of taskPool for which that gameobject is not active, fills with info about the new task.
    /// </summary>
    /// <param name="ttype"></param>
    /// <param name="ttext"></param>
    /// <param name="tstepsmax"></param>
//    public void CreateNewTask(TaskType ttype, string ttext, int tstepsmax) {
//        int i = NextAvailableObject();
//        if (i == -1) return;
//        int destinationIndexVec2StartPosition = CountActiveTasks();
//        // turn on object
//#if DEBUG
//        D.log("Activating taskPool[" + i + "]");
//#endif
//        taskPool[i].gameObject.SetActive(true);
//        // track order in orderList
//        orderList.Add(i);
//        // initialize values for this object
//        Task ttarget = taskPool[i].GetComponent(typeof(Task)) as Task;
//        if (ttarget != null) ttarget.Init(ttype, ttext, tstepsmax);
//        // start from lowest position, will move to top
//        RectTransform rt = ttarget.gameObject.GetComponent(typeof(RectTransform)) as RectTransform;
//        if (rt != null) rt.anchoredPosition = vec2StartPosition[taskPool.Length - 1];
//        // lerp to target Vector2 position on screen
//        MoveTaskPosition(i, destinationIndexVec2StartPosition);
//        //StartCoroutine(LerpUtility.MoveTowardsAnchored_V2(rt, vec2StartPosition[destinationIndexVec2StartPosition], 600f));
//        // play audioclip
//        if (_onNewTaskClip != null) { AudioSource.PlayClipAtPoint(_onNewTaskClip, Camera.main.transform.position); }
//    }

    /// <summary>
    /// After a delay, deactivates the gameobject, and removes that index (taskPool) from the orderList of active tasks.
    /// </summary>
    /// <param name="taskPoolIndexToRemove"></param>
    /// <returns></returns>
    //IEnumerator RemoveTask(int taskPoolIndexToRemove) {
    //    yield return new WaitForSeconds(delayRemoveAfterComplete);
    //    // disable
    //    taskPool[taskPoolIndexToRemove].gameObject.SetActive(false);
    //    // remove from list
    //    RemoveFromOrderList(taskPoolIndexToRemove);
    //}

    //private void RemoveFromOrderList(int i) {
    //    // remove from list
    //    int toRemove = orderList.IndexOf(i);
    //    D.assert(toRemove != -1);
    //    // update position of others past it
    //    for (int j = toRemove + 1; j < orderList.Count; j++) { // move each from 
    //        MoveTaskPosition(orderList[j], j - 1);
    //    }
    //    // remove from list
    //    orderList.RemoveAt(toRemove);
    //}


    /// <summary>
    /// Lerps the anchoredPosition of RectTransform of taskPool[taskPoolIndex] gameobject until it matches that of object 
    /// specified in vec2StartPosition.
    /// </summary>
    /// <param name="taskPoolIndex"></param>
    /// <param name="vec2StartPositionIndex"></param>
    //private void MoveTaskPosition(int taskPoolIndex, int vec2StartPositionIndex) {
    //    RectTransform rt = taskPool[taskPoolIndex].gameObject.GetComponent(typeof(RectTransform)) as RectTransform;
    //    if (rt != null) {
    //        // stop all coroutines on this Task gameobject
    //        taskPool[taskPoolIndex].StopAllCoroutines();
    //        StartCoroutine(LerpUtility.MoveTowardsAnchored_V2(rt, vec2StartPosition[vec2StartPositionIndex], 600f));
    //    }
    //}

    /// <summary>
    /// Returns the count of gameobjects that are currently active for each element of taskPool.
    /// </summary>
    /// <returns></returns>
    //private int CountActiveTasks() {
    //    int count = 0;
    //    for (int i = 0; i < taskPool.Length; i++) {
    //        if (taskPool[i].gameObject.activeSelf) count++;
    //    }
    //    return count;
    //}
    /// <summary>
    /// Returns the first index of the array taskPool for which that element is not active, thus available for use.  
    /// Returns -1 if all active.
    /// </summary>
    /// <returns></returns>
//    private int NextAvailableObject() {
//        for (int i = 0; i < taskPool.Length; i++) {
//            if (!taskPool[i].gameObject.activeSelf) {
//                return i;
//            }
//        }
//#if WARN
//        D.warn("failed to create new task, no available object");
//#endif
//        return -1;
//    }
}
