using UnityEngine;
using System.Collections;
using System;

public class TaskCreator : MonoBehaviour {

    [System.Serializable]
    public class TaskCreated {
        public bool isUsed = false;
        public TaskType taskType;
        public string taskText;
        public int taskStepsMax = 1;
    }
    [System.Serializable]
    public class OnCompleteTrigger : TaskCreated {
        public TaskType taskTypeTrigger;
    }
    [System.Serializable]
    public class OnMessageTrigger : TaskCreated {
        public string messageEvent;
    }
    [System.Serializable]
    public class OnStartTrigger : TaskCreated {
        public float delayAfterStart = 0.0f;
    }

    /// <summary>
    /// Array, set in inspector, that holds info describing which tasks should be created upon the completion of another.
    /// </summary>
    public OnCompleteTrigger[] onCompleteTriggers;
    /// <summary>
    /// Array, set in inspector, that holds info describing which tasks should be created upon receiving dialoguer message event.
    /// </summary>
    public OnMessageTrigger[] onMessageTriggers;
    /// <summary>
    /// Which tasks should be created upon level start and delayed by a given amount (seconds).
    /// </summary>
    public OnStartTrigger[] onStartTriggers;

    /// <summary>
    /// Subscribes to events from TaskManager and Dialoguer for TaskComplete and onMessageEvent respectively.
    /// Also, starts coroutines for each element in onStartTriggers, each will be created after its specified delay.
    /// </summary>
    protected void Start() {
        TaskManager.Instance.TaskComplete += onTaskComplete;
        Dialoguer.events.onMessageEvent += onMessageEvent;
        for (int i = 0; i < onStartTriggers.Length; i++) {
            if (!onStartTriggers[i].isUsed) {
                StartCoroutine(DelayedCreate(onStartTriggers[i]));
                onStartTriggers[i].isUsed = true;
            }
        }
    }
    /// <summary>
    /// Must unhook from listening on gameobject destroyed.
    /// </summary>
    protected void OnDestroy() {
        if (TaskManager.Instance)
            TaskManager.Instance.TaskComplete -= onTaskComplete;
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    IEnumerator DelayedCreate(OnStartTrigger onStartTrigger) {
        yield return new WaitForSeconds(onStartTrigger.delayAfterStart);
        D.log("TaskCreator, delayed create task: " + onStartTrigger.taskType.ToString() + " is now created");
        TaskManager.Instance.CreateNewTask(onStartTrigger.taskType, onStartTrigger.taskText, onStartTrigger.taskStepsMax);
    }

    private void onMessageEvent(string message, string metadata) {
        for (int i = 0; i < onMessageTriggers.Length; i++) {
            if (!onMessageTriggers[i].isUsed && onMessageTriggers[i].messageEvent == message) {
                D.log("TaskCreator, messageEvent: " + message + ", triggered creation of " + onMessageTriggers[i].taskType.ToString());
                TaskManager.Instance.CreateNewTask(onMessageTriggers[i].taskType, onMessageTriggers[i].taskText, 
                    onMessageTriggers[i].taskStepsMax);
                onMessageTriggers[i].isUsed = true; // use only once
            }
        }
    }

    private void onTaskComplete(object sender, TaskCompleteEventArgs e) {
        for (int i = 0; i < onCompleteTriggers.Length; i++) {
            if (!onCompleteTriggers[i].isUsed && onCompleteTriggers[i].taskTypeTrigger == e.TypeCompleted) {
                D.log("TaskCreator, completed TaskType: " + e.TypeCompleted + ", triggered creation of " + 
                    onCompleteTriggers[i].taskType.ToString());
                TaskManager.Instance.CreateNewTask(onCompleteTriggers[i].taskType, onCompleteTriggers[i].taskText,
                    onCompleteTriggers[i].taskStepsMax);
                onCompleteTriggers[i].isUsed = true;
            }
        }
    }
}
