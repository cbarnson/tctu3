﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveOnTaskComplete : MonoBehaviour {

    public GameObject objToSet;
    public TaskType taskType;
    public bool targetState;

	protected void Start() {
        TaskManager.Instance.TaskComplete += OnTaskComplete;
    }

    protected void OnDestroy() {
        if (TaskManager.Instance != null) {
            TaskManager.Instance.TaskComplete -= OnTaskComplete;
        }
    }

    private void OnTaskComplete(object sender, TaskCompleteEventArgs e) {
        if (e.TypeCompleted == taskType) {
            if (objToSet != null) {
                objToSet.SetActive(targetState);
            }
        }
    }
}
