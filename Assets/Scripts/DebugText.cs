using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DebugText : MonoBehaviour {

    public Text text;

    public static DebugText Instance {
        get {
            return _instance;
        }
    }
    private static DebugText _instance;

    void Awake() {
        _instance = this;
    }

    public static void ShowDebugText(string s) {
        if (_instance.text) _instance.text.text = s;

    }

    public static void ClearDebugText() {
        if (_instance.text) _instance.text.text = "";
    }

}
