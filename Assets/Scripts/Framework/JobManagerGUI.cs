using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// class purely for the purpose of demoing how the JobManager can be used to manage coroutines,
// even those with child coroutines
public class JobManagerGUI : MonoBehaviour {

    private Job _job;

    void OnGUI () {
        int buttonHeight = 60;

        if (GUI.Button(new Rect(5, 5, 200, buttonHeight), "make and start 5 iteration job")) {
            _job = Job.make (writeLog (5));
            _job.jobComplete += (wasKilled) => { D.log ("job done, was it killed? " + wasKilled); };
        }

        if (GUI.Button (new Rect (5, 100, 200, buttonHeight), "make job paused")) {
            _job = Job.make (endlessLog (), false); // not actually starting the job, only creating
            _job.jobComplete += (wasKilled) => { D.log ("job done, was it killed? " + wasKilled); };
        }

        if (GUI.Button (new Rect (5, 100 + buttonHeight, 200, buttonHeight), "start made job")) {
            _job.start ();
        }

        if (GUI.Button (new Rect (5, 100 + buttonHeight * 2, 200, buttonHeight), "pause")) {
            _job.pause ();
        }

        if (GUI.Button (new Rect (5, 100 + buttonHeight * 3, 200, buttonHeight), "unpause")) {
            _job.unpause ();
        }

        if (GUI.Button (new Rect (5, 100 + buttonHeight * 4, 200, buttonHeight), "kill immediately")) {
            _job.kill ();
        }

        if (GUI.Button (new Rect (5, 100 + buttonHeight * 5, 200, buttonHeight), "kill after 3s delay")) {
            _job.kill (3);
        }

        // child jobs
        var xPos = Screen.width - 205;
        if (GUI.Button (new Rect(xPos, 5, 200, buttonHeight), "run job with 5 children")) {
            var j = Job.make (printAfterDelay ("im the parent job", 1), false);
            j.jobComplete += (wasKilled) => { D.log ("parent job done"); };
            for (var i = 1; i <= 5; i++) {
                var text = "Job Number: " + i;
                j.createAndAddChildJob (printAfterDelay (text, 1));
            }
            j.start ();
        }
        

    }



    IEnumerator writeLog (int totalTimes) {
        var i = 0;
        var wfs = new WaitForSeconds (1);
        while (i <= totalTimes) {
            D.log (string.Format ("writing log {0} of {1}", i, totalTimes));
            i++;
            yield return wfs;
        }
    }

    // continuously write to log  - with JobManager we can control it
    IEnumerator endlessLog () {
        var i = 0;
        var wfs = new WaitForSeconds (1);
        for (;;) {
            D.log ("writing endless log: " + i);
            i++;
            yield return wfs;
        }
    }

    // print text after delay
    IEnumerator printAfterDelay (string text, float delay) {
        yield return new WaitForSeconds (delay);
        D.log ("printAfterDelay: " + text);
    }
}
