using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Assertions;
using System.Collections;

using util;

[RequireComponent (typeof(AudioSource))]

// class for video playback using the MovieTexture
public class Video : MonoBehaviour {

    public string _filename;
    private MovieTexture _movie;

	// Use this for initialization
	void Start() {
        _movie = Resources.Load("MovieTextures/" + _filename, typeof(MovieTexture)) as MovieTexture;
        Assert.IsNotNull(_movie);
        // set mainTexture to our _movie parameter
        GetComponent<Renderer>().material.mainTexture = 
            _movie as MovieTexture;
        // get audio from our _movie parameter
        GetComponent<AudioSource>().clip = _movie.audioClip;
        // play audio
        GetComponent<AudioSource>().Play(); 
        // play video
        _movie.Play();  
	}

    // continue to museum level if the video is done
    void Update() {
        // key to exit game -- may not be necessary in the end
        if (Input.GetKeyUp(KeyCode.Escape)) {
            tctu.exitGame();
        }
        // skip _movie clip if any button hit or if the _movie ends
        if (!_movie.isPlaying || Input.anyKeyDown) {
            tctu.loadNextScene();
        }
    }

}
