using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// use to override setting for number of grid columns and rows at runtime
// attach script to UI element with a grid layout group component
public class DynamicGrid : MonoBehaviour {

    public int row, col;

	// Use this for initialization
	void Start () {

        RectTransform parent = gameObject.GetComponent<RectTransform>();
        GridLayoutGroup grid = gameObject.GetComponent<GridLayoutGroup>();

        grid.cellSize = new Vector2(parent.rect.width / col, parent.rect.height / row);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
