using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;

// use DialoguerDialogues to access enum for dialogue sequences

namespace util {

    // to add files:
    // 1. increment the corresponding NUMBER_OF ...
    // 2. add the string filename to the appropriate static string array
    // 3. add the shortened type to be referenced in Dialoguer (i.e. "curator_angry" => "angry" under CuratorType)
    public static class constant {

    }


    public static class tctu {

        public static string GenTimeSpanFromSeconds(double seconds) {
            // Create a TimeSpan object and TimeSpan string from 
            // a number of seconds.
            TimeSpan interval = TimeSpan.FromSeconds(seconds);
            string timeInterval = interval.ToString();

            // Pad the end of the TimeSpan string with spaces if it 
            // does not contain milliseconds.
            int pIndex = timeInterval.IndexOf(':');
            pIndex = timeInterval.IndexOf('.', pIndex);
            // if (pIndex < 0) timeInterval += "        ";

            return timeInterval;
            //Console.WriteLine("{0,21}{1,26}", seconds, timeInterval);
        }

        public static void loadLevelSummary() {
            // create the level summary object, which will spawn through ResourceManager, and automatically fade in
            ResourceManager.Instance.CreateLevelSummaryCanvasPrefab();
            // lock the player so they may not move, i.e. simulate that the player is no longer in the scene

            // WAS CAUSING PROBLEMS WITH LOCKING IN THE FOLLOWING LEVEL AFTER THE LEVEL SUMMARY WAS DISMISSED
            //FPController.isMoveLocked = true;
            //FPController.isMouseLocked = true;
        }

        public static string getCurrentSceneName() {
            return SceneManager.GetActiveScene().name;
        }

        public static void loadNextScene () {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        // LoadSceneMode.Single closes all loaded scenes and loads a scene
        // LoadSceneMode.Additive adds the scene to the current loaded scenes
        public static void loadSceneByName (string sceneName, LoadSceneMode mode = LoadSceneMode.Single) {
            SceneManager.LoadScene (sceneName, mode);
        }

        public static void reloadCurrentScene () {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public static void loadMenu () {
            SceneManager.LoadScene (0);
        }

        public static void exitGame () {
            Application.Quit ();
        }

        public static void Swap<T> (ref T lhs, ref T rhs) {
            T temp;
            temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        //public static IEnumerator MoveTowards_V3(GameObject go, Vector3 target, float speed) {
        //    while (go.transform.position != target) {
        //        go.transform.position = Vector3.MoveTowards(go.transform.position, target, Time.deltaTime * speed);
        //        yield return null;
        //    }
        //    D.log("MoveTowards_V3 completed for gameObject: " + go.name);
        //}

        //public static IEnumerator MoveTowardsLocal_V3(GameObject go, Vector3 target, float speed) {
        //    while (go.transform.localPosition != target) {
        //        go.transform.localPosition = Vector3.MoveTowards(go.transform.localPosition, target, Time.deltaTime * speed);
        //        yield return null;
        //    }
        //    D.log("MoveTowards_V3 completed for gameObject: " + go.name);
        //}

    }
}
