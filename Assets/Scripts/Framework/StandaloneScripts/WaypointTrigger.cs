using UnityEngine;
using System.Collections;

public class WaypointTrigger : MonoBehaviour {

    public GameObject waypointRoot;
    public GameObject walker;
    public GameObject particleSystemPrefab;       // instantiate instance, set parent to walker
    public string tagTrigger = "Player";

    private GameObject _particleSystemInstance;

    protected void Awake() {
        D.assert(waypointRoot != null);
        D.assert(walker != null);
        D.assert(particleSystemPrefab != null);
    }

    protected void Start() {
        waypointRoot.SetActive(false);
        _particleSystemInstance = (GameObject)Instantiate(particleSystemPrefab, walker.transform, false);
    }

    protected void OnDestroy() {
        if (_particleSystemInstance != null)
            Destroy(_particleSystemInstance); // destroy the instantiated object
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == tagTrigger) {
            waypointRoot.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == tagTrigger) {
            waypointRoot.SetActive(false);
        }
    }
}
