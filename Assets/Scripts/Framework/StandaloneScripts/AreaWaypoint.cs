using UnityEngine;
using System.Collections;

// whenever object tagged "Player" enters the collider trigger, the 4 node
// waypoint will begin to pulse.  When the "Player" exits the collider,
// the waypoint object will be disabled
public class AreaWaypoint : MonoBehaviour {

    public GameObject waypoint;

	// Use this for initialization
	void Start () {
        D.assert (waypoint != null);
        // by default, set to inactive
        waypoint.SetActive (false);
	}
	
    void OnTriggerEnter (Collider other) {
        if (other.tag == "Player") {
            D.log ("area waypoint: {0} enabled", gameObject.name);
            waypoint.SetActive (true);
        }
    }

    void OnTriggerExit (Collider other) {
        if (other.tag == "Player") {
            D.log ("area waypoint: {0} disabled", gameObject.name);
            waypoint.SetActive (false);
        }
    }
}
