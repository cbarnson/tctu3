using UnityEngine;
using System.Collections;

public class DragHandler3D : MonoBehaviour {

    void OnMouseDrag () {
        // get distance from object to player camera
        float distance_to_screen = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
        // set dragged object position to new xy, but same distance from camera
        transform.position = Camera.main.ScreenToWorldPoint(
            new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance_to_screen));

    }
}
