using UnityEngine;
using System.Collections;

public class ResourceManager : MonoBehaviour {

    static ResourceManager _instance = null;

    public static ResourceManager Instance {
        get {
            if (!_instance) {
                // check if an JobManager is already available in the scene graph
                _instance = FindObjectOfType(typeof(ResourceManager)) as ResourceManager;

                // nope, create a new one
                if (!_instance) {
                    var obj = new GameObject("ResourceManager");
                    _instance = obj.AddComponent<ResourceManager>();
                }
            }
            return _instance;
        }
    }


    //-----------------------------------------------
    // resource files
    //-----------------------------------------------
    private static string CursorTextureFile = "cursor";
    private static string CompleteTextureFile = "complete";
    private static string IncompleteTextureFile = "incomplete";
    private static string LevelSummaryCanvasPrefabFile = "LevelSummaryCanvasPrefab";


    //-----------------------------------------------
    // Portrait assets
    //-----------------------------------------------

    private enum character {
        curator,
        collectress,
        oracle,
        young,          // brown-blue

        // BGR
        old_blue,       // grey-blue
        old_green,      // grey-green
        old_red,        // grey-red

        // BGR
        middle_blue,    // brown-blue
        middle_green,   // brown-green
        middle_red,     // brown-red

        count
    }

    private Texture2D[][] portraits;
    private int[] characterPortraitCount = { 5, 5, 3, 4, 4, 4, 4, 4, 4, 4 };
    private string[][] portraitNames = {
        new string[] {"angry",  "happy",    "inquisitive",  "neutral", "sad" },     // curator
        new string[] {"angry",  "happy",    "mischevious",  "neutral", "sad" },     // collectress
        new string[] {"happy",  "neutral",  "black" },                              // oracle

        new string[] { "sad",   "neutral",  "happy",        "angry" },              // young - brown/blue

        new string[] { "sad",   "neutral",  "happy",        "angry" },              // blue
        new string[] { "sad",   "neutral",  "happy",        "angry" },              // green
        new string[] { "sad",   "neutral",  "happy",        "angry" },              // red

        new string[] { "sad",   "neutral",  "happy",        "angry" },              // blue
        new string[] { "sad",   "neutral",  "happy",        "angry" },              // green
        new string[] { "sad",   "neutral",  "happy",        "angry" }               // red
    };

    private string[][] portraitFileNames = {
        new string[] { "curator_angry",             "curator_happy",                "curator_inquisitive",      "curator_neutral",      "curator_sad" },
        new string[] { "collectress_angry",         "collectress_happy",            "collectress_mischeivious", "collectress_neutral",  "collectress_sad" },   
        new string[] { "oracle_happy",              "oracle_neutral",               "oracle_black" },            
                                                      
        new string[] { "young_brown_blue_sad",      "young_brown_blue_neutral",     "young_brown_blue_happy",   "young_brown_blue_angry" },  

        new string[] { "old_grey_blue_sad",         "old_grey_blue_neutral",        "old_grey_blue_happy",      "old_grey_blue_angry" }, 
        new string[] { "old_grey_green_sad",        "old_grey_green_neutral",       "old_grey_green_happy",     "old_grey_green_angry" },           
        new string[] { "old_grey_red_sad",          "old_grey_red_neutral",         "old_grey_red_happy",       "old_grey_red_angry" },             

        new string[] { "middle_brown_blue_sad",     "middle_brown_blue_neutral",    "middle_brown_blue_happy",  "middle_brown_blue_angry" },          
        new string[] { "middle_brown_green_sad",    "middle_brown_green_neutral",   "middle_brown_green_happy", "middle_brown_green_angry" }, 
        new string[] { "middle_brown_red_sad",      "middle_brown_red_neutral",     "middle_brown_red_happy",   "middle_brown_red_angry" }
    };


    //-----------------------------------------------
    // get properties
    //-----------------------------------------------
    public Texture2D CursorTexture {
        get {
            var temp = Resources.Load(CursorTextureFile, typeof(Texture2D)) as Texture2D;
            return temp;
        }
    }
    public Texture2D CompleteTexture {
        get {
            var temp = Resources.Load(CompleteTextureFile, typeof(Texture2D)) as Texture2D;
            return temp;
        }
    }
    public Texture2D IncompleteTexture {
        get {
            var temp = Resources.Load(IncompleteTextureFile, typeof(Texture2D)) as Texture2D;
            return temp;
        }
    }

    //-----------------------------------------------
    // Unity methods
    //-----------------------------------------------
    protected void Awake() {
        portraits = new Texture2D[(int)character.count][];
        for (int i = 0; i < (int)character.count; i++) {
            portraits[i] = new Texture2D[characterPortraitCount[i]];
            for (int j = 0; j < characterPortraitCount[i]; j++) {
                portraits[i][j] = Resources.Load(portraitFileNames[i][j], typeof(Texture2D)) as Texture2D;
            }
        }
        //D.log("all portraits loaded");
    }

    //protected void Update() {
    //    if (Input.GetKeyDown(KeyCode.I)) {
    //        for (int i = 0; i < 10; i++) {
    //            D.log("portraits[" + i + "] size is: " + portraits[i].Length);
    //        }
    //    }
    //}


    //-----------------------------------------------
    // Public methods
    //-----------------------------------------------

    // looks in scene hierarchy, if there is no GUILevelSummary, loads the prefab and instantiates it
    public void CreateLevelSummaryCanvasPrefab() {
        GUILevelSummary tempLevelSummary = null;
        tempLevelSummary = FindObjectOfType<GUILevelSummary>();
        if (!tempLevelSummary) {
            var temp = Resources.Load(LevelSummaryCanvasPrefabFile, typeof(GameObject)) as GameObject;
            Instantiate(temp);
        }
    }

    public Texture2D PortraitGetTexture(string name, string type) {

        int nameIndex = 0, typeIndex = 0;
        FindPortrait(ref nameIndex, ref typeIndex, name, type);
        return portraits[nameIndex][typeIndex];
    }

    private void FindPortrait(ref int nameIndex, ref int typeIndex, string name, string type) {
        // locate index of name in enum characters
        for (int i = 0; i < (int)character.count; i++) {
            string enumName = ((character) i).ToString();
            if (name != enumName) { continue; }
            // name is enumName, now find type
            nameIndex = i;
            for (int j = 0; j < characterPortraitCount[i]; j++) {
                if (portraitNames[nameIndex][j] == type) {
                    typeIndex = j;
                    return;
                }
            }
        }
    }

    //-----------------------------------------------
    // private helper methods
    //-----------------------------------------------

    //private int PortraitGetIndexFromType(int nameIndex, string type) {
    //    D.log("PortraitGetIndexFromType searching for type: " + type);
    //    for (int i = 0; i < characterPortraitCount[i]; i++) {
    //        D.log("portraitNames[nameIndex][" + i + "] is: " + portraitNames[nameIndex][i]);
    //        if (portraitNames[nameIndex][i] == type) {
    //            return i;
    //        }
    //    }
    //    return -1;
    //}

    //private int PortraitGetIndexFromName(string name) {
    //    for (int i = 0; i < (int)character.count; i++) {
    //        string enumName = ((character)i).ToString();
    //        //D.log("the interpreted enum to string name is: " + enumName);
    //        if (name == enumName) { return i; }
    //    }
    //    return -1;
    //}
}
