using UnityEngine;
using System.Collections;
using System;

public class UIPuzzleEventArgs : System.EventArgs {
    public string LevelName { get; set; }
    public string PuzzleName { get; set; }  // all lower case, no spaces; for names of more than 1 word,
                                            // should match the form "puzzleNameMultipleWords"
    public int ErrorCount { get; set; }
}

public static class UIPuzzle {

    public static event EventHandler<UIPuzzleEventArgs> PuzzleStarted;
    public static event EventHandler<UIPuzzleEventArgs> PuzzleEnded;

    public static void onPuzzleStarted (object sender, UIPuzzleEventArgs e) {
        if (PuzzleStarted != null) {
            PuzzleStarted(sender, e);
        }
    }

    public static void onPuzzleEnded(object sender, UIPuzzleEventArgs e) {
        if (PuzzleEnded != null) {
            PuzzleEnded(sender, e);
        }
    }

}
