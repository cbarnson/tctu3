using UnityEngine;
using System.Collections;

public class TracePath : MonoBehaviour {

    public Transform start;
    public Transform end;
    public float traceSpeed = 10.0f;

    private float height = 0f;

	// Use this for initialization
	void Start () {
        Vector3 startPos = new Vector3(start.position.x, height, start.position.z);
        transform.position = startPos;
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    void FixedUpdate () {
        transform.position = Vector3.MoveTowards(transform.position, end.position, traceSpeed * Time.deltaTime);
        if (transform.position == end.position) {
            Vector3 startPos = new Vector3(start.position.x, height, start.position.z);
            transform.position = startPos;
        }
    }
}
