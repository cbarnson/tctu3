using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour {

    // PUBLIC
    public float fadeInRate = 2.0f;
    public float fadeOutRate = 2.0f;
    public float delaySeconds = 2.0f;
    // PRIVATE
    private CanvasGroup _canvasGroup;

    void Awake () {
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    public void CancelRoutines() {
        StopAllCoroutines();
    }

    public void FadeMeOut() {
        StartCoroutine(FadeOut());
    }

    public void FadeMeIn() {
        StartCoroutine(FadeIn());
    }

    public void FadeMeInOut() {
        StartCoroutine(FadeInOut());
    }

    public void FadeMeInOutDelayed() {
        StartCoroutine(FadeInOutDelayed());
    }

    public void FadeMeInOutDelayed(float fadeStartDelay) {
        StartCoroutine(FadeInOutDelayed(fadeStartDelay));
    }

    // fade in, wait, then fade out
    IEnumerator FadeInOutDelayed() {
        while (_canvasGroup.alpha < 1.0f) {
            _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 1.0f, fadeInRate * Time.deltaTime);
            yield return null;
        }
        _canvasGroup.interactable = false;
        yield return new WaitForSeconds(delaySeconds);
        StartCoroutine(FadeOut());
    }

    // fade in, wait, then fade out
    IEnumerator FadeInOutDelayed(float fadeStartDelay) {
        yield return new WaitForSeconds(fadeStartDelay);
        while (_canvasGroup.alpha < 1.0f) {
            _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 1.0f, fadeInRate * Time.deltaTime);
            yield return null;
        }
        _canvasGroup.interactable = false;
        yield return new WaitForSeconds(delaySeconds);
        StartCoroutine(FadeOut());
    }

    // fade in, fade out, no delay
    IEnumerator FadeInOut() {
        while (_canvasGroup.alpha < 1.0f) {
            _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 1.0f, fadeInRate * Time.deltaTime);
            yield return null;
        }
        _canvasGroup.interactable = false;
        StartCoroutine(FadeOut());
        yield return null;
    }

    // fade out, basic
    IEnumerator FadeOut() {
        while (_canvasGroup.alpha > 0.0f) {
            _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 0.0f, fadeOutRate * Time.deltaTime);
            yield return null;
        }
        //_canvasGroup.interactable = false;
        yield return null;
    }

    // fade in, basic
    IEnumerator FadeIn() {
        while (_canvasGroup.alpha < 1.0f) {
            _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 1.0f, fadeInRate * Time.deltaTime);
            yield return null;
        }
        //_canvasGroup.interactable = false;
        yield return null;
    }
}
