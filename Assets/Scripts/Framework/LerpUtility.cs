﻿using UnityEngine;
using System.Collections;

public static class LerpUtility {

    // move position towards a target Vector3
    public static IEnumerator MoveTowards_V3(GameObject go, Vector3 target, float speed) {
        while (go.transform.position != target) {
            go.transform.position = Vector3.MoveTowards(go.transform.position, target, Time.deltaTime * speed);
            yield return null;
        }
    }
    public static IEnumerator MoveTowards_V3(Transform current, Vector3 target, float speed) {
        while (current.position != target) {
            current.position = Vector3.MoveTowards(current.position, target, Time.deltaTime * speed);
            yield return null;
        }
    }

    // move local position towards a target Vector3
    public static IEnumerator MoveTowardsLocal_V3(GameObject go, Vector3 target, float speed) {
        while (go.transform.localPosition != target) {
            go.transform.localPosition = Vector3.MoveTowards(go.transform.localPosition, target, Time.deltaTime * speed);
            yield return null;
        }
    }
    public static IEnumerator MoveTowardsLocal_V3(Transform current, Vector3 target, float speed) {
        while (current.localPosition != target) {
            current.localPosition = Vector3.MoveTowards(current.localPosition, target, Time.deltaTime * speed);
            yield return null;
        }
    }

    public static IEnumerator MoveTowardsAnchored_V2(RectTransform current, Vector2 target, float speed) {
        while (current.anchoredPosition != target) {
            current.anchoredPosition = Vector2.MoveTowards(current.anchoredPosition, target, speed * Time.deltaTime);
            yield return null;
        }
        //_canvas.enabled = false;
        //yield return null;
    }

    public static IEnumerator MoveTowardsAnchored_V2_DisableCanvasOnCompletion(RectTransform current, Vector2 target, float speed, Canvas canvasToDisable) {
        while (current.anchoredPosition != target) {
            current.anchoredPosition = Vector2.MoveTowards(current.anchoredPosition, target, speed * Time.deltaTime);
            yield return null;
        }
        canvasToDisable.enabled = false;
        //yield return null;
    }

    public static IEnumerator FadeCanvasGroupIn(CanvasGroup canvasGroup, float speed = 4.0f) {
        while (canvasGroup.alpha < 1.0f) {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 1.0f, speed * Time.deltaTime);
            yield return null;
        }
    }

    public static IEnumerator FadeCanvasGroupOut_DisableCanvasOnCompletion(CanvasGroup canvasGroup, Canvas canvasToDisable, float speed = 4.0f) {
        while (canvasGroup.alpha > 0.0f) {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 0.0f, speed * Time.deltaTime);
            yield return null;
        }
        canvasToDisable.enabled = false;
    }

    public static IEnumerator FadeCanvasGroupOut(CanvasGroup canvasGroup, float speed = 4.0f) {
        while (canvasGroup.alpha > 0.0f) {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 0.0f, speed * Time.deltaTime);
            yield return null;
        }
    }

    // fade out, basic
    //IEnumerator fadeOut() {
    //    while (_canvasGroup.alpha > 0.0f) {
    //        _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 0.0f, _fadeRate * Time.deltaTime);
    //        yield return null;
    //    }
    //    _canvas.enabled = false;
    //    yield return null;
    //}

    //// fade in, basic
    //IEnumerator fadeIn() {
    //    while (_canvasGroup.alpha < 1.0f) {
    //        _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 1.0f, _fadeRate * Time.deltaTime);
    //        yield return null;
    //    }
    //    yield return null;
    //}

}
