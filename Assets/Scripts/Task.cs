using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;
using System.Collections;

/// <summary>
/// Unique identifier for each task used in the game.  The first prefixed word specifies the 
/// level in which it is used.
/// </summary>
public enum TaskType {
    museum_speak_start,
    museum_pickup,
    museum_open,
    museum_match,
    museum_speak_end,
    acropolis_speak_start,
    acropolis_timeline,
    acropolis_select,
    aphaia_speak_start,
    aphaia_match,
    aphaia_pediment,
    propylaia_speak_guard,
    propylaia_dialogue_guard,
    propylaia_speak_statue,
    propylaia_statue,
    propylaia_speak_column,
    propylaia_column,
    propylaia_speak_laocoon,
    propylaia_dialogue_laocoon,
    propylaia_speak_laocoon_oracle
}


public class Task : MonoBehaviour {

    private TaskType taskType;
    private string taskText;
    private int taskStepsMax;
    private int taskStepsCurrent;

    /// <summary>
    /// Initializes this Task's private members to the supplied parameter values.  
    /// taskStepsCurrent is initialized to 0, taskComplete is initialized to false.
    /// </summary>
    /// <param name="ttype"></param>
    /// <param name="ttext"></param>
    /// <param name="tstepsmax"></param>
    public void Init(TaskType ttype, string ttext, int tstepsmax) {
        taskType = ttype;
        taskText = ttext;
        taskStepsMax = tstepsmax;
        taskStepsCurrent = 0;
        UpdateTask();
    }

    /// <summary>
    /// Returns true if parameter TaskType matches this Task's taskType member.
    /// </summary>
    /// <param name="ttype"></param>
    /// <returns></returns>
    public bool IsTaskType(TaskType ttype) {
        return taskType == ttype;
    }

    /// <summary>
    /// Returns true if task has been advanced the required number of times to complete.
    /// </summary>
    /// <returns></returns>
    public bool IsTaskComplete() {
        return taskStepsCurrent == taskStepsMax;
    }

    /// <summary>
    /// Increments this Task's current step counter by one if current is less than max before this increment.
    /// Then calls UpdateTask.
    /// </summary>
    public void AdvanceTask() {
        if (taskStepsCurrent < taskStepsMax) taskStepsCurrent++;
        UpdateTask();
    }

    // update icon (complete/incomplete) and the text
    private void UpdateTask() {
        UpdateIcon();
        UpdateText();
    }

    private void UpdateIcon() {
        RawImage r = gameObject.GetComponentInChildren(typeof(RawImage)) as RawImage;
        if (r) {
            if (taskStepsCurrent >= taskStepsMax) {
                r.texture = ResourceManager.Instance.CompleteTexture;
            } else {
                r.texture = ResourceManager.Instance.IncompleteTexture;
            }
        } else {
            D.warn("Task " + gameObject.name + " is missing RawImage child component");
        }
    }

    private void UpdateText() {
        Text t = gameObject.GetComponentInChildren(typeof(Text)) as Text;
        if (t != null) {
            StringBuilder sb = new StringBuilder();
            sb.Append(taskText);
            if (taskStepsMax > 1) {
                sb.Append(string.Format(" ({0}/{1})", taskStepsCurrent, taskStepsMax));
            }
            t.text = sb.ToString();
        }
        
        //if (t && taskStepsMax > 1) {
        //    t.text = taskText + " (" + taskStepsCurrent + "/" + taskStepsMax + ")";
        //}

        //if (t) {
        //    t.text = taskText + " (" + taskStepsCurrent + "/" + taskStepsMax + ")";
        //} else {
        //    D.warn("Task " + gameObject.name + " is missing Text child component");
        //}
    }


}
