using UnityEngine;
using System.Collections;

public class MessageSenderOnMessageEventDelayed : MonoBehaviour {

    public delegate void DialogueMessageEventHandler(string msg);
    public static event DialogueMessageEventHandler DialogueMessageEventDelayed;

    public string messageEvent; // what we are listening for
    public string messageToSend;// the message output to the UI for the player
    public float messageDelay;  // delay in seconds before the message is sent out

    void Start() {
        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    void OnDestroy() {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    // when Dialoguer message occurs and matches the one we are looking for, call onDialogueMessageEvent
    private void onMessageEvent(string message, string metadata) {
        if (message == messageEvent) {
            Invoke("onDialogueMessageEventDelayed", messageDelay);
        }
    }

    private void onDialogueMessageEventDelayed() {
        if (DialogueMessageEventDelayed != null) {
            DialogueMessageEventDelayed(messageToSend);
        }
    }
}
