using UnityEngine;
using System.Collections;

// provide the messageEvent string in inspector that we are listening to
// from Dialoguer.  When messageEvent is received, this object will be destroy on the next 
// Update
public class DestroyOnMessageEvent : MonoBehaviour {

    public string messageEvent;

    void Start () {
        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    void OnDestroy () {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    private void onMessageEvent (string message, string metadata) {
        if (message == messageEvent) {
            D.log ("messageEvent: {0} recieved, destroying object: {1}", messageEvent, gameObject.name);
            Destroy (gameObject);
        }
    }
}
