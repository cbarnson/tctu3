using UnityEngine;
using System.Collections;

public class MessageSenderOnStart : MonoBehaviour {

    public delegate void OnEnableMessageHandler(string msg, float delay, bool single);
    public static event OnEnableMessageHandler OnStartMessageEvent;

    public string messageToSend;
    public float messageStartDelay = 0.0f; // amount of time that should pass before the message becomes visible
    public bool singleMode = true; // if true, message will fade in a out with delay, else waits for exit collider

    private void Start() {
        if (OnStartMessageEvent != null) {
            OnStartMessageEvent(messageToSend, messageStartDelay, singleMode);
        }
    }
}
