using UnityEngine;
using System.Collections;
using System;

// class to dispatch messages to be displayed to the user on a specific Dialoguer message event
public class MessageSenderOnMessageEvent : MonoBehaviour {

    public delegate void DialogueMessageEventHandler (string msg);
    public static event DialogueMessageEventHandler onDialogueMessageEvent;

    public string messageEvent; // what we are listening for
    public string messageToSend;// the message output to the UI for the player

    void Start () {
        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    void OnDestroy () {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    // when Dialoguer message occurs and matches the one we are looking for, call onDialogueMessageEvent
    private void onMessageEvent (string message, string metadata) {
        if (message == messageEvent) {
            if (onDialogueMessageEvent != null) {
                onDialogueMessageEvent (messageToSend);
            }
        }
    }
}
