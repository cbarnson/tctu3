using UnityEngine;
using System.Collections;

public class DestroyComponentOnMessageEvent : MonoBehaviour {

    public string messageEvent;
    public Component componentToDestroy;

    void Start () {
        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    void OnDestroy () {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    private void onMessageEvent (string message, string metadata) {
        if (message == messageEvent) {
            D.log ("messageEvent: {0} recieved, destroying component: {1}", messageEvent, componentToDestroy.name);
            Destroy (componentToDestroy);
            D.log ("messageEvent: {0} recieved, destroying self: {1}", messageEvent, gameObject.name);
            Destroy (this);
        }
    }
}
