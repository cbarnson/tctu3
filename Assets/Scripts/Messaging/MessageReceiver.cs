using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent (typeof(CanvasGroup))]
[RequireComponent (typeof(Fade))]
public class MessageReceiver : MonoBehaviour {

    private CanvasGroup _canvasGroup;
    private Fade _fade; // lets us fade it in and out smoothly
    private Text _text; // the Text component we will be modifying

    //private string _lastMessage;
    private List<string> _messageHistory;

    void Awake () {
        _canvasGroup = GetComponent<CanvasGroup> ();
        _fade = GetComponent<Fade> ();
        _text = GetComponentInChildren<Text> ();
        _messageHistory = new List<string>();
        MessageSenderOnStart.OnStartMessageEvent += onStartMessageEvent;
    }

    void Start () {
        MessageSender.onTriggerEnter += onTriggerEnter;
        MessageSender.onTriggerExit += onTriggerExit;
        MessageSenderOnMessageEvent.onDialogueMessageEvent += onDialogueMessageEvent;
        MessageSenderOnMessageEventDelayed.DialogueMessageEventDelayed += onDialogueMessageEvent;
    }

    void OnDestroy () {
        MessageSender.onTriggerEnter -= onTriggerEnter;
        MessageSender.onTriggerExit -= onTriggerExit;
        MessageSenderOnMessageEvent.onDialogueMessageEvent -= onDialogueMessageEvent;
        MessageSenderOnMessageEventDelayed.DialogueMessageEventDelayed -= onDialogueMessageEvent;
        MessageSenderOnStart.OnStartMessageEvent -= onStartMessageEvent;
    }

    private void onStartMessageEvent(string msg, float delay, bool single) {
        _text.text = msg;
        _messageHistory.Add(msg);
        _fade.CancelRoutines();
        if (single) {
            _fade.FadeMeInOutDelayed(delay); // note: delay is the time before the message starts
        } else {
            _fade.FadeMeIn();
        }
    }

    // set the text in the target object (containing a Text component) to the parameter msg
    // update _lastMessage, then perform the fading accordingly
    // if single is TRUE, the message will only be displayed a 'single' time before fading out automatically,
    // otherwise the message will be displayed until onTriggerExit is called
    private void onTriggerEnter (string msg, bool single) {
        _text.text = msg;
        _messageHistory.Add(msg);          // add message to history

        _fade.CancelRoutines ();
        if (single) {
            _fade.FadeMeInOutDelayed ();
        } else {
            _fade.FadeMeIn ();
        }
    }

    // fades out the message
    private void onTriggerExit () {
        _fade.CancelRoutines ();
        if (_canvasGroup.alpha > 0.0f) {
            _fade.FadeMeOut ();
        }
    }

    // updates the target text with the supplied string, fades in the canvas, then fades out after a delay
    private void onDialogueMessageEvent (string msg) {
        _text.text = msg;
        _messageHistory.Add(msg);          // add message to history

        _fade.CancelRoutines ();
        _fade.FadeMeInOutDelayed ();
    }

    private void onStartMessageDelayed(string msg) {
        _text.text = msg;
        _messageHistory.Add(msg);

        _fade.CancelRoutines();
        _fade.FadeMeInOutDelayed();
    }


}
