using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Collider))]
public class MessageSender : MonoBehaviour {

    public delegate void EnterMessageHandler (string msg, bool single);
    public static event EnterMessageHandler onTriggerEnter;

    public delegate void ExitMessageHandler ();
    public static event ExitMessageHandler onTriggerExit;

    public string messageToSend;
    public bool singleMode = true; // if true, message will fade in a out with delay, else waits for exit collider

    public void OnTriggerEnter (Collider other) {
        if (other.tag == "Player") { // ensure the player object with the main capsule collider is tagged "Player"
            if (onTriggerEnter != null) {
                onTriggerEnter (messageToSend, singleMode);
            }
        }
    }

    public void OnTriggerExit (Collider other) {
        if (other.tag == "Player") {
            if (onTriggerExit != null) {
                onTriggerExit ();
            }
        }
    }
}
