using UnityEngine;
using System.Collections;

// class to manage the activation of a member GameObject (set in inspector) upon receiving 
// a particular Dialoguer message event
public class EnableOnMessageEvent : MonoBehaviour {

    public string messageEvent;   // Dialoguer message event that triggers the activation of toBeEnabled
    public GameObject toBeEnabled;// GameObject to be enabled when message event is received

    private void Awake () {
        if (toBeEnabled != null) {
            toBeEnabled.SetActive (false); // GameObject should be disabled by default, otherwise this class is useless
        }
    }

    void Start () {
        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    void OnDestroy () {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    // set the member GameObject to active, write info to debug log
    private void onMessageEvent (string message, string metadata) {
        if (message == messageEvent) {
            D.log ("messageEvent: {0} recieved, enabling object: {1}", messageEvent, gameObject.name);
            toBeEnabled.SetActive (true);
        }
    }
}
