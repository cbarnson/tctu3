﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// class to manage the deactivation of a member GameObject (set in inspector) upon receiving 
// a particular Dialoguer message event
public class DisableOnMessageEvent : MonoBehaviour {

    public string messageEvent;   // Dialoguer message event that triggers the activation of toBeEnabled
    public GameObject toBeDisabled;// GameObject to be enabled when message event is received

    protected void Awake() {
        D.assert(toBeDisabled != null);
    }

    void Start() {
        Dialoguer.events.onMessageEvent += onMessageEvent;
    }

    void OnDestroy() {
        Dialoguer.events.onMessageEvent -= onMessageEvent;
    }

    // set the member GameObject to active, write info to debug log
    private void onMessageEvent(string message, string metadata) {
        if (string.IsNullOrEmpty(message) || string.IsNullOrEmpty(messageEvent)) return;
        if (message == messageEvent) {
            D.log("messageEvent: {0} recieved, enabling object: {1}", messageEvent, gameObject.name);
            toBeDisabled.SetActive(false);
        }
    }
}
