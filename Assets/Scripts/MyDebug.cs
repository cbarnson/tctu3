using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using util;

public class MyDebug : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


        if (Input.GetKeyDown(KeyCode.N)) {
            util.tctu.loadLevelSummary();
        } else if (Input.GetKeyDown(KeyCode.Alpha1)) {
            // load museum
            tctu.loadSceneByName("museum");
        } else if (Input.GetKeyDown(KeyCode.Alpha2)) {
            // load acropolis
            tctu.loadSceneByName("acropolis");
        } else if (Input.GetKeyDown(KeyCode.Alpha3)) {
            // load aphaia
            tctu.loadSceneByName("aphaia");
        } else if (Input.GetKeyDown(KeyCode.Alpha4)) {
            // load propylaia
            tctu.loadSceneByName("propylaia");
        }
    }

    //void OnMouseDown() {
    //    D.log("clicked: " + EventSystem.current.currentSelectedGameObject);
    //}
}
