using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

namespace DentedPixel.LTExamples {

    public class PathBezier : MonoBehaviour {

        public float timeToCompleteCircuit = 5.0f;

        public Transform[] trans;
        public GameObject avatar1;
        private LTBezierPath cr;

        void OnEnable () {
            // create the path
            Vector3[] vec = new Vector3[trans.Length];
            for (int i = 0; i < trans.Length; i++) {
                vec[i] = trans[i].position;
            }
            cr = new LTBezierPath(vec);
        }

        void Start () {
            Assert.IsNotNull(avatar1);
            // Tween automatically
            LTDescr descr = LeanTween.move(avatar1, cr.pts, timeToCompleteCircuit).setOrientToPath(true).setRepeat(-1);
            //Debug.Log("length of path 1:" + cr.length);
            //Debug.Log("length of path 2:" + descr.path.length);
        }

        private float iter;
        void Update () {
            // Or Update Manually
            //cr.place2d( sprite1.transform, iter );

            iter += Time.deltaTime * 0.07f;
            if (iter > 1.0f)
                iter = 0.0f;
        }

        void OnDrawGizmos () {
            // Debug.Log("drwaing");
            if (cr != null)
                OnEnable();
            Gizmos.color = Color.red;
            if (cr != null)
                cr.gizmoDraw(); // To Visualize the path, use this method
        }
    }

}