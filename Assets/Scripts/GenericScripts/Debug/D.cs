#define DEBUG_LEVEL_LOG
#define DEBUG_LEVEL_WARN
#define DEBUG_LEVEL_ERROR

using UnityEngine;
using System.Collections;
using System;


// setting the conditional to the platform of choice will only compile the method for that platform
// alternatively, use the #defines at the top of this file
public static class D {

    static D () {
#pragma warning disable CS0618 // Type or member is obsolete
        Application.RegisterLogCallback(logCallback);
#pragma warning restore CS0618 // Type or member is obsolete
    }

    public static void logCallback (string log, string stackTrace, LogType type) {
        // error gets a stack trace
        if (type == LogType.Error) {
            Console.WriteLine(log);
            Console.WriteLine(stackTrace);
        } else {
            Console.WriteLine(log);
        }
    }

    // if several conditionals, will fire on log OR warn OR error
    [System.Diagnostics.Conditional("DEBUG_LEVEL_LOG")]
    [System.Diagnostics.Conditional("DEBUG_LEVEL_WARN")]   
    [System.Diagnostics.Conditional("DEBUG_LEVEL_ERROR")]
    public static void log (object format, params object[] paramList) {
        if (format is string)
            Debug.Log(string.Format(format as string, paramList));
        else
            Debug.Log(format);
    }


    [System.Diagnostics.Conditional("DEBUG_LEVEL_WARN")]
    [System.Diagnostics.Conditional("DEBUG_LEVEL_ERROR")]
    public static void warn (object format, params object[] paramList) {
        if (format is string)
            Debug.LogWarning(string.Format(format as string, paramList));
        else
            Debug.LogWarning(format);
    }


    [System.Diagnostics.Conditional("DEBUG_LEVEL_ERROR")]
    public static void error (object format, params object[] paramList) {
        if (format is string)
            Debug.LogError(string.Format(format as string, paramList));
        else
            Debug.LogError(format);
    }

    // passes assert on to version with 3 parameters below
    // DOES pause on fail
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    [System.Diagnostics.Conditional("DEBUG_LEVEL_LOG")]
    public static void assert (bool condition) {
        assert(condition, string.Empty, true);
    }

    // passes assert on to version with 3 parameters below
    // does NOT pause on fail
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    [System.Diagnostics.Conditional("DEBUG_LEVEL_LOG")]
    public static void assert (bool condition, string assertString) {
        assert(condition, assertString, false);
    }


    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    [System.Diagnostics.Conditional("DEBUG_LEVEL_LOG")]
    public static void assert (bool condition, string assertString, bool pauseOnFail) {
        if (!condition) {
            Debug.LogError("assert failed! " + assertString);

            if (pauseOnFail)
                Debug.Break();
        }
    }

}
