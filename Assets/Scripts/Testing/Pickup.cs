using UnityEngine;
using System.Collections;
using System;

//public class PickupEventArgs : System.EventArgs {
//    public PickupEventArgs(string n) {
//        name = n;
//    }
//    public string name;
//}

public class Pickup : MonoBehaviour {

    //public delegate void ObjectPickupEventHandler(object pickup);
    //public static event ObjectPickupEventHandler ObjectPickup;

    //public static event EventHandler<PickupEventArgs> ObjectPickup;
    //PickupEventArgs e = new PickupEventArgs("calf_bearer");

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            D.log("object was picked up");
            //NoteManager.unhideNote("calf_bearer");
            //OnObjectPickup(this, e);
            Destroy(this.gameObject);
        }
    }

    //private void OnObjectPickup(object sender, PickupEventArgs e) {
    //    if (ObjectPickup != null) {
    //        D.log("calling objectpickup from pickup");
    //        ObjectPickup(sender, e);
    //    }
    //}

    
}
