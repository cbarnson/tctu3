using UnityEngine;
using System.Collections;
using System;

public class Player : MonoBehaviour {

    public static Collider m_collider;
    public static Transform m_transform;
    public static Camera m_camera;

    void Awake() {
        m_collider = GetComponent<Collider>();
        m_transform = GetComponent<Transform>();
        m_camera = GetComponentInChildren<Camera>();
    }

    private void Start() {
        CameraController.onSpeakerFocus += onSpeakerFocus;
    }

    private void OnDestroy() {
        CameraController.onSpeakerFocus -= onSpeakerFocus;
    }

    private static Vector3 t = Vector3.zero;
    private static Vector3 t_last = Vector3.zero;
    private static float rotateRate = 20f;
    private static float initialForwardFocusPoint = 0.5f; // we start at a point in front of us, and update the view using
                                                          // LookAt each frame until the point arrives at the final target

    public void stopCoroutines() {
        StopAllCoroutines();
    }

    private void onSpeakerFocus(Vector3 pos) {
        // get initial Vec3 at point just in front of player at center screen
        t = Player.m_camera.ScreenToWorldPoint(new Vector3(Player.m_camera.pixelWidth / 2, Player.m_camera.pixelHeight / 2, initialForwardFocusPoint));
        // save the initial value for t in t_last, we use this after dialogue ends to smooth reset the camera rotation
        t_last = t;
        // look smoothly at the target
        StopAllCoroutines();
        StartCoroutine(smoothLook(pos));
    }

    IEnumerator smoothLook(Vector3 target) {
        while (t != target) {
            t = Vector3.MoveTowards(t, target, rotateRate * Time.deltaTime);
            Player.m_camera.transform.LookAt(t);
            yield return null;
        }
    }
}
