using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using System.Collections;
using System;

public class Fieldbook : MonoBehaviour {

    public delegate void FieldbookEventHandler (bool isFieldbookOpen);
    public static event FieldbookEventHandler onFieldbook;


    public static Canvas m_canvas;
    public static GameObject m_catalog;     // stores inventory items
    public static GameObject m_notes;       // stores note entries

    public Button catalogButton;            // when fieldbook open, switch to catalog view
    public Button notesButton;              // when fieldbook open, switch to notes view
    public float _fadeRate = 4.0f;          // rate at which fieldbook fades when opened or closed

    public AudioClip clip;                  // sound to play on open or on close
    private float _clipVolume = 1.0f;       // sound volume, 1 = normal

    private CanvasGroup _canvasGroup;
    //private Timeline _timeline;
    private static CatalogPanel _catalog;
    private bool _lockedCanvasState = false;

    // properties

    // return the number of items currently in the catalog inventory
    public static int InventoryCount {
        get { return _catalog.countItems(); }
    }

    // add the param obj to the catalog inventory
    public static void addToInventory (GameObject obj) {
        _catalog.addItem(obj);
    }

    void Awake () {
        m_canvas = GetComponent<Canvas>();
        m_canvas.enabled = false;
        m_catalog = GetComponentInChildren<CatalogPanel>(true).gameObject;
        _catalog = GetComponentInChildren<CatalogPanel>(true);
        if (!_catalog) { D.log("error, could not find catalog"); }
        m_notes = GetComponentInChildren<Notes>(true).gameObject;
    }

    //public void onPause(bool isPauseEnabled) {
    //    if (isPauseEnabled) {
    //        D.log ("canvas state is now locked");
    //        _lockedCanvasState = true;
    //    } else {
    //        D.log ("canvas state is unlocked");
    //        _lockedCanvasState = false;
    //    }
    //}

    // set defaults for the fieldbook
    void Start () {
        m_catalog.SetActive(true);
        m_notes.SetActive(false);
        _canvasGroup = GetComponent<CanvasGroup>();
        //_canvasGroup.alpha = 0.0f;
        //_timeline = (Timeline)FindObjectOfType(typeof(Timeline));

        // listen for events
        //Pause.onPause += onPause;
        //Dialoguer.events.onStarted += onStarted;
        //Dialoguer.events.onEnded += onEnded;
    }

    //private void onStarted () {
    //    _lockedCanvasState = true;
    //}

    //private void onEnded () {
    //    _lockedCanvasState = false;
    //}



    //void OnDestroy () {
    //    Pause.onPause -= onPause;
    //    Dialoguer.events.onStarted -= onStarted;
    //    Dialoguer.events.onEnded -= onEnded;
    //}

    // reset the fieldbook if it is closed and notes tab was the last one opened
    void Update() {
        //if (_lockedCanvasState) { return; }
        if (!m_canvas.enabled && m_notes.activeSelf) {
            m_notes.SetActive(false);
            m_catalog.SetActive(true);
        }
        //if (Input.GetKeyDown(KeyCode.T)) {
        //    if (m_canvas.enabled) {
        //        deactivate();
        //    } else {
        //        activate();
        //    }
        //}
    }

    public void openNotes() {
        m_notes.SetActive(true);
        m_catalog.SetActive(false);
    }

    public void openCatalog() {
        m_notes.SetActive(false);
        m_catalog.SetActive(true);
    }

    // close fieldbook UI
    //private void deactivate () {
    //    D.log ("fieldbook deactivate");
    //    StopAllCoroutines ();
    //    StartCoroutine (fadeOut ());
    //    if (onFieldbook != null) { onFieldbook (false); }
    //    AudioSource.PlayClipAtPoint(clip, Player.m_camera.transform.position, _clipVolume);
    //}

    // open fieldbook UI
    //private void activate () {
    //    D.log ("fieldbook activate");
    //    m_canvas.enabled = true;
    //    StopAllCoroutines ();
    //    StartCoroutine (fadeIn ());
    //    if (onFieldbook != null) { onFieldbook (true); }
    //    AudioSource.PlayClipAtPoint(clip, Player.m_camera.transform.position, _clipVolume);
    //}

    // fade out, basic
    IEnumerator fadeOut () {
        while (_canvasGroup.alpha > 0.0f) {
            _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 0.0f, _fadeRate * Time.deltaTime);
            yield return null;
        }
        D.log("disable canvas");
        m_canvas.enabled = false;
        yield return null;
    }

    // fade in, basic
    IEnumerator fadeIn () {
        while (_canvasGroup.alpha < 1.0f) {
            _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, 1.0f, _fadeRate * Time.deltaTime);
            yield return null;
        }
        yield return null;
    }

}
